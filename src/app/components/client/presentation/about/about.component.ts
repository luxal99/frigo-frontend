import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.sass'],
})
export class AboutComponent implements OnInit {
  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.route.fragment.subscribe((fragment) => {
        if (fragment) {
          document.getElementById(fragment).scrollIntoView();
        } else {
          window.scrollTo(0, 0);
        }
      });
    }, 100);
  }
}
