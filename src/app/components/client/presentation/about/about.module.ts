import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './about.component';
import { SharedModule } from '../../../common/shared.module';

@NgModule({
  declarations: [AboutComponent],
  imports: [CommonModule, SharedModule],
})
export class AboutModule {}
