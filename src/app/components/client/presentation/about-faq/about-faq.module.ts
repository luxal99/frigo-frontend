import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutFaqComponent } from './about-faq.component';
import { SharedModule } from '../../../common/shared.module';

@NgModule({
  declarations: [AboutFaqComponent],
  imports: [CommonModule, SharedModule],
})
export class AboutFaqModule {}
