import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PricelistComponent } from './pricelist.component';
import { SharedModule } from '../../../common/shared.module';

@NgModule({
  declarations: [PricelistComponent],
  imports: [CommonModule, SharedModule],
})
export class PricelistModule {}
