import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-terms',
  templateUrl: './about-terms.component.html',
  styleUrls: ['./about-terms.component.sass'],
})
export class AboutTermsComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }
}
