import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutTermsComponent } from './about-terms.component';
import { SharedModule } from '../../../common/shared.module';

@NgModule({
  declarations: [AboutTermsComponent],
  imports: [CommonModule, SharedModule],
})
export class AboutTermsModule {}
