import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrivacyPolicyComponent } from './privacy-policy.component';
import { SharedModule } from '../../../common/shared.module';

@NgModule({
  declarations: [PrivacyPolicyComponent],
  imports: [CommonModule, SharedModule],
})
export class PrivacyPolicyModule {}
