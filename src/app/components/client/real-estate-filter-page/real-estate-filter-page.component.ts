import {
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { RealEstateService } from '../../../service/real-estate.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RealEstate } from '../../../models/RealEstate';
import {
  Cordinates,
  CURRENT_QUERY,
  Headers,
  PAGE_INCREMENT,
  Pages,
  RestRoutesConst,
} from '../../../const/const';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DialogUtil } from '../../../util/dialog-util';
import { AdvancedSearchDialogComponent } from '../../common/advanced-search-dialog/advanced-search-dialog.component';
import { SpinnerService } from '../../../service/spinner.service';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { LazyLoadComponentsUtil } from '../../../util/lazy-load-components';
import { MediaType } from '../../../enums/MediaType';
import { EmptyListComponent } from '../../common/empty-list/empty-list.component';
import { PageFilter } from '../../../models/PageFilter';
import { CurrentQuery } from '../../../models/current-query';
import { CustomPaginatorComponent } from '../../common/custom-paginator/custom-paginator.component';
import { FooterComponent } from '../../common/footer/footer.component';
import { MobileService } from '../../../util/service/mobile.service';
import { PaginatorService } from '../../common/custom-paginator/service/paginator.service';

@Component({
  selector: 'app-real-estate-filter-page',
  templateUrl: './real-estate-filter-page.component.html',
  styleUrls: ['./real-estate-filter-page.component.sass'],
})
export class RealEstateFilterPageComponent implements OnInit {
  @ViewChild('spinner') spinner: MatProgressSpinner;

  @ViewChild('footer', { read: ViewContainerRef, static: false })
  footer: ViewContainerRef;
  @ViewChild('emptyList', { read: ViewContainerRef, static: false })
  entry: ViewContainerRef;

  @ViewChild(CustomPaginatorComponent)
  paginator: CustomPaginatorComponent;

  initPage = 0;
  pageIncrement = PAGE_INCREMENT;
  currentQuery!: CurrentQuery;
  gap = 3;

  pageFilter!: PageFilter;

  lat = Cordinates.DEF_LATITUDE;
  lng = Cordinates.DEF_LONGITUDE;

  isDateDescending = false;
  isPriceDescending = undefined;

  zoom = 10;

  urlParams = '';

  URL_PREFIX = RestRoutesConst.REST_URL;
  listOfListRealEstates: RealEstate[] = [];

  constructor(
    private realEstateService: RealEstateService,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private spinnerService: SpinnerService,
    private cvRef: ViewContainerRef,
    private resolver: ComponentFactoryResolver,
    public mobileService: MobileService,
    public paginatorService: PaginatorService
  ) {}

  ngOnInit(): void {
    this.getUrlParams();
    this.scrollToTop();

    this.getAllEstatesByUrl();
    setTimeout(() => {
      this.loadFooter();
    }, 500);
  }

  async navigateToEstate(id): Promise<void> {
    await this.router.navigate([Pages.REAL_ESTATE_DETAIL_PAGE_ROUTE], {
      queryParams: { id },
    });
  }

  sortByLowestPrice(): void {
    this.setPriceAndDateFilter(true, undefined);
    this.spinnerService.show(this.spinner);
    this.clearPageFilter();
    this.getAllEstatesByUrl();
  }

  sortByHighestPrice(): void {
    this.setPriceAndDateFilter(false, undefined);
    this.spinnerService.show(this.spinner);
    this.clearPageFilter();
    this.getAllEstatesByUrl();
  }

  sortByDateUp(): void {
    this.setPriceAndDateFilter(undefined, true);
    this.spinnerService.show(this.spinner);
    this.clearPageFilter();
    this.getAllEstatesByUrl();
  }

  sortByDateDown(): void {
    this.setPriceAndDateFilter(undefined, false);
    this.spinnerService.show(this.spinner);
    this.clearPageFilter();
    this.getAllEstatesByUrl();
  }

  openAdvancedSearchDialog(): void {
    const dialogConfig: MatDialogConfig = { autoFocus: false };
    if (window.screen.width <= 570) {
      dialogConfig.position = { bottom: '0' };
      dialogConfig.minWidth = '100%';
      dialogConfig.width = '100%';
      dialogConfig.height = '85%';
    } else {
      dialogConfig.position = { left: '0' };
      dialogConfig.width = '40%';
      dialogConfig.height = '100vh';
    }

    DialogUtil.openDialog(
      AdvancedSearchDialogComponent,
      dialogConfig,
      this.dialog
    )
      .afterClosed()
      .subscribe(() => {
        this.spinnerService.show(this.spinner);
        this.initPage = 0;
        this.pageIncrement = this.gap;
        this.getAllEstatesByUrl(this.initPage);
      });
  }

  getUrlParams(): void {
    this.route.queryParams.subscribe((params) => {
      this.urlParams = params.q;
    });
  }

  getAllEstatesByUrl(page?: number): void {
    this.scrollToTop();
    // @ts-ignore
    if (page >= 0) {
      this.spinnerService.show(this.spinner);

      sessionStorage.setItem(
        CURRENT_QUERY,
        JSON.stringify({ page, urlParams: this.urlParams })
      );
    }
    this.realEstateService
      .getRealEstateByProperty(
        this.urlParams,
        this.choosePage(page),
        this.isDateDescending,
        this.isPriceDescending
      )
      .subscribe(async (resp) => {
        this.paginatorService.setPageFilter(
          new PageFilter(
            // tslint:disable-next-line:radix
            Number.parseInt(resp.headers.get(Headers.X_PAGE)),
            // @ts-ignore
            resp.headers.get(Headers.X_PAGE_COUNT) as number,
            // tslint:disable-next-line:radix
            Number.parseInt(resp.headers.get(Headers.X_DATA_COUNT))
          )
        );

        if (resp.body.length === 0) {
          this.spinnerService.hide(this.spinner);
          LazyLoadComponentsUtil.loadComponent(
            EmptyListComponent,
            this.entry,
            this.cvRef,
            this.resolver
          );
        } else {
          this.entry.clear();
        }
        this.listOfListRealEstates = resp.body;
        for (const realEstate of this.listOfListRealEstates) {
          realEstate.createdDate = new Date(realEstate.createdDate);
          realEstate.medias = realEstate.medias?.filter(
            (media) => media.type !== MediaType.YOUTUBE
          );
        }
        this.spinnerService.hide(this.spinner);
      });
  }

  zoomToMarker(longitude: number, latitude: number): void {
    if (longitude === null && latitude === null) {
      this.lat = Cordinates.DEF_LATITUDE;
      this.lng = Cordinates.DEF_LONGITUDE;
      this.zoom = 10;
    } else {
      this.lat = latitude;
      this.lng = longitude;
      this.zoom = 18;
    }
  }

  nextPage(): void {
    this.initPage += this.gap;
    this.pageIncrement += this.gap;
  }

  previousPage(): void {
    if (this.initPage - this.gap >= 0) {
      this.initPage -= this.gap;
      this.pageIncrement -= this.gap;
    }
  }

  choosePage(page: any): any {
    setTimeout(() => {
      document.getElementById('real-estate-column').scrollTop = 0;
    }, 100);
    // @ts-ignore
    this.currentQuery = JSON.parse(sessionStorage.getItem(CURRENT_QUERY));
    // @ts-ignore
    if (page) {
      return page;
    } else if (
      this.currentQuery &&
      this.urlParams === this.currentQuery.urlParams
    ) {
      setTimeout(() => {
        this.paginator.initPage = this.currentQuery.page;
        this.paginator.pageIncrement = this.currentQuery.page + this.gap;
      }, 200);
      return this.currentQuery.page;
    } else {
      return 0;
    }
  }

  onImageError($event: any): void {
    $event.target.src =
      'https://i2.wp.com/learn.onemonth.com/wp-content/uploads/2017/08/1-10.png?w=845&ssl=1';
  }

  setPriceAndDateFilter(price: any, date: any): void {
    this.isPriceDescending = price;
    this.isDateDescending = date;
  }

  clearPageFilter(): void {
    sessionStorage.removeItem(CURRENT_QUERY);
  }

  scrollToTop(): void {
    window.scrollTo(0, 0);
  }

  loadFooter(): void {
    LazyLoadComponentsUtil.loadComponent(
      FooterComponent,
      this.footer,
      this.cvRef,
      this.resolver
    );
  }
}
