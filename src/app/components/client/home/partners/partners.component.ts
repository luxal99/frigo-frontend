import { Component, OnInit } from '@angular/core';
import { MobileService } from '../../../../util/service/mobile.service';
import { Partners } from '../../../../models/Partners';
import SwiperCore, { Autoplay } from 'swiper';

SwiperCore.use([Autoplay]);

@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.sass'],
})
export class PartnersComponent implements OnInit {
  URL_PREFIX = 'assets/img/partners/';
  partners: Partners[] = [
    {
      id: 1,
      url: this.URL_PREFIX + 'icon-192.png',
      path: 'https://www.nekretnine.rs/',
    },
    {
      id: 2,
      url: this.URL_PREFIX + 'nadji dom.png',
      path: 'https://www.nadjidom.com/',
    },
    {
      id: 3,
      url: this.URL_PREFIX + 'intesaaa.jpg',
      path: 'https://www.bancaintesa.rs/',
    },
    {
      id: 4,
      url: this.URL_PREFIX + '4zida.png',
      path: 'https://www.4zida.rs/',
    },
    {
      id: 5,
      url: this.URL_PREFIX + 'halo.jpg',
      path: 'https://www.halooglasi.com/',
    },
    {
      id: 6,
      url: this.URL_PREFIX + 'komercijalna_banka_logo.jpg',
      path: 'https://www.kombank.com/sr',
    },
    {
      id: 7,
      url: this.URL_PREFIX + 'otp-logo.png',
      path: 'https://www.otpbanka.rs/',
    },
    {
      id: 8,
      url: this.URL_PREFIX + 'Indomio.png',
      path: 'https://www.indomio.rs/',
    },
    {
      id: 9,
      url: this.URL_PREFIX + 'Banka_Poštanska_Štedionica.png',
      path: 'http://www.posted.co.rs/',
    },
    {
      id: 10,
      url: this.URL_PREFIX + 'renew.jfif',
      path: 'https://www.renewconcept.rs/',
    },
    {
      id: 11,
      url: this.URL_PREFIX + 'LOGO FRIGO 2000.jpg',
      path: 'https://www.frigonekretnine.com/',
    },
    {
      id: 12,
      url: this.URL_PREFIX + 'realitica.jpg',
      path: 'https://www.realitica.com/',
    },
  ];

  constructor(public mobileService: MobileService) {}

  ngOnInit(): void {}
}
