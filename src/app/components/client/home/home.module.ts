import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { SharedModule } from '../../common/shared.module';
import { SwiperModule } from 'swiper/angular';
import { PartnersComponent } from './partners/partners.component';

@NgModule({
  declarations: [HomeComponent, PartnersComponent],
  imports: [CommonModule, SharedModule, SwiperModule],
})
export class HomeModule {}
