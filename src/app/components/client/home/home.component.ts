import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoryService } from '../../../service/category.service';
import { Category } from '../../../models/Category';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DialogUtil } from '../../../util/dialog-util';
import { OfferDialogComponent } from '../../common/offer-dialog/offer-dialog.component';
import { DialogOptions } from '../../../util/dialog-options';
import { MatDialog } from '@angular/material/dialog';
import { RealEstateService } from '../../../service/real-estate.service';
import { RealEstate } from '../../../models/RealEstate';
import { FormControlNames, Pages, RestRoutesConst } from '../../../const/const';
import { Router } from '@angular/router';
import { CriteriaBuilder } from '../../../util/query/CriteriaBuilder';
import { MediaType } from '../../../enums/MediaType';
import { SpinnerService } from '../../../service/spinner.service';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { GenericService } from '../../../service/generic.service';
import { Status } from '../../../enums/Status';
import { SaleType } from '../../../enums/SaleType';
import SwiperCore, { Navigation, Pagination, Virtual } from 'swiper';
import { MobileService } from '../../../util/service/mobile.service';
import { ClientCriteriaSingleton } from '../../../util/query/ClientCriteriaSingleton';

SwiperCore.use([Virtual, Navigation, Pagination]);

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass'],
})
export class HomeComponent implements OnInit {
  @ViewChild('spinner') spinner: MatProgressSpinner;
  URL_PREFIX = RestRoutesConst.REST_URL;

  listOfPinnedRealEstates: RealEstate[] = [];
  listOfSaleTypes: string[] = [];
  searchByCategoryForm = new FormGroup({
    categoryName: new FormControl('', Validators.required),
    saleType: new FormControl('', Validators.required),
  });
  listOfCategories: Category[] = [];

  constructor(
    private categoryService: CategoryService,
    private dialog: MatDialog,
    private spinnerService: SpinnerService,
    private genericService: GenericService<any>,
    private realEstateService: RealEstateService,
    private router: Router,
    public mobileService: MobileService
  ) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.getAllCategories();
    this.getLastThreeEstates();
    this.getAllSaleTypes();
  }

  getAllSaleTypes(): void {
    this.realEstateService.getAllSalesTypes().subscribe((resp) => {
      this.listOfSaleTypes = resp;
    });
  }

  getLastThreeEstates(): void {
    this.realEstateService.getPinnedEstates().subscribe((resp) => {
      this.listOfPinnedRealEstates = resp;
      this.listOfPinnedRealEstates = this.listOfPinnedRealEstates.filter(
        (item) => item.status === Status.ACTIVE
      );
      for (const realEstate of this.listOfPinnedRealEstates) {
        realEstate.createdDate = new Date(realEstate.createdDate);
        realEstate.medias = realEstate.medias?.filter(
          (media) => media.type !== MediaType.YOUTUBE
        );
      }
      this.spinnerService.hide(this.spinner);
    });
  }

  openAddOfferDialog(): void {
    DialogUtil.openDialog(
      OfferDialogComponent,
      DialogOptions.getOptions({}),
      this.dialog
    );
  }

  getAllCategories(): void {
    this.categoryService.getAll().subscribe((resp) => {
      this.listOfCategories = resp.map((item) => ({
        id: item.id,
        name: item.name,
      }));
    });
  }

  basicSearch(): void {
    const query = new ClientCriteriaSingleton().queryBuilder;
    query
      .eq(
        'category.id',
        this.searchByCategoryForm.get(
          FormControlNames.CATEGORY_NAME_FORM_CONTROL
        )?.value
      )
      .criteria((builder) => {
        builder.eq(
          'saleType',
          this.searchByCategoryForm.get(
            FormControlNames.SALES_TYPE_FORM_CONTROL
          )?.value
        );
        return builder;
      });
    this.router.navigate([Pages.REAL_ESTATE_FILTER_PAGE_ROUTE], {
      queryParams: { q: query.buildUrlEncoded() },
    });
  }

  async navigateToEstate(id): Promise<void> {
    await this.router.navigate([Pages.REAL_ESTATE_DETAIL_PAGE_ROUTE], {
      queryParams: { id },
    });
  }

  async searchOnSell(): Promise<void> {
    const query = new CriteriaBuilder();
    query
      .eq(FormControlNames.SALES_TYPE_FORM_CONTROL, SaleType.SALE.toString())
      .and()
      .ne('status', Status.INACTIVE.toString());
    await this.router.navigate([Pages.REAL_ESTATE_FILTER_PAGE_ROUTE], {
      queryParams: { q: query.buildUrlEncoded() },
    });
  }

  async searchOnRent(): Promise<void> {
    const query = new CriteriaBuilder();
    query
      .eq(FormControlNames.SALES_TYPE_FORM_CONTROL, SaleType.RENT.toString())
      .and()
      .ne('status', Status.INACTIVE.toString());
    await this.router.navigate([Pages.REAL_ESTATE_FILTER_PAGE_ROUTE], {
      queryParams: { q: query.buildUrlEncoded() },
    });
  }

  navigateToPart(fragment): void {
    this.router.navigate(['/about'], { fragment });
  }

  navigateToCompanyOfTrust(fragment): void {
    this.router.navigate(['/about'], { fragment });
  }
}
