import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RealEstateByIdComponent } from './real-estate-by-id.component';
import { RealEstateSmallMediaGalleryComponent } from './real-estate-small-media-gallery/real-estate-small-media-gallery.component';
import { SharedModule } from '../../common/shared.module';
import { AgmCoreModule } from '@agm/core';
import { LuxalLightboxModule } from 'luxal-lightbox';
@NgModule({
  declarations: [RealEstateByIdComponent, RealEstateSmallMediaGalleryComponent],
  imports: [
    CommonModule,
    SharedModule,
    LuxalLightboxModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD2xkxTiOL085Yi8ZekKHnUB-E9c4UT-Hg',
    }),
  ],
})
export class RealEstateByIdModule {}
