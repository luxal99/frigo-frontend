import {
  Component,
  ElementRef,
  HostListener,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RealEstate } from '../../../models/RealEstate';
import { RealEstateService } from '../../../service/real-estate.service';
import { Cordinates, RestRoutesConst } from '../../../const/const';
import { MatDialog } from '@angular/material/dialog';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { SpinnerService } from '../../../service/spinner.service';
import { MediaType } from '../../../enums/MediaType';
import { MobileService } from '../../../util/service/mobile.service';
import { LuxalLightboxService } from 'luxal-lightbox';

@Component({
  selector: 'app-real-estate-by-id',
  templateUrl: './real-estate-by-id.component.html',
  styleUrls: ['./real-estate-by-id.component.sass'],
})
export class RealEstateByIdComponent implements OnInit {
  @ViewChild('ytVideo') iframe: ElementRef;
  @ViewChild('spinner') spinner: MatProgressSpinner;
  URL_PREFIX = RestRoutesConst.REST_URL;
  realEstate: RealEstate;
  ytLink = '';

  lat = Cordinates.DEF_LATITUDE;
  lng = Cordinates.DEF_LONGITUDE;

  constructor(
    private route: ActivatedRoute,
    private realEstateService: RealEstateService,
    private dialog: MatDialog,
    private spinnerService: SpinnerService,
    public mobileService: MobileService,
    private luxalLightBoxService: LuxalLightboxService
  ) {}

  ngOnInit(): void {
    this.findById();
    window.scrollTo(0, 0);
  }

  findById(): void {
    this.route.params.subscribe((params) => {
      this.realEstateService.findById(params.id).subscribe(
        (realEstate) => {
          this.realEstate = realEstate;
          // @ts-ignore
          const realEstateYouTubeMedia: [] = realEstate.medias?.filter(
            (media) => media.type === MediaType.YOUTUBE
          );
          if (realEstateYouTubeMedia.length > 0) {
            // @ts-ignore
            this.ytLink = realEstateYouTubeMedia[0].uri;
          }
          realEstate.medias = realEstate.medias?.filter(
            (media) => media.type !== MediaType.YOUTUBE
          );
          setTimeout(() => {
            this.spinnerService.hide(this.spinner);
          }, 100);
        },
        () => {}
      );
    });
  }

  async openMediaDialog(): Promise<void> {
    const images: { uri: string }[] = this.realEstate.medias.map((item) => ({
      order: item.order,
      uri: this.URL_PREFIX + '/' + item.uri,
    }));
    this.luxalLightBoxService.openGallery(images);
  }

  showGallery(): void {
    const prop = {
      images: this.realEstate.medias?.map((media) => ({
        path: this.URL_PREFIX + '/' + media.uri,
      })),
    };
    // @ts-ignore
    this.gallery.load(prop);
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(): void {
    const div = document.querySelector('.header-navbar-info');
    const h1 = document.querySelector('.header-nav-h1');
    const h3 = document.querySelector('.header-nav-h3');
    if (window.pageYOffset > 300) {
      // @ts-ignore
      div.style.height = '170px';
      // @ts-ignore
      div.style.padding = '4em 2em';
      // @ts-ignore
      div.style.backgroundColor = '#fff';
      // @ts-ignore
      h1.style.visibility = 'visible';
      // @ts-ignore
      h3.style.visibility = 'visible';
    } else {
      // @ts-ignore
      h1.style.visibility = 'hidden';
      // @ts-ignore
      h3.style.visibility = 'hidden';
      // @ts-ignore
      div.style.height = '1%';
      // @ts-ignore
      div.style.backgroundColor = '#fff';
      // @ts-ignore
      div.style.padding = '2em';
    }
  }
}
