import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { Media } from '../../../../models/Media';
import { RestRoutesConst } from '../../../../const/const';
import { MobileService } from '../../../../util/service/mobile.service';

@Component({
  selector: 'app-real-estate-small-media-gallery',
  templateUrl: './real-estate-small-media-gallery.component.html',
  styleUrls: ['./real-estate-small-media-gallery.component.sass'],
})
export class RealEstateSmallMediaGalleryComponent implements OnInit {
  @Input() realEstateListOfMedias: Media[] = [];
  @Output() openImageGallery = new EventEmitter();

  URL_PREFIX = RestRoutesConst.REST_URL;
  public increment = 0;

  constructor(public mobileService: MobileService) {}

  ngOnInit(): void {}

  nextImage(): void {
    if (this.increment + 1 <= this.realEstateListOfMedias.length - 1) {
      this.increment++;
    } else {
      this.increment = 0;
    }
  }

  previousImage(): void {
    if (this.increment > 0) {
      this.increment--;
    }
  }

  @HostListener('window:orientationchange', ['$event'])
  onOrientationChange(): void {
    this.mobileService.setIsMobileValue(!(window.screen.width <= 660));
    this.mobileService.setIsTabletValue(
      !(window.screen.width >= 660 && window.screen.width <= 1025)
    );
  }
}
