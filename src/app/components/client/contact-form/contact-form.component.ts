import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FieldConfig } from '../../../models/FIeldConfig';
import {
  FormControlNames,
  InputTypes,
  MESSAGE_FORM_CONTROL,
  PHONE_FORM_CONTROL,
  SnackBarMessages,
} from '../../../const/const';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ContactMessageService } from '../../../service/contact-message.service';
import { SnackbarUtil } from '../../../util/snackbar-util';
import { SpinnerService } from '../../../service/spinner.service';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { CKEditorComponent } from '@ckeditor/ckeditor5-angular';
import * as ClassicEditor from 'lib/ckeditor5-build-classic';
import { ContactMessage } from '../../../models/ContactMessage';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.sass'],
})
export class ContactFormComponent implements OnInit {
  @ViewChild('editor', { static: false }) editorComponent: CKEditorComponent;
  public Editor = ClassicEditor;
  @ViewChild('spinner') spinner: MatProgressSpinner;
  contactMessageForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
  });

  phoneInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: PHONE_FORM_CONTROL,
  };
  firstNameInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.FIRST_NAME_FORM_CONTROL,
  };
  lastnameNameInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.LAST_NAME_FORM_CONTROL,
  };
  emailInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.EMAIL_FORM_CONTROL,
  };
  messageInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: MESSAGE_FORM_CONTROL,
  };

  constructor(
    private snackBar: MatSnackBar,
    private spinnerService: SpinnerService,
    private contactMessageService: ContactMessageService
  ) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }

  send(): void {
    this.spinnerService.show(this.spinner);
    const contactMessage: ContactMessage =
      this.contactMessageForm.getRawValue();
    contactMessage.message = this.editorComponent.editorInstance?.getData();
    this.contactMessageService.save(contactMessage).subscribe(
      () => {
        SnackbarUtil.openSnackBar(
          this.snackBar,
          SnackBarMessages.SUCCESS_MESSAGE
        );
        this.spinnerService.hide(this.spinner);
      },
      () => {
        SnackbarUtil.openSnackBar(this.snackBar, SnackBarMessages.ERR_MESSAGE);
        this.spinnerService.hide(this.spinner);
      }
    );
  }
}
