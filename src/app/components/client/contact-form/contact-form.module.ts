import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactFormComponent } from './contact-form.component';
import { SharedModule } from '../../common/shared.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@NgModule({
  declarations: [ContactFormComponent],
  imports: [CommonModule, SharedModule, CKEditorModule],
})
export class ContactFormModule {}
