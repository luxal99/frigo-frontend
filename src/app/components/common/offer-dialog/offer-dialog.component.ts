import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CategoryService } from '../../../service/category.service';
import { Category } from '../../../models/Category';
import { Structure } from '../../../models/Structure';
import { StructureService } from '../../../service/structure.service';
import { FloorService } from '../../../service/floor.service';
import { Floor } from '../../../models/Floor';
import { City } from '../../../models/City';
import { CityService } from '../../../service/city.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RealEstateService } from '../../../service/real-estate.service';
import { Offer } from '../../../models/Offer';
import {
  AREA_UNIT_FORM_CONTROL,
  FormControlNames,
  PROPERTY_AREA_UNIT_FORM_CONTROL,
  SnackBarMessages,
} from '../../../const/const';
import { ContactType } from '../../../models/ContactType';
import { OfferService } from '../../../service/offer.service';
import { SnackbarUtil } from '../../../util/snackbar-util';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AddressService } from '../../../service/address.service';
import { Municipality } from '../../../models/Municipality';
import { MunicipalityService } from '../../../service/municipality.service';
import { AreaUnitService } from '../../../service/area-unit.service';
import { AreaUnit } from '../../../models/AreaUnit';

@Component({
  selector: 'app-offer-dialog',
  templateUrl: './offer-dialog.component.html',
  styleUrls: ['./offer-dialog.component.sass'],
})
export class OfferDialogComponent implements OnInit {
  @ViewChild('desiredMunicipality') desiredMunicipality: ElementRef;
  addressForm = new FormGroup({
    street: new FormControl('', Validators.required),
    number: new FormControl(0),
    city: new FormControl('', Validators.required),
    desiredMunicipality: new FormControl(),
    municipality: new FormControl(),
  });
  contactForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    telephone: new FormControl('', Validators.required),
    email: new FormControl(''),
  });

  offerForm = new FormGroup({
    area: new FormControl(),
    areaUnit: new FormControl(1),
    propertyArea: new FormControl(),
    propertyAreaUnit: new FormControl(0.01),
    category: new FormControl('', Validators.required),
    floor: new FormControl(),
    structure: new FormControl(),
    saleType: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required),
  });

  listOfCategories: Category[] = [];
  listOfStructures: Structure[] = [];
  listOfFloors: Floor[] = [];
  listOfCities: City[] = [];
  listOfMunicipalities: Municipality[] = [];
  listOfAreaUnits: AreaUnit[] = [];
  listOfSalesTypes: string[] = [];

  constructor(
    private categoryService: CategoryService,
    private floorService: FloorService,
    private offerService: OfferService,
    private snackBar: MatSnackBar,
    private addressService: AddressService,
    private municipalityService: MunicipalityService,
    private cityService: CityService,
    private advertService: RealEstateService,
    private structureService: StructureService,
    private areaUnitService: AreaUnitService
  ) {}

  ngOnInit(): void {
    this.getAllCategories();
    this.getAllFloors();
    this.getAllCities();
    this.getAllStructures();
    this.getAllSalesTypes();
    this.getAllMunicipality();
    this.getAllAreaUnits();
  }

  getAllMunicipality(): void {
    this.municipalityService.getAll().subscribe((resp) => {
      this.listOfMunicipalities = resp;
    });
  }

  getAllSalesTypes(): void {
    this.advertService.getAllSalesTypes().subscribe((resp) => {
      this.listOfSalesTypes = resp;
    });
  }

  getAllAreaUnits(): void {
    this.areaUnitService.getAll().subscribe((resp) => {
      this.listOfAreaUnits = resp;
    });
  }

  getAllCities(): void {
    this.cityService.getAll().subscribe((resp) => {
      this.listOfCities = resp;
    });
  }

  getAllFloors(): void {
    this.floorService.getAll().subscribe((resp) => {
      this.listOfFloors = resp;
    });
  }

  getAllCategories(): void {
    this.categoryService.getAll().subscribe((resp) => {
      this.listOfCategories = resp;
    });
  }

  getAllStructures(): void {
    this.structureService.getAll().subscribe((resp) => {
      this.listOfStructures = resp;
    });
  }

  showDesiredMunicipalityInput(): void {
    if (this.desiredMunicipality.nativeElement.style.display === 'none') {
      this.desiredMunicipality.nativeElement.style.display = 'block';
    } else {
      this.desiredMunicipality.nativeElement.style.display = 'none';
    }
  }

  sendOffer(): void {
    const offer: Offer = {
      address:
        this.addressForm.get(FormControlNames.STREET_FORM_CONTROL)?.value +
          ' ' +
          this.addressForm.get(FormControlNames.NUMBER_FORM_CONTROL)?.value !==
        '0'
          ? this.addressForm.get(FormControlNames.NUMBER_FORM_CONTROL)?.value
          : '',
      municipality: this.addressForm.get(
        FormControlNames.MUNICIPALITY_FORM_CONTROL
      ).value,
      city: {
        id: this.addressForm.get(FormControlNames.CITY_FORM_CONTROL).value,
      },
      price: this.offerForm.get(FormControlNames.PRICE_FORM_CONTROL)?.value,
      category: this.offerForm.get(FormControlNames.CATEGORY_FORM_CONTROL)
        ?.value,
      floor: this.offerForm.get(FormControlNames.FLOOR_FORM_CONTROL)?.value,
      structure: this.offerForm.get(FormControlNames.STRUCTURE_FORM_CONTROL)
        ?.value,
      saleType: this.offerForm.get(FormControlNames.SALES_TYPE_FORM_CONTROL)
        ?.value,
      owner: {
        person: {
          firstName: this.contactForm.get(
            FormControlNames.FIRST_NAME_FORM_CONTROL
          )?.value,
          lastName: this.contactForm.get(
            FormControlNames.LAST_NAME_FORM_CONTROL
          )?.value,
          contacts: [
            {
              type: ContactType.EMAIL,
              value: this.contactForm.get(FormControlNames.EMAIL_FORM_CONTROL)
                ?.value,
            },
            {
              type: ContactType.PHONE,
              value: this.contactForm.get(
                FormControlNames.TELEPHONE_FORM_CONTROL
              )?.value,
            },
          ],
        },
      },
    };

    if (this.offerForm.get(FormControlNames.AREA_FORM_CONTROL)?.value) {
      const areaUnit: AreaUnit | undefined = this.listOfAreaUnits.find(
        (arUn) =>
          arUn.conversionFactor ===
          this.offerForm.get(AREA_UNIT_FORM_CONTROL)?.value
      );
      offer.area =
        this.offerForm.get(FormControlNames.AREA_FORM_CONTROL)?.value +
        areaUnit?.value;
    }

    if (
      this.offerForm.get(FormControlNames.PROPERTY_AREA_FORM_CONTROL)?.value
    ) {
      const areaUnit: AreaUnit | undefined = this.listOfAreaUnits.find(
        (arUn) =>
          arUn.conversionFactor ===
          this.offerForm.get(PROPERTY_AREA_UNIT_FORM_CONTROL)?.value
      );
      // @ts-ignore
      offer.propertyArea =
        this.offerForm.get(FormControlNames.PROPERTY_AREA_FORM_CONTROL)?.value +
        areaUnit?.value;
    }

    this.offerService.save(offer).subscribe(
      () => {
        SnackbarUtil.openSnackBar(
          this.snackBar,
          SnackBarMessages.SUCCESS_MESSAGE
        );
      },
      () => {
        SnackbarUtil.openSnackBar(this.snackBar, SnackBarMessages.ERR_MESSAGE);
      }
    );
  }
}
