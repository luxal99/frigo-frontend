import { Component, OnInit } from '@angular/core';
import { CriteriaBuilder } from '../../../util/query/CriteriaBuilder';
import { FormControlNames, Pages } from '../../../const/const';
import { SaleType } from '../../../enums/SaleType';
import { Router } from '@angular/router';
import { DialogUtil } from '../../../util/dialog-util';
import { OfferDialogComponent } from '../offer-dialog/offer-dialog.component';
import { DialogOptions } from '../../../util/dialog-options';
import { MatDialog } from '@angular/material/dialog';
import { ClientCriteriaSingleton } from '../../../util/query/ClientCriteriaSingleton';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass'],
})
export class FooterComponent implements OnInit {
  constructor(private router: Router, private dialog: MatDialog) {}

  ngOnInit(): void {}

  openAddOfferDialog(): void {
    DialogUtil.openDialog(
      OfferDialogComponent,
      DialogOptions.getOptions({}),
      this.dialog
    );
  }

  async searchOnSell(): Promise<void> {
    const query = new ClientCriteriaSingleton().queryBuilder;
    query.eq(
      FormControlNames.SALES_TYPE_FORM_CONTROL,
      SaleType.SALE.toString()
    );
    this.router
      .navigate([Pages.REAL_ESTATE_FILTER_PAGE_ROUTE], {
        queryParams: { q: query.buildUrlEncoded() },
      })
      .then(() => {
        location.reload();
      });
  }

  searchOnRent(): void {
    const query = new ClientCriteriaSingleton().queryBuilder;
    query.eq(
      FormControlNames.SALES_TYPE_FORM_CONTROL,
      SaleType.RENT.toString()
    );
    this.router
      .navigate([Pages.REAL_ESTATE_FILTER_PAGE_ROUTE], {
        queryParams: { q: query.buildUrlEncoded() },
      })
      .then(() => {
        location.reload();
      });
  }

  navigateToCompanyOfTrust(fragment): void {
    this.router.navigate(['/about'], { fragment });
  }
}
