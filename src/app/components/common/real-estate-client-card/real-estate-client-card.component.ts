import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RealEstate } from '../../../models/RealEstate';
import { RestRoutesConst, SnackBarMessages } from '../../../const/const';
import { MobileService } from '../../../util/service/mobile.service';
import { AuthService } from '../../../service/auth.service';
import { DialogUtil } from '../../../util/dialog-util';
import { AddRealEstateDialogComponent } from '../../business/login/admin/real-estate-overview/add-real-estate-dialog/add-real-estate-dialog.component';
import { SnackbarUtil } from '../../../util/snackbar-util';
import { MatDialog } from '@angular/material/dialog';
import { RealEstateService } from '../../../service/real-estate.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RealEstateOverviewDialogComponent } from '../../business/login/admin/real-estate-overview/real-estate-overview-dialog/real-estate-overview-dialog.component';
import { DialogOptions } from '../../../util/dialog-options';

@Component({
  selector: 'app-real-estate-client-card',
  templateUrl: './real-estate-client-card.component.html',
  styleUrls: ['./real-estate-client-card.component.sass'],
})
export class RealEstateClientCardComponent implements OnInit {
  @Input() listOfRealEstates: RealEstate[] = [];
  @Input() realEstate: RealEstate;
  @Input() flex = 2;
  URL_PREFIX = RestRoutesConst.REST_URL;

  @Output() reloadEstate = new EventEmitter();
  @Output() openRealEstateOverview = new EventEmitter();
  @Output() realEstateOverviewMethod = new EventEmitter();
  @Output() imageHover = new EventEmitter();

  constructor(
    public mobileService: MobileService,
    public authService: AuthService,
    private dialog: MatDialog,
    private realEstateService: RealEstateService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {}

  openEditEstateDialog(realEstate: RealEstate): void {
    DialogUtil.openDialog(
      AddRealEstateDialogComponent,
      {
        maxWidth: '100vw',
        maxHeight: '100vh',
        height: '100%',
        width: '100%',
        data: { id: realEstate.id },
      },
      this.dialog
    )
      .afterClosed()
      .subscribe(() => {
        this.reloadEstate.emit();
      });
  }

  openRealEstateOverviewDialog(realEstate: RealEstate): void {
    DialogUtil.openDialog(
      RealEstateOverviewDialogComponent,
      DialogOptions.getOptions({ id: realEstate.id }),
      this.dialog
    );
  }

  deleteEstate(id): void {
    this.realEstateService.delete(id).subscribe(
      () => {
        this.reloadEstate.emit(true);
        SnackbarUtil.openSnackBar(
          this.snackBar,
          SnackBarMessages.SUCCESS_MESSAGE
        );
      },
      () => {
        SnackbarUtil.openSnackBar(this.snackBar, SnackBarMessages.ERR_MESSAGE);
      }
    );
  }

  navigateToRealEstate(id: number): void {
    window.open(`http://${window.location.hostname}/oglas/${id}`, '_blank');
  }
}
