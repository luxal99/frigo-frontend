import { NgModule } from '@angular/core';
import { CustomPaginatorComponent } from './custom-paginator/custom-paginator.component';
import { EmptyListComponent } from './empty-list/empty-list.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PropertyBindingComponent } from './property-binding/property-binding.component';
import { RealEstateClientCardComponent } from './real-estate-client-card/real-estate-client-card.component';
import { RealEstateFilterPageHeaderToolComponent } from './real-estate-filter-page-header-tool/real-estate-filter-page-header-tool.component';
import { ScrollToTopButtonComponent } from './scroll-to-top-button/scroll-to-top-button.component';
import { PipesModule } from '../../pipes/pipes.module';
import { DynamicFormModule } from '../../util/dynamic-form/dynamic-form.module';
import { MaterialModule } from '../../material.module';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HeadingComponent } from './heading/heading.component';
import { OfferDialogComponent } from './offer-dialog/offer-dialog.component';
import { AdvancedSearchDialogComponent } from './advanced-search-dialog/advanced-search-dialog.component';

@NgModule({
  declarations: [
    AdvancedSearchDialogComponent,
    CustomPaginatorComponent,
    EmptyListComponent,
    FooterComponent,
    NavbarComponent,
    PropertyBindingComponent,
    RealEstateClientCardComponent,
    RealEstateFilterPageHeaderToolComponent,
    ScrollToTopButtonComponent,
    OfferDialogComponent,
    HeadingComponent,
  ],
  imports: [
    DynamicFormModule,
    MaterialModule,
    PipesModule,
    RouterModule,
    CommonModule,
  ],
  exports: [
    AdvancedSearchDialogComponent,
    CustomPaginatorComponent,
    EmptyListComponent,
    MaterialModule,
    FooterComponent,
    NavbarComponent,
    HeadingComponent,
    OfferDialogComponent,
    PropertyBindingComponent,
    RealEstateClientCardComponent,
    RealEstateFilterPageHeaderToolComponent,
    ScrollToTopButtonComponent,
    DynamicFormModule,
    PipesModule,
  ],
})
export class SharedModule {}
