import { Component, HostListener, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-scroll-to-top-button',
  templateUrl: './scroll-to-top-button.component.html',
  styleUrls: ['./scroll-to-top-button.component.sass'],
})
export class ScrollToTopButtonComponent implements OnInit {
  @Input() element: HTMLElement;

  isHidden = true;

  constructor() {}

  ngOnInit(): void {}

  scrollToTop(): void {
    this.element.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'nearest',
    });
  }

  @HostListener('window:scroll', ['$event'])
  showButton(): void {
    this.isHidden = window.pageYOffset < 500;
  }
}
