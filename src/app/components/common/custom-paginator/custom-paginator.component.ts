import {
  AfterContentChecked,
  AfterViewChecked,
  AfterViewInit,
  Component,
  EventEmitter,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { PageFilter } from '../../../models/PageFilter';
import { PAGE_INCREMENT } from '../../../const/const';
import { CurrentQuery } from '../../../models/current-query';
import { PaginatorService } from './service/paginator.service';
import { Observable, of, Subscription } from 'rxjs';

@Component({
  selector: 'app-custom-paginator',
  templateUrl: './custom-paginator.component.html',
  styleUrls: ['./custom-paginator.component.sass'],
})
export class CustomPaginatorComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onPageChange = new EventEmitter();
  pageFilter: PageFilter = new PageFilter(0, 0, 0);

  initPage = 0;
  pageIncrement = PAGE_INCREMENT;
  currentQuery!: CurrentQuery;
  gap = PAGE_INCREMENT;

  urlParams = '';

  pageFilterSubscription: Subscription = new Subscription();

  constructor(private paginatorService: PaginatorService) {}

  ngOnDestroy(): void {
    this.pageFilterSubscription.unsubscribe();
  }

  ngOnInit(): void {}

  nextPage(): void {
    this.initPage += this.gap;
    this.pageIncrement += this.gap;
  }

  previousPage(): void {
    if (this.initPage - this.gap >= 0) {
      this.initPage -= this.gap;
      this.pageIncrement -= this.gap;
    } else {
      this.initPage = 0;
      this.pageIncrement = PAGE_INCREMENT;
    }
  }

  resetPaginator(): void {
    this.pageIncrement = PAGE_INCREMENT;
    this.initPage = 0;
  }

  ngAfterViewInit(): void {
    this.pageFilterSubscription =
      this.paginatorService.behaviorSubjectPageFilter.subscribe((value) => {
        this.pageFilter = value;
      });
  }
}
