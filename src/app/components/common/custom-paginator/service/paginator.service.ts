import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PageFilter } from '../../../../models/PageFilter';

@Injectable({
  providedIn: 'root',
})
export class PaginatorService {
  // tslint:disable-next-line:variable-name
  private _behaviorSubjectPageFilter = new BehaviorSubject(
    new PageFilter(0, 0, 0)
  );

  constructor() {}

  setPageFilter(pageFilter: PageFilter): void {
    this._behaviorSubjectPageFilter.next(pageFilter);
  }

  getLastPageFilter(): PageFilter {
    return this._behaviorSubjectPageFilter.value;
  }

  get behaviorSubjectPageFilter(): BehaviorSubject<PageFilter> {
    return this._behaviorSubjectPageFilter;
  }
}
