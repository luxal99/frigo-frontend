import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-heading',
  templateUrl: './heading.component.html',
  styleUrls: ['./heading.component.sass'],
})
export class HeadingComponent implements OnInit {
  @Input() headerText = '';

  constructor() {}

  ngOnInit(): void {}
}
