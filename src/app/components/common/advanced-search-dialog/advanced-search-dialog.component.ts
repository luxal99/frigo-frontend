import {
  AfterContentInit,
  AfterViewInit,
  Component,
  ComponentFactoryResolver,
  Inject,
  OnInit,
  ViewContainerRef,
} from '@angular/core';
import { CategoryService } from '../../../service/category.service';
import { Category } from '../../../models/Category';
import { FloorService } from '../../../service/floor.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RealEstateService } from '../../../service/real-estate.service';
import { StructureService } from '../../../service/structure.service';
import { PaymentPeriodService } from '../../../service/payment-period.service';
import { PriceTypeService } from '../../../service/price-type.service';
import { ParkingService } from '../../../service/parking.service';
import { FurnitureService } from '../../../service/furniture.service';
import { HeatingService } from '../../../service/heating.service';
import { Heating } from '../../../models/Heating';
import { Floor } from '../../../models/Floor';
import { City } from '../../../models/City';
import { Parking } from '../../../models/Parking';
import { Furniture } from '../../../models/Furniture';
import { PriceType } from '../../../models/PriceType';
import { PaymentPeriod } from '../../../models/PaymentPeriod';
import { Structure } from '../../../models/Structure';
import { Municipality } from '../../../models/Municipality';
import { FieldConfig } from '../../../models/FIeldConfig';
import {
  ADMIN_FILTER,
  AREA_UNIT_FORM_CONTROL,
  CLIENT_FILTER,
  CURRENT_QUERY,
  FormControlNames,
  InputTypes,
  Pages,
  PROPERTY_AREA_FROM_FORM_CONTROL,
  PROPERTY_AREA_TO_FORM_CONTROL,
} from '../../../const/const';
import { CriteriaBuilder } from '../../../util/query/CriteriaBuilder';
import { ActivatedRoute, Router } from '@angular/router';
import { Status } from '../../../enums/Status';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AreaUnit } from '../../../models/AreaUnit';
import { AreaUnitService } from '../../../service/area-unit.service';
import { ConstructionYearDto } from '../../../models/ContructionYearDto';
import { FromTo } from '../../../models/from-to';
import { CityService } from '../../../service/city.service';
import { MobileService } from '../../../util/service/mobile.service';
import { ClientCriteriaSingleton } from '../../../util/query/ClientCriteriaSingleton';

@Component({
  selector: 'app-advanced-search-dialog',
  templateUrl: './advanced-search-dialog.component.html',
  styleUrls: ['./advanced-search-dialog.component.sass'],
})
export class AdvancedSearchDialogComponent implements OnInit, AfterViewInit {
  realEstateFilter!: any;

  listOfStatuses: any[] = [
    { name: Status.ACTIVE },
    { name: Status.INACTIVE },
    { name: Status.SOLD_RENTED },
  ];
  listOfCategories: Category[] = [];
  listOfHeatings: Heating[] = [];
  listOfFloors: Floor[] = [];
  listOfCities: City[] = [];
  listOfParkings: Parking[] = [];
  listOfFurnitures: Furniture[] = [];
  listOfPriceTypes: PriceType[] = [];
  listOfSalesTypes: string[] = [];
  listOfPaymentPeriods: PaymentPeriod[] = [];
  listOfStructures: Structure[] = [];
  listOfMunicipalities: Municipality[] = [];
  listOfAreaUnits: AreaUnit[] = [];
  listOfConstructionYears: ConstructionYearDto[] = [
    { id: 1, lt: '1999', gt: '', name: 'Pre 2000' },
    { id: 2, lt: '2010', gt: '2000', name: '2000-2010' },
    { id: 3, lt: '2019', gt: '2011', name: '2011-2019' },
    { id: 4, lt: '', gt: '2020', name: '2020 i novije' },
  ];

  areaUnitSelectConfig: FieldConfig = {
    name: AREA_UNIT_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };
  //region -- FirstFormConfig --
  categorySelectConfig: FieldConfig = {
    name: FormControlNames.CATEGORY_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };
  heatingSelectConfig: FieldConfig = {
    name: FormControlNames.HEATING_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };
  floorSelectConfig: FieldConfig = {
    name: FormControlNames.FLOOR_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };
  statusSelectConfig: FieldConfig = {
    name: FormControlNames.STATUS_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
    options: this.listOfStatuses,
  };
  //endregion

  //region -- AddressAndContactFormConfig --
  municipalitySelectConfig: FieldConfig = {
    type: InputTypes.SELECT_TYPE_NAME,
    name: FormControlNames.MUNICIPALITY_FORM_CONTROL,
  };

  citySelectConfig: FieldConfig = {
    type: InputTypes.SELECT_TYPE_NAME,
    name: FormControlNames.CITY_FORM_CONTROL,
  };
  //endregion

  //region -- 6th FormConfig --
  furnitureStatusSelectConfig: FieldConfig = {
    name: FormControlNames.FURNITURE_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };
  parkingSelectConfig: FieldConfig = {
    name: FormControlNames.PARKING_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };

  //endregion

  //region --PriceFormConfig--
  priceTypeSelectConfig: FieldConfig = {
    type: InputTypes.SELECT_TYPE_NAME,
    name: FormControlNames.PRICE_TYPE_FORM_CONTROL,
  };
  priceFromInputConfig: FieldConfig = {
    type: InputTypes.SELECT_TYPE_NAME,
    name: FormControlNames.PRICE_FROM_FORM_CONTROL,
  };
  priceToInputConfig: FieldConfig = {
    type: InputTypes.SELECT_TYPE_NAME,
    name: FormControlNames.PRICE_TO_FORM_CONTROL,
  };
  constructionYearSelectConfig: FieldConfig = {
    type: InputTypes.SELECT_TYPE_NAME,
    name: FormControlNames.CONSTRUCTION_YEAR_FORM_CONTROL,
    options: this.listOfConstructionYears,
  };

  areaToInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.AREA_TO_FORM_CONTROL,
  };
  areaFromInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.AREA_FROM_FORM_CONTROL,
  };
  //endregion

  propertyAreaToInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: PROPERTY_AREA_TO_FORM_CONTROL,
  };
  propertyAreaFromInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: PROPERTY_AREA_FROM_FORM_CONTROL,
  };
  //endregion

  // region -- LastStepFormConfig --
  saleTypeSelectConfig: FieldConfig = {
    name: FormControlNames.SALES_TYPE_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };
  structureSelectConfig: FieldConfig = {
    name: FormControlNames.STRUCTURE_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };
  termsPaymentPeriodSelectConfig: FieldConfig = {
    name: FormControlNames.TERMS_PAYMENT_PERIOD_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };
  termsDepositInputConfig: FieldConfig = {
    name: FormControlNames.TERMS_DEPOSIT_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };

  //endregion

  realEstateForm = new FormGroup({
    category: new FormControl('', Validators.required),
    city: new FormControl(''),
    areaUnit: new FormControl(1),
    propertyAreaUnit: new FormControl(0.01),
    heatings: new FormControl(''),
    areaFrom: new FormControl(),
    areaTo: new FormControl(),
    propertyAreaFrom: new FormControl(''),
    propertyAreaTo: new FormControl(''),
    furniture: new FormControl(''),
    municipality: new FormControl(''),
    parkings: new FormControl(''),
    priceFrom: new FormControl(''),
    priceTo: new FormControl(''),
    priceType: new FormControl(''),
    saleType: new FormControl('', Validators.required),
    structure: new FormControl([]),
    floor: new FormControl([]),
    constructionYear: new FormControl(''),
    status: new FormControl(''),
  });

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: boolean,
    private categoryService: CategoryService,
    private floorService: FloorService,
    private realEstateService: RealEstateService,
    private structureService: StructureService,
    private paymentPeriodService: PaymentPeriodService,
    private priceTypeService: PriceTypeService,
    private router: Router,
    private route: ActivatedRoute,
    private parkingService: ParkingService,
    private furnitureService: FurnitureService,
    private cvRef: ViewContainerRef,
    private resolver: ComponentFactoryResolver,
    private dialogRef: MatDialogRef<AdvancedSearchDialogComponent>,
    private areaUnitService: AreaUnitService,
    private heatingService: HeatingService,
    private cityService: CityService,
    public mobileService: MobileService
  ) {}

  ngAfterViewInit(): void {
    this.setValuesToForm();
  }

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.getAllCategories();
    this.getAllHeating();
    this.getAllFloors();
    this.getAllCities();
    this.getAllParkings();
    this.getAllFurnitures();
    this.getAllPriceTypes();
    this.getAllStructures();
    this.getAllSalesTypes();
    this.getAllPaymentPeriods();
    this.getAllAreaUnits();
  }

  getAllAreaUnits(): void {
    this.areaUnitService.getAll().subscribe((resp) => {
      this.listOfAreaUnits = resp;
      this.areaUnitSelectConfig.options = resp.map((areaUnit) => ({
        id: areaUnit.id,
        name: areaUnit.value,
        conversionFactor: areaUnit.conversionFactor,
      }));
    });
  }

  getAllCategories(): void {
    this.categoryService.getAll().subscribe((resp) => {
      this.listOfCategories = resp;
      this.categorySelectConfig.options = resp;
    });
  }

  getAllCities(): void {
    this.cityService.getAll().subscribe((resp) => {
      this.listOfCities = resp;
      this.citySelectConfig.options = resp;
    });
  }

  getAllFloors(): void {
    this.floorService.getAll().subscribe((resp) => {
      this.listOfFloors = resp;
      this.floorSelectConfig.options = resp;
    });
  }

  getAllHeating(): void {
    this.heatingService.getAll().subscribe((resp) => {
      this.listOfHeatings = resp;
      this.heatingSelectConfig.options = resp.map((item) => ({
        id: item.id,
        name: item.type,
      }));
    });
  }

  getAllParkings(): void {
    this.parkingService.getAll().subscribe((resp) => {
      this.listOfParkings = resp;
      this.parkingSelectConfig.options = resp.map((item) => ({
        id: item.id,
        name: item.type,
      }));
    });
  }

  getAllFurnitures(): void {
    this.furnitureService.getAll().subscribe((resp) => {
      this.listOfFurnitures = resp;
      this.furnitureStatusSelectConfig.options = resp.map((item) => ({
        id: item.id,
        name: item.type,
      }));
    });
  }

  getAllPriceTypes(): void {
    this.priceTypeService.getAll().subscribe((resp) => {
      this.listOfPriceTypes = resp;
      this.priceTypeSelectConfig.options = resp.map((item) => ({
        id: item.id,
        name: item.type,
      }));
    });
  }

  getAllSalesTypes(): void {
    this.realEstateService.getAllSalesTypes().subscribe((resp) => {
      this.listOfSalesTypes = resp;
      this.saleTypeSelectConfig.options = resp.map((item) => ({
        name: item,
      }));
    });
  }

  getAllPaymentPeriods(): void {
    this.paymentPeriodService.getAll().subscribe((resp) => {
      this.listOfPaymentPeriods = resp;
      this.termsPaymentPeriodSelectConfig.options = resp.map((item) => ({
        id: item.id,
        name: item.value,
      }));
    });
  }

  getAllStructures(): void {
    this.structureService.getAll().subscribe((resp) => {
      this.listOfStructures = resp;
      this.structureSelectConfig.options = resp.map((item) => ({
        id: item.id,
        name: item.type,
      }));
    });
  }

  setValuesToForm(): void {
    const filterName = this.data ? ADMIN_FILTER : CLIENT_FILTER;
    if (localStorage.getItem(filterName)) {
      this.realEstateFilter = JSON.parse(
        localStorage.getItem(filterName) as string
      );
      for (const [k, v] of Object.entries(this.realEstateFilter)) {
        if (k === 'propertyAreaFrom' || k === 'propertyAreaTo') {
          // @ts-ignore
          const value = v * this.realEstateFilter.propertyAreaUnit;
          this.realEstateForm.get(k)?.setValue(value);
        } else if (k === 'areaFrom' || k === 'areaTo') {
          // @ts-ignore
          const value = v * this.realEstateFilter.areaUnit;
          this.realEstateForm.get(k)?.setValue(value);
        } else {
          this.realEstateForm.get(k)?.setValue(v);
        }
      }
    }
  }

  async filter(): Promise<void> {
    let queryBuilder = null;
    if (this.data) {
      queryBuilder = new CriteriaBuilder();
    } else {
      queryBuilder = new ClientCriteriaSingleton().queryBuilder;
    }
    const realEstateFilter: any = this.realEstateForm.getRawValue();
    for (const [k, v] of Object.entries(realEstateFilter)) {
      if (!v) {
        delete realEstateFilter[k];
      }
    }
    // @ts-ignore
    let constructionYear: ConstructionYearDto = {};
    if (
      this.realEstateForm.get(FormControlNames.CONSTRUCTION_YEAR_FORM_CONTROL)
        ?.value
    ) {
      // @ts-ignore
      constructionYear = this.listOfConstructionYears.find(
        (cY) =>
          cY.id ===
          this.realEstateForm.get(
            FormControlNames.CONSTRUCTION_YEAR_FORM_CONTROL
          )?.value
      );
    }
    if (realEstateFilter.areaFrom) {
      realEstateFilter.areaFrom =
        realEstateFilter.areaFrom / realEstateFilter.areaUnit;
    }

    if (realEstateFilter.areaTo) {
      realEstateFilter.areaTo =
        realEstateFilter.areaTo / realEstateFilter.areaUnit;
    }

    if (!this.data) {
      queryBuilder.eq('status', Status.ACTIVE);
    }

    if (realEstateFilter.propertyAreaFrom) {
      realEstateFilter.propertyAreaFrom =
        realEstateFilter.propertyAreaFrom / realEstateFilter.propertyAreaUnit;
    }

    if (realEstateFilter.propertyAreaTo) {
      realEstateFilter.propertyAreaTo =
        realEstateFilter.propertyAreaTo / realEstateFilter.propertyAreaUnit;
    }
    queryBuilder
      .eq('category.id', realEstateFilter.category)
      .and()
      .eq('address.municipality.id', realEstateFilter.municipality)
      .and()
      .eq('heatings.type', realEstateFilter.heatings)
      .and()
      .eq('parkings.type', realEstateFilter.parkings)
      .and()
      .eq('furniture.type', realEstateFilter.furniture)
      .and()
      .eq('saleType', realEstateFilter.saleType)
      .and()
      .eq('address.municipality.city.id', realEstateFilter.city)
      .and();

    if (this.data) {
      queryBuilder.eq('status', realEstateFilter.status);
    }
    queryBuilder.criteriaList = queryBuilder.criteriaList.filter(
      (searchCriteria) =>
        searchCriteria.secondOperand && searchCriteria.secondOperand !== '0'
    );

    if (realEstateFilter.propertyAreaFrom || realEstateFilter.propertyAreaTo) {
      this.makeCriteria('propertyArea', queryBuilder, {
        from: realEstateFilter.propertyAreaFrom,
        to: realEstateFilter.propertyAreaTo,
      });
    }

    if (realEstateFilter.priceFrom || realEstateFilter.priceTo) {
      queryBuilder.criteria((builder) => {
        this.makeCriteria('price', builder, {
          from: realEstateFilter.priceFrom,
          to: realEstateFilter.priceTo,
        });
        builder.or();
        this.makeCriteria('priceDiscounted', builder, {
          from: realEstateFilter.priceFrom,
          to: realEstateFilter.priceTo,
        });
        return builder;
      });
    }

    if (realEstateFilter.areaTo || realEstateFilter.areaFrom) {
      this.makeCriteria('area', queryBuilder, {
        from: realEstateFilter.areaFrom,
        to: realEstateFilter.areaTo,
      });
    }

    if (constructionYear.lt || constructionYear.gt) {
      this.makeCriteria('constructionYear', queryBuilder, {
        from: constructionYear.gt,
        to: constructionYear.lt,
      });
    }

    if (realEstateFilter.structure.length > 0) {
      queryBuilder.criteria((builder) => {
        realEstateFilter.structure.filter((structureId: string) => {
          builder.eq('structure.id', structureId).or();
        });
        return builder;
      });
    }
    if (realEstateFilter.floor.length > 0) {
      queryBuilder.criteria((builder) => {
        realEstateFilter.floor.filter((floor: string) => {
          builder.eq('floor.name', floor.split(' ').join('_')).or();
        });
        return builder;
      });
    }
    if (this.data) {
      localStorage.setItem(ADMIN_FILTER, JSON.stringify(realEstateFilter));
      this.dialogRef.close(queryBuilder.buildUrlEncoded());
    } else {
      localStorage.setItem(CLIENT_FILTER, JSON.stringify(realEstateFilter));
      this.dialogRef.close();
      sessionStorage.removeItem(CURRENT_QUERY);
      await this.router.navigate([Pages.REAL_ESTATE_FILTER_PAGE_ROUTE], {
        queryParams: { q: queryBuilder.buildUrlEncoded() },
      });
    }
  }

  makeCriteria(
    propertyName: string,
    criteriaBuilder: CriteriaBuilder,
    attr: FromTo
  ): CriteriaBuilder {
    criteriaBuilder.criteria((builder) => {
      if (attr.from) {
        builder.criteria((b) => {
          b.gt(propertyName, attr.from).or().eq(propertyName, attr.from);
          return b;
        });
      }
      if (attr.to) {
        builder.and();
        builder.criteria((b) => {
          b.lt(propertyName, attr.to).or().eq(propertyName, attr.to);
          return b;
        });
      }

      return builder;
    });

    return criteriaBuilder;
  }

  async resetForm(): Promise<void> {
    if (this.data) {
      await this.router.navigate([Pages.ADMIN_PANEL_PAGE_ROUTE]);
    } else {
      const queryBuilder = new CriteriaBuilder();
      queryBuilder.eq('saleType', this.realEstateFilter.saleType);
      queryBuilder.eq('status', 'ACTIVE');
      this.realEstateForm.reset();
      localStorage.removeItem(CLIENT_FILTER);
      await this.router.navigate([Pages.REAL_ESTATE_FILTER_PAGE_ROUTE], {
        queryParams: { q: queryBuilder.buildUrlEncoded() },
      });
    }
  }
}
