import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {
  CURRENT_QUERY,
  FormControlNames,
  Pages,
  RestRoutesConst,
} from '../../../const/const';
import { DialogOptions } from '../../../util/dialog-options';
import { DialogUtil } from '../../../util/dialog-util';
import { MatDialog } from '@angular/material/dialog';
import { OfferDialogComponent } from '../offer-dialog/offer-dialog.component';
import { RealEstate } from '../../../models/RealEstate';
import { RealEstateService } from '../../../service/real-estate.service';
import { Router } from '@angular/router';
import { SaleType } from '../../../enums/SaleType';
import { MediaType } from '../../../enums/MediaType';
import { debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import { ClientCriteriaSingleton } from '../../../util/query/ClientCriteriaSingleton';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass'],
})
export class NavbarComponent implements OnInit, AfterViewInit {
  URL_PREFIX = RestRoutesConst.REST_URL;

  listOfRealEstates: RealEstate[] = [];
  searchForm = new FormGroup({
    search: new FormControl(),
  });
  searchText = '';
  urlParams = '';

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private realEstateService: RealEstateService
  ) {}

  ngAfterViewInit(): void {
    this.searchForRealEstate();
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.addListener();
    }, 1000);
  }

  getAllEstatesByUrl(): void {
    this.realEstateService
      .getRealEstateByProperty(this.urlParams)
      .subscribe(async (resp) => {
        this.listOfRealEstates = resp.body;
        for (const realEstate of this.listOfRealEstates) {
          realEstate.createdDate = new Date(realEstate.createdDate);
          realEstate.medias = realEstate.medias?.filter(
            (media) => media.type !== MediaType.YOUTUBE
          );
        }
      });
  }

  addListener(): void {
    document
      .getElementById('search')
      ?.addEventListener('keypress', async (e) => {
        if (e.key === 'Enter') {
          await this.searchOnFilterPage();
        }
      });
  }

  async searchOnSell(): Promise<void> {
    sessionStorage.removeItem(CURRENT_QUERY);
    const query = new ClientCriteriaSingleton().queryBuilder;
    query.eq(
      FormControlNames.SALES_TYPE_FORM_CONTROL,
      SaleType.SALE.toString()
    );
    const reload = window.location.pathname !== '/';
    await this.router.navigate([Pages.REAL_ESTATE_FILTER_PAGE_ROUTE], {
      queryParams: { q: query.buildUrlEncoded() },
    });

    if (reload) {
      location.reload();
    }
  }

  async searchOnRent(): Promise<void> {
    sessionStorage.removeItem(CURRENT_QUERY);
    const query = new ClientCriteriaSingleton().queryBuilder.eq(
      FormControlNames.SALES_TYPE_FORM_CONTROL,
      SaleType.RENT.toString()
    );

    this.router
      .navigate([Pages.REAL_ESTATE_FILTER_PAGE_ROUTE], {
        queryParams: { q: query.buildUrlEncoded() },
      })
      .then(() => {
        if (window.location.pathname !== '/') {
          location.reload();
        }
      });
  }

  searchForRealEstate(): void {
    sessionStorage.removeItem(CURRENT_QUERY);
    this.searchForm
      .get(FormControlNames.SEARCH_FORM_CONTROL)
      .valueChanges.pipe(
        filter((inputValue) => {
          if (inputValue.length > 2) {
            return inputValue;
          } else {
            this.listOfRealEstates = [];
            return '';
          }
        }),
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe((data) => {
        data = data.replace(/\s/g, '_');
        this.urlParams = new ClientCriteriaSingleton().queryBuilder
          .startsWith('code', data)
          .or()
          .startsWith('name', data)
          .or()
          .startsWith('address.municipality.name', data)
          .or()
          .startsWith('address.municipality.city.name', data)
          .buildUrlEncoded();
        this.getAllEstatesByUrl();
      });
  }

  openAddOfferDialog(): void {
    DialogUtil.openDialog(
      OfferDialogComponent,
      DialogOptions.getOptions({}),
      this.dialog
    );
  }

  async navigateToEstate(id): Promise<void> {
    await this.router.navigate([
      Pages.REAL_ESTATE_DETAIL_PAGE_ROUTE + `/${id}`,
    ]);
  }

  async searchOnFilterPage(): Promise<void> {
    await this.router.navigate([Pages.REAL_ESTATE_FILTER_PAGE_ROUTE], {
      queryParams: { q: this.urlParams },
    });
    if (
      window.location.pathname ===
      '/' + Pages.REAL_ESTATE_FILTER_PAGE_ROUTE
    ) {
      location.reload();
    }
  }

  openOrCloseMenu(): void {
    // @ts-ignore
    const navbarContent = document.getElementById('navbarNav');

    // @ts-ignore
    if (navbarContent.classList.contains('show')) {
      // @ts-ignore
      navbarContent.classList.remove('show');

      // @ts-ignore
      document.querySelector('.fa-angle-left').style.transform = 'rotate(0deg)';
    } else {
      // @ts-ignore
      navbarContent.classList.add('show');

      // @ts-ignore
      document.querySelector('.fa-angle-left').style.transform =
        'rotate(-90deg)';
    }
  }
}
