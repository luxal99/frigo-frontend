import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-real-real-estate-filter-page-header-tool',
  templateUrl: './real-estate-filter-page-header-tool.component.html',
  styleUrls: ['./real-estate-filter-page-header-tool.component.sass'],
})
export class RealEstateFilterPageHeaderToolComponent implements OnInit {
  @Input() realEstateListSize: number;

  @Output() openAdvancedSearchDialog = new EventEmitter();
  @Output() sortByLowestPrice = new EventEmitter();
  @Output() sortByHighestPrice = new EventEmitter();
  @Output() sortByDateUp = new EventEmitter();
  @Output() sortByDateDown = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}
}
