import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-property-binding',
  templateUrl: './property-binding.component.html',
  styleUrls: ['./property-binding.component.sass'],
})
export class PropertyBindingComponent implements OnInit {
  @Input() key = '';
  @Input() value: any;
  @Input() propertyName = 'type';
  @Input() currency = false;
  @Input() titleCase = false;

  secondValue = '';

  constructor() {}

  ngOnInit(): void {
    if (this.value instanceof Array) {
      if (this.value.length === 0) {
        delete this.value;
      } else {
        this.value.filter(
          (item, index) =>
            (this.secondValue +=
              (item[this.propertyName].toUpperCase() === 'PVC' ||
              item[this.propertyName].toUpperCase() === 'CG'
                ? item[this.propertyName].toUpperCase()
                : index === 0
                ? item[this.propertyName][0].toUpperCase() +
                  item[this.propertyName].slice(1)
                : item[this.propertyName].toLowerCase()) +
              (index === this.value.length - 1 ? '' : ', '))
        );
      }
    }
  }
}
