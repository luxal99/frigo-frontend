import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../../service/auth.service';
import { FormControl, FormGroup } from '@angular/forms';
import { FieldConfig } from '../../../models/FIeldConfig';
import {
  FormControlNames,
  InputTypes,
  Pages,
  SnackBarMessages,
  TokenConst,
} from '../../../const/const';
import { User } from '../../../models/User';
import { SnackbarUtil } from '../../../util/snackbar-util';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { SpinnerService } from '../../../service/spinner.service';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthGuard } from '../../../guards/auth.guard';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass'],
})
export class LoginComponent implements OnInit {
  @ViewChild('spinner') spinner: MatProgressSpinner;

  usernameInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.USERNAME_NAME_FORM_CONTROL,
  };
  passwordInputConfig: FieldConfig = {
    type: InputTypes.PASSWORD_TYPE_NAME,
    name: FormControlNames.PASSWORD_NAME_FORM_CONTROL,
  };

  loginForm = new FormGroup({
    username: new FormControl(),
    password: new FormControl(),
  });

  constructor(
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private authGuard: AuthGuard,
    private router: Router,
    private spinnerService: SpinnerService
  ) {}

  ngOnInit(): void {
    this.addListener();
  }

  addListener(): void {
    document.getElementById('username')?.addEventListener('keydown', (e) => {
      if (e.key === 'Enter') {
        this.auth();
      }
    });

    document.getElementById('password')?.addEventListener('keydown', (e) => {
      if (e.key === 'Enter') {
        this.auth();
      }
    });
  }

  auth(): void {
    this.spinnerService.show(this.spinner);
    const user: User = this.loginForm.getRawValue();
    this.authService.auth(user).subscribe(
      async (token) => {
        sessionStorage.setItem(TokenConst.TOKEN_NAME, token);
        this.authGuard.addAuthUser();
        this.authService.setIsAdminRoute(true);
        await this.router.navigate([Pages.ADMIN_PANEL_PAGE_ROUTE]);
        this.spinnerService.hide(this.spinner);
      },
      (err: HttpErrorResponse) => {
        if (err.status === 401) {
          this.spinnerService.hide(this.spinner);
          SnackbarUtil.openSnackBar(
            this.snackBar,
            SnackBarMessages.BAD_CREDENTIALS_MESSAGE
          );
        } else {
          this.spinnerService.hide(this.spinner);
          SnackbarUtil.openSnackBar(
            this.snackBar,
            SnackBarMessages.ERR_MESSAGE
          );
        }
      }
    );
  }
}
