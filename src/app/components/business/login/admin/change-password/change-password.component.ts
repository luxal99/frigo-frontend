import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../../../../service/user.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FieldConfig } from '../../../../../models/FIeldConfig';
import {
  FormControlNames,
  InputTypes,
  Regex,
  SnackBarMessages,
} from '../../../../../const/const';
import { AuthGuard } from '../../../../../guards/auth.guard';
import { ChangePasswordDTO } from '../../../../../models/ChangePasswordDTO';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarUtil } from '../../../../../util/snackbar-util';
import { SpinnerService } from '../../../../../service/spinner.service';
import { MatProgressSpinner } from '@angular/material/progress-spinner';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.sass'],
})
export class ChangePasswordComponent implements OnInit {
  @ViewChild('spinner') spinner: MatProgressSpinner;
  changePasswordForm = new FormGroup({
    password: new FormControl('', Validators.pattern(Regex.PASSWORD_REGEX)),
    confirm: new FormControl('', Validators.pattern(Regex.PASSWORD_REGEX)),
    previous: new FormControl('', Validators.required),
  });

  passwordInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.PASSWORD_NAME_FORM_CONTROL,
  };
  confirmPasswordInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.CONFIRM_PASSWORD_FORM_CONTROL,
  };
  previousInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.PREVIOUS_PASSWORD_FORM_CONTROL,
  };

  constructor(
    private userService: UserService,
    private authGuard: AuthGuard,
    private snackBar: MatSnackBar,
    private spinnerService: SpinnerService
  ) {}

  ngOnInit(): void {}

  changePassword(): void {
    this.spinnerService.show(this.spinner);
    const changePasswordDTO: ChangePasswordDTO =
      this.changePasswordForm.getRawValue();
    changePasswordDTO.valid =
      changePasswordDTO.confirm === changePasswordDTO.password;

    if (changePasswordDTO.valid) {
      this.userService.changePassword(changePasswordDTO).subscribe(
        (resp) => {
          SnackbarUtil.openSnackBar(
            this.snackBar,
            SnackBarMessages.SUCCESS_MESSAGE
          );
          this.spinnerService.hide(this.spinner);
        },
        () => {
          SnackbarUtil.openSnackBar(
            this.snackBar,
            SnackBarMessages.ERR_MESSAGE
          );
          this.spinnerService.hide(this.spinner);
        }
      );
    }
  }
}
