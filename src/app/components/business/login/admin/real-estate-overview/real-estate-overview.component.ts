import {
  AfterViewInit,
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogUtil } from '../../../../../util/dialog-util';
import { AddRealEstateDialogComponent } from './add-real-estate-dialog/add-real-estate-dialog.component';
import { RealEstate } from '../../../../../models/RealEstate';
import { RealEstateService } from '../../../../../service/real-estate.service';
import {
  CURRENT_QUERY,
  FormControlNames,
  Headers,
  PAGE_INCREMENT,
  RestRoutesConst,
} from '../../../../../const/const';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MediaType } from '../../../../../enums/MediaType';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { SpinnerService } from '../../../../../service/spinner.service';
import { AdvancedSearchDialogComponent } from '../../../../common/advanced-search-dialog/advanced-search-dialog.component';
import { ActivatedRoute } from '@angular/router';
import { PageFilter } from '../../../../../models/PageFilter';
import { CurrentQuery } from '../../../../../models/current-query';
import { RealEstateOverviewDialogComponent } from './real-estate-overview-dialog/real-estate-overview-dialog.component';
import { DialogOptions } from '../../../../../util/dialog-options';
import { CriteriaBuilder } from '../../../../../util/query/CriteriaBuilder';
import { CustomPaginatorComponent } from '../../../../common/custom-paginator/custom-paginator.component';
import { debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import { PaginatorService } from '../../../../common/custom-paginator/service/paginator.service';

@Component({
  selector: 'app-real-estate-overview',
  templateUrl: './real-estate-overview.component.html',
  styleUrls: ['./real-estate-overview.component.sass'],
})
export class RealEstateOverviewComponent implements OnInit, AfterViewInit {
  @ViewChild('target', { read: ViewContainerRef, static: false })
  entry: ViewContainerRef;

  @ViewChild('spinner', { static: false }) spinner: MatProgressSpinner;

  @ViewChild('emptyList', { read: ViewContainerRef, static: false })
  emptyList: ViewContainerRef;

  @ViewChild(CustomPaginatorComponent)
  paginator: CustomPaginatorComponent;

  advancedSearch = '';

  listOfRealEstates: RealEstate[] = [];
  URL_PREFIX = RestRoutesConst.REST_URL;

  searchForm = new FormGroup({
    search: new FormControl(''),
  });
  searchText = '';

  isDateDescending = false;
  isPriceDescending = undefined;

  initPage = 0;
  pageIncrement = PAGE_INCREMENT;
  currentQuery!: CurrentQuery;
  gap = 3;

  pageFilter!: PageFilter;

  urlParams = '';

  constructor(
    private dialog: MatDialog,
    private realEstateService: RealEstateService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private spinnerService: SpinnerService,
    public paginatorService: PaginatorService
  ) {}

  ngAfterViewInit(): void {
    this.searchForRealEstate();
  }

  ngOnInit(): void {}

  getAllEstates(page?: number): void {
    if (page >= 0) {
      this.spinnerService.show(this.spinner);
      sessionStorage.setItem(
        CURRENT_QUERY,
        JSON.stringify({ page, urlParams: this.urlParams })
      );
    }
    this.realEstateService
      .getRealEstateByProperty(
        this.urlParams,
        this.choosePage(page),
        this.isDateDescending,
        this.isPriceDescending
      )
      .subscribe(async (resp) => {
        this.paginatorService.setPageFilter(
          new PageFilter(
            // tslint:disable-next-line:radix
            Number.parseInt(resp.headers.get(Headers.X_PAGE)),
            // tslint:disable-next-line:radix
            Number.parseInt(resp.headers.get(Headers.X_PAGE_COUNT)),
            // tslint:disable-next-line:radix
            Number.parseInt(resp.headers.get(Headers.X_DATA_COUNT))
          )
        );
        this.listOfRealEstates = resp.body;
        for (const realEstate of this.listOfRealEstates) {
          realEstate.createdDate = new Date(realEstate.createdDate);
          realEstate.medias = realEstate.medias?.filter(
            (media) => media.type !== MediaType.YOUTUBE
          );
        }
        this.spinnerService.hide(this.spinner);
      });
  }

  openAddEstateDialog(): void {
    DialogUtil.openDialog(
      AddRealEstateDialogComponent,
      {
        maxWidth: '100vw',
        maxHeight: '100vh',
        height: '100%',
        width: '100%',
      },
      this.dialog
    )
      .afterClosed()
      .subscribe(() => {
        this.getAllEstates();
      });
  }

  openRealEstateOverview(realEstate: RealEstate): void {
    DialogUtil.openDialog(
      RealEstateOverviewDialogComponent,
      DialogOptions.getOptions(realEstate),
      this.dialog
    );
  }

  sortByLowestPrice(): void {
    this.setPriceAndDateFilter(true, undefined);
    this.spinnerService.show(this.spinner);
    this.paginator.resetPaginator();
    this.getAllEstates();
  }

  sortByHighestPrice(): void {
    this.setPriceAndDateFilter(false, undefined);
    this.spinnerService.show(this.spinner);
    this.paginator.resetPaginator();
    this.getAllEstates();
  }

  sortByDateUp(): void {
    this.setPriceAndDateFilter(undefined, true);
    this.spinnerService.show(this.spinner);
    this.paginator.resetPaginator();
    this.getAllEstates();
  }

  sortByDateDown(): void {
    this.setPriceAndDateFilter(undefined, false);
    this.spinnerService.show(this.spinner);
    this.paginator.resetPaginator();
    this.getAllEstates();
  }

  openAdvancedSearchDialog(): void {
    DialogUtil.openDialog(
      AdvancedSearchDialogComponent,
      {
        position: { right: '2%' },
        minWidth: '30%',
        height: '90vh',
        data: true,
      },
      this.dialog
    )
      .afterClosed()
      .subscribe((result: string) => {
        this.urlParams = result;
        this.getAllEstates(0);
      });
  }

  nextPage(): void {
    this.initPage += this.gap;
    this.pageIncrement += this.gap;
  }

  previousPage(): void {
    if (this.initPage - this.gap >= 0) {
      this.initPage -= this.gap;
      this.pageIncrement -= this.gap;
    }
  }

  choosePage(page: any): any {
    // @ts-ignore
    this.currentQuery = JSON.parse(sessionStorage.getItem(CURRENT_QUERY));
    // @ts-ignore
    if (page) {
      return page;
    } else if (this.currentQuery) {
      setTimeout(() => {
        this.paginator.initPage = this.currentQuery.page;
        this.paginator.pageIncrement = this.currentQuery.page + this.gap;
      }, 200);
      return this.currentQuery.page;
    } else {
      return 0;
    }
  }

  setPriceAndDateFilter(price: any, date: any): void {
    this.isPriceDescending = price;
    this.isDateDescending = date;
  }

  searchForRealEstate(): void {
    this.searchForm
      .get(FormControlNames.SEARCH_FORM_CONTROL)
      .valueChanges.pipe(
        filter((inputValue) => {
          if (inputValue.length > 2) {
            return inputValue;
          } else {
            this.getAllEstates();
            return '';
          }
        }),
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe((data: string) => {
        data = data.replace(/\s/g, '_');
        this.urlParams = new CriteriaBuilder()
          .startsWith('code', data)
          .or()
          .startsWith('name', this.searchText)
          .or()
          .startsWith('address.municipality.name', data)
          .or()
          .startsWith('address.municipality.city.name', data)
          .or()
          .startsWith('address.street', data)
          .or()
          .startsWith('contactPerson.person.firstName', data.split('_')[0])
          .or()
          .startsWith('owner.person.firstName', data.split('_')[0])
          .or()
          .startsWith('owner.person.lastName', data.split('_')[1])
          .or()
          .startsWith('contactPerson.person.lastName', data.split('_')[1])
          .buildUrlEncoded();
        this.searchForRealEstatesCallBack();
      });
  }

  searchForRealEstatesCallBack(): void {
    this.paginator.resetPaginator();
    this.spinnerService.show(this.spinner);
    this.getAllEstates();
    sessionStorage.removeItem(CURRENT_QUERY);
  }
}
