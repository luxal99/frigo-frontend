import { Component, Inject, OnInit } from '@angular/core';
import { RealEstate } from '../../../../../../models/RealEstate';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RestRoutesConst } from '../../../../../../const/const';
import { MediaType } from '../../../../../../enums/MediaType';
import SwiperCore, { Navigation, Pagination, Virtual } from 'swiper';
import { Media } from '../../../../../../models/Media';
import { RealEstateService } from '../../../../../../service/real-estate.service';

SwiperCore.use([Virtual, Navigation, Pagination]);

@Component({
  selector: 'app-real-real-estate-overview-dialog',
  templateUrl: './real-estate-overview-dialog.component.html',
  styleUrls: ['./real-estate-overview-dialog.component.sass'],
})
export class RealEstateOverviewDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: RealEstate,
    private realEstateService: RealEstateService
  ) {}

  youtubeVideo: Media;

  URL_PREFIX = RestRoutesConst.REST_URL;

  ngOnInit(): void {
    this.findRealEstate();
  }

  findRealEstate(): void {
    this.realEstateService.findById(this.data.id).subscribe((resp) => {
      this.data = resp;

      this.youtubeVideo = this.data.medias?.find(
        (media) => media.type === MediaType.YOUTUBE.toString()
      );
      this.data.medias = this.data.medias?.filter(
        (media) => media.type !== MediaType.YOUTUBE.toString()
      );
    });
  }
}
