import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { CategoryService } from '../../../../../../service/category.service';
import { HeatingService } from '../../../../../../service/heating.service';
import { Heating } from '../../../../../../models/Heating';
import { FloorService } from '../../../../../../service/floor.service';
import { FieldConfig } from '../../../../../../models/FIeldConfig';
import {
  AREA_UNIT_FORM_CONTROL,
  CADASTRE_NUMBER_FORM_CONTROL,
  CategoryValues,
  FormControlNames,
  InputTypes,
  PROPERTY_AREA_UNIT_FORM_CONTROL,
  RENOVATED_DATE_FORM_CONTROL,
  RestRoutesConst,
  SnackBarMessages,
} from 'src/app/const/const';
import * as ClassicEditor from 'lib/ckeditor5-build-classic';
import { CKEditorComponent } from '@ckeditor/ckeditor5-angular';
import { Parking } from '../../../../../../models/Parking';
import { ParkingService } from '../../../../../../service/parking.service';
import { FurnitureService } from '../../../../../../service/furniture.service';
import { FiledStatusService } from '../../../../../../service/filed-status.service';
import { PriceTypeService } from '../../../../../../service/price-type.service';
import { RealEstateService } from '../../../../../../service/real-estate.service';
import { StructureService } from '../../../../../../service/structure.service';
import { PaymentPeriodService } from '../../../../../../service/payment-period.service';
import { Status } from '../../../../../../enums/Status';
import { RealEstate } from '../../../../../../models/RealEstate';
import { PriceDisplayType } from '../../../../../../enums/PriceDisplayType';
import { SnackbarUtil } from '../../../../../../util/snackbar-util';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MediaService } from '../../../../../../service/media.service';
import { MunicipalityService } from '../../../../../../service/municipality.service';
import { ContactType } from '../../../../../../models/ContactType';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { ExtraInfo } from '../../../../../../models/ExtraInfo';
import { ExtraInfoService } from '../../../../../../service/extra-info.service';
import { MediaType } from '../../../../../../enums/MediaType';
import { AgentsService } from '../../../../../../service/agents.service';
import { SpinnerService } from '../../../../../../service/spinner.service';
import { AuthGuard } from '../../../../../../guards/auth.guard';
import { AreaUnitService } from '../../../../../../service/area-unit.service';
import { NavigationMenu } from '../../../../../../models/NavigationMenu';
import { CarpentryService } from '../../../../../../service/carpentry.service';
import { Carpentry } from '../../../../../../models/Carpentry';
import { ExtraRoomService } from '../../../../../../service/extra-room.service';
import { ExtraRoom } from '../../../../../../models/ExtraRoom';
import { MatTab } from '@angular/material/tabs';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Media } from '../../../../../../models/Media';
import SwiperCore, { Navigation, Virtual } from 'swiper';
import { Contact } from '../../../../../../models/Contact';
import * as moment from 'moment';
import { City } from '../../../../../../models/City';
import { CityService } from '../../../../../../service/city.service';

SwiperCore.use([Virtual, Navigation]);

@Component({
  selector: 'app-add-real-estate-dialog',
  templateUrl: './add-real-estate-dialog.component.html',
  styleUrls: ['./add-real-estate-dialog.component.sass'],
})
export class AddRealEstateDialogComponent
  implements OnInit, AfterViewChecked, AfterViewInit
{
  navigationMenu: NavigationMenu[] = [
    { title: 'Informacije o nekrentini', navigateTo: 'realEstateInfo' },
    { title: 'Informacije o zemljištu', navigateTo: 'landInfo' },
    { title: 'Troškovi', navigateTo: 'expensesInfo' },
    { title: 'Uslovi zakupa', navigateTo: 'termOfLease' },
    { title: 'Opremljenost nekretnine', navigateTo: 'equipment' },
    { title: 'Opremljenost objekta', navigateTo: 'objectEquipment' },
  ];
  locationAndContactNavMenu: NavigationMenu[] = [
    { title: 'Informacije o vlasniku', navigateTo: 'ownerInfo' },
    { title: 'Lokacija', navigateTo: 'location' },
    { title: 'Agenti', navigateTo: 'agent' },
  ];

  ownerTelephone: Contact;
  ownerEmail: Contact;
  contactPersonTelephone: Contact;
  contactPersonEmail: Contact;
  youtubeLink = '';

  isPropertyAreaRequired = false;
  // @ts-ignore
  listOfImages: Media[] = [];

  mediaNavMenu: NavigationMenu[] = [
    { title: 'Slike', navigateTo: 'picture' },
    { title: 'Video', navigateTo: 'video' },
  ];
  private token: string;

  lat = 44.787197;
  lng = 20.457273;

  zoom = 10;

  draggedImage: Media = {};
  fileUploadIndex = 0;
  @ViewChild('spinner') spinner: MatProgressSpinner;

  @ViewChild('characteristicTab') characteristicTab: MatTab;
  @ViewChild('mediaTab') mediaTab: MatTab;
  @ViewChild('locationTab') locationTab: MatTab;

  @ViewChild('editor', { static: false }) editorComponent: CKEditorComponent;
  public Editor = ClassicEditor;
  @ViewChild('whatsRenovatedEditor', { static: false })
  whatsRenovatedEditor: CKEditorComponent;
  public WhatsRenovatedEditor = ClassicEditor;

  listOfHeatings: Heating[] = [];
  listOfParkings: Parking[] = [];
  listOfExtraInfos: ExtraInfo[] = [];
  listOfAgents: any[] = [];
  listOfCarpentries: Carpentry[] = [];
  listOfExtraRooms: ExtraRoom[];
  listOfCities: City[] = [];
  fileUploadList: Array<any> = [];

  //region -- BasicInfoTab --
  nameInputConfig: FieldConfig = {
    name: FormControlNames.NAME_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  codeInputConfig: FieldConfig = {
    name: FormControlNames.CODE_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  categorySelectConfig: FieldConfig = {
    name: FormControlNames.CATEGORY_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };
  priceInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.PRICE_FORM_CONTROL,
  };
  priceDiscountedInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.PRICE_DISCOUNTED_FORM_CONTROL,
  };
  priceDisplayTypeSelectConfig: FieldConfig = {
    type: InputTypes.SELECT_TYPE_NAME,
    name: FormControlNames.PRICE_DISPLAY_TYPE_FORM_CONTROL,
    options: [
      { name: PriceDisplayType.TOTAL },
      { name: PriceDisplayType.PER_UNIT },
    ],
  };
  priceTypeSelectConfig: FieldConfig = {
    type: InputTypes.SELECT_TYPE_NAME,
    name: FormControlNames.PRICE_TYPE_FORM_CONTROL,
  };
  saleTypeSelectConfig: FieldConfig = {
    name: FormControlNames.SALES_TYPE_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };
  statusSelectConfig: FieldConfig = {
    name: FormControlNames.STATUS_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
    options: [
      { name: Status.ACTIVE },
      { name: Status.INACTIVE },
      { name: Status.SOLD_RENTED },
    ],
  };
  //endregion

  // region -- CharacteristicTab --
  floorSelectConfig: FieldConfig = {
    name: FormControlNames.FLOOR_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };
  bathroomCountInputConfig: FieldConfig = {
    name: FormControlNames.BATHROOM_COUNT_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  bedCountInputConfig: FieldConfig = {
    name: FormControlNames.BED_COUNT_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  bedroomCountInputConfig: FieldConfig = {
    name: FormControlNames.BEDROOM_COUNT_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  areaInputConfig: FieldConfig = {
    name: FormControlNames.AREA_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  areaUnitSelectInputConfig: FieldConfig = {
    name: AREA_UNIT_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };
  propertyAreaUnitSelectInputConfig: FieldConfig = {
    name: PROPERTY_AREA_UNIT_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };
  longitudeInputConfig: FieldConfig = {
    name: FormControlNames.LONGITUDE_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  latitudeInputConfig: FieldConfig = {
    name: FormControlNames.LATITUDE_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  buildingFloorsInputConfig: FieldConfig = {
    name: FormControlNames.BUILDING_FLOORS_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  centerDistanceInputConfig: FieldConfig = {
    name: FormControlNames.CENTER_DISTANCE_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  ceilingHeightInputConfig: FieldConfig = {
    name: FormControlNames.CEILING_HEIGHT_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  constructionYearInputConfig: FieldConfig = {
    name: FormControlNames.CONSTRUCTION_YEAR_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  propertyAreaInputConfig: FieldConfig = {
    name: FormControlNames.PROPERTY_AREA_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  roomCountInputConfig: FieldConfig = {
    name: FormControlNames.ROOM_COUNT_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  cadastreNumberInputConfig: FieldConfig = {
    name: CADASTRE_NUMBER_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  filedAreaInputConfig: FieldConfig = {
    name: FormControlNames.FILED_AREA_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  filedStatusSelectConfig: FieldConfig = {
    name: FormControlNames.FILED_STATUS_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };
  furnitureStatusSelectConfig: FieldConfig = {
    name: FormControlNames.FURNITURE_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };
  structureSelectConfig: FieldConfig = {
    name: FormControlNames.STRUCTURE_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };
  youtubeLinkInputConfig: FieldConfig = {
    name: FormControlNames.YOUTUBE_LINK_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  termsPaymentPeriodSelectConfig: FieldConfig = {
    name: FormControlNames.TERMS_PAYMENT_PERIOD_FORM_CONTROL,
    type: InputTypes.SELECT_TYPE_NAME,
  };

  termsDepositInputConfig: FieldConfig = {
    name: FormControlNames.TERMS_DEPOSIT_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  termsMinimumLeaseInputConfig: FieldConfig = {
    name: FormControlNames.TERMS_MINIMUM_LEASE_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  wcCountInputConfig: FieldConfig = {
    name: FormControlNames.WC_COUNT_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  //endregion

  //region -- AddressAndContactFormConfig --
  streetInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.STREET_FORM_CONTROL,
  };
  municipalitySelectConfig: FieldConfig = {
    type: InputTypes.SELECT_TYPE_NAME,
    name: FormControlNames.MUNICIPALITY_FORM_CONTROL,
  };
  streetNoInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.NUMBER_FORM_CONTROL,
  };
  firstNameInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.FIRST_NAME_FORM_CONTROL,
  };
  lastNameInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.LAST_NAME_FORM_CONTROL,
  };
  telephoneInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.TELEPHONE_FORM_CONTROL,
  };
  emailInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.EMAIL_FORM_CONTROL,
  };
  //endregion

  //region -- ExpensesForm --
  expensesCableInputConfig: FieldConfig = {
    name: FormControlNames.EXPENSES_CABLE_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  expensesElectricityInputConfig: FieldConfig = {
    name: FormControlNames.EXPENSES_ELECTRICITY_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  expensesInfostanInputConfig: FieldConfig = {
    name: FormControlNames.EXPENSES_INFOSTAN_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  expensesInternetInputConfig: FieldConfig = {
    name: FormControlNames.EXPENSES_INTERNET_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  expensesMaintenanceInputConfig: FieldConfig = {
    name: FormControlNames.EXPENSES_MAINTENANCE_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  expensesPhoneInputConfig: FieldConfig = {
    name: FormControlNames.EXPENSES_PHONE_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  expensesTaxesInputConfig: FieldConfig = {
    name: FormControlNames.EXPENSES_TAXES_FORM_CONTROL,
    type: InputTypes.INPUT_TYPE_NAME,
  };
  //endregion

  //region --Forms--

  basicInfoForm = new FormGroup({
    category: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required),
    priceDiscounted: new FormControl(),
    priceDisplayType: new FormControl('', Validators.required),
    priceType: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    code: new FormControl('', Validators.required),
    saleType: new FormControl('', Validators.required),
    status: new FormControl('', Validators.required),
    propertyArea: new FormControl(),
    propertyAreaUnit: new FormControl(),
  });

  characteristicsForm = new FormGroup({
    floor: new FormControl(''),
    heating: new FormControl(),
    area: new FormControl('', Validators.required),
    areaUnit: new FormControl('', Validators.required),
    bathroomCount: new FormControl(),
    bedCount: new FormControl(),
    bedroomCount: new FormControl(),
    buildingFloors: new FormControl(),
    centerDistance: new FormControl(),
    agents: new FormControl(),
    structure: new FormControl(''),
    filedArea: new FormControl(),
    furniture: new FormControl(),
    moveIn: new FormControl(),
    moveInDate: new FormControl(),
    ceilingHeight: new FormControl(),
    wcCount: new FormControl(),
    buildingIntercom: new FormControl(),
    buildingSecurity: new FormControl(),
    constructionYear: new FormControl(),
    expensesCable: new FormControl(),
    expensesElectricity: new FormControl(),
    expensesInfostan: new FormControl(),
    expensesInternet: new FormControl(),
    expensesMaintenance: new FormControl(),
    expensesPhone: new FormControl(),
    expensesTaxes: new FormControl(),
    extraInfos: new FormControl(''),
    filedStatus: new FormControl(),
    propertyElectricity: new FormControl(),
    propertyFenced: new FormControl(),
    propertyGas: new FormControl(),
    propertyWater: new FormControl(),
    roomCount: new FormControl(),
    termsDeposit: new FormControl(),
    termsMinimumLease: new FormControl(),
    termsPaymentPeriod: new FormControl(''),
    termsPets: new FormControl(),
    buildingAccessibility: new FormControl(),
    propertyArea: new FormControl(),
    heatings: new FormControl(),
    propertyAreaUnit: new FormControl(),
    renovatedDate: new FormControl(''),
    extraRooms: new FormControl(),
    parkings: new FormControl(),
    longitude: new FormControl(),
    latitude: new FormControl(),
    cadastreNumber: new FormControl(),
    carpentries: new FormControl(),
    renovated: new FormControl(),
    buildingCamera: new FormControl(),
    buildingElevator: new FormControl(),
    duplex: new FormControl(),
    pinned: new FormControl(),
  });

  mediaForm = new FormGroup({
    youtubeLink: new FormControl(''),
  });

  addressForm = new FormGroup({
    street: new FormControl('', Validators.required),
    number: new FormControl('', Validators.required),
    municipality: new FormControl('', Validators.required),
  });

  ownerForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    telephone: new FormControl('', Validators.required),
    email: new FormControl(),
    agentNote: new FormControl(''),
    lastName: new FormControl('', Validators.required),
  });

  contactPersonForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    telephone: new FormControl(),
    email: new FormControl(),
    lastName: new FormControl('', Validators.required),
  });

  uploadForm: FormGroup;

  //endregion
  URL_PREFIX = RestRoutesConst.REST_URL;
  isFloatingBtnDisabled = true;
  private uploadArraySize: number;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: RealEstate,
    private categoryService: CategoryService,
    private floorService: FloorService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private areaUnitService: AreaUnitService,
    private agentService: AgentsService,
    private spinnerService: SpinnerService,
    private mediaService: MediaService,
    private extraInfoService: ExtraInfoService,
    private realEstateService: RealEstateService,
    private structureService: StructureService,
    private paymentPeriodService: PaymentPeriodService,
    private authGuard: AuthGuard,
    private filedStatusService: FiledStatusService,
    private priceTypeService: PriceTypeService,
    private parkingService: ParkingService,
    private furnitureService: FurnitureService,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private cityService: CityService,
    private carpentryService: CarpentryService,
    private extraRoomService: ExtraRoomService,
    private heatingService: HeatingService,
    private municipalityService: MunicipalityService
  ) {}

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.setValuesToForm();
      this.getAllCategories();
      this.getAllHeating();
      this.getAllFloors();
      this.getAllCities();
      this.getAllParkings();
      this.getAllFurnitures();
      this.getAllFiledStatuses();
      this.getAllPriceTypes();
      this.getAllStructures();
      this.getAllSalesTypes();
      this.getAllPaymentPeriods();
      this.getAllExtraInfos();
      this.initForm();
      this.getAllAgents();
      this.getAllAreaUnits();
      this.getAllCarpentries();
      this.getAllExtraRooms();
      this.getMedias();
    }, 100);
  }

  ngOnInit(): void {}

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  setValuesToForm(): void {
    if (!this.data) {
      // @ts-ignore
      this.data = null;
      this.spinnerService.hide(this.spinner);
    } else {
      this.realEstateService.findById(this.data.id).subscribe((resp) => {
        this.data = resp;

        this.basicInfoForm
          .get(FormControlNames.STATUS_FORM_CONTROL)
          .setValue(resp.status);
        if (resp.description) {
          this.editorComponent.editorInstance.setData(resp.description);
        }

        if (resp.whatsRenovated) {
          this.whatsRenovatedEditor.editorInstance.setData(resp.whatsRenovated);
        }
        this.isFloatingBtnDisabled = false;
        this.basicInfoForm
          .get(FormControlNames.SALES_TYPE_FORM_CONTROL)
          ?.setValue(this.data.saleType);
        this.basicInfoForm
          .get(FormControlNames.PRICE_DISPLAY_TYPE_FORM_CONTROL)
          ?.setValue(this.data.priceDisplayType);
        // @ts-ignore
        this.ownerTelephone = this.data.owner.person?.contacts?.find(
          (contact) => contact.type === ContactType.PHONE.toString()
        );
        // @ts-ignore
        this.ownerEmail = this.data.owner.person?.contacts?.find(
          (contact) => contact.type === ContactType.EMAIL.toString()
        );

        // @ts-ignore
        this.contactPersonTelephone =
          this.data.contactPerson.person?.contacts?.find(
            (contact) => contact.type === ContactType.PHONE.toString()
          );
        // @ts-ignore
        this.contactPersonEmail =
          this.data.contactPerson.person?.contacts?.find(
            (contact) => contact.type === ContactType.EMAIL.toString()
          );

        this.ownerForm
          .get(FormControlNames.AGENT_NOTE_FORM_CONTROL)
          ?.setValue(this.data.owner?.agentNote);
        // @ts-ignore
        const youtubeMedia: Media = this.data.medias?.find(
          (media) => media.type === MediaType.YOUTUBE.toString()
        );
        if (youtubeMedia) {
          // @ts-ignore
          this.youtubeLink = youtubeMedia.uri;
        }

        this.characteristicsForm
          .get(RENOVATED_DATE_FORM_CONTROL)
          ?.setValue(this.data.renovatedDate || '');
        this.characteristicsForm
          .get(FormControlNames.MOVE_IN_DATE_FORM_CONTROL)
          ?.setValue(this.data.moveInDate || '');

        this.spinnerService.hide(this.spinner);
      });
    }
  }

  validatePropertyArea(): void {
    let categoryValue: string = this.basicInfoForm.get(
      FormControlNames.CATEGORY_FORM_CONTROL
    )?.value.name;

    categoryValue = categoryValue.split(' ').join('_').toUpperCase();
    const requiredGroup: string[] = [
      CategoryValues.BUILDING_LAND,
      CategoryValues.AGRICULTURAL_LAND,
      CategoryValues.COTTAGE,
      CategoryValues.HOUSE,
      CategoryValues.AGRICULTURAL_LAND,
      CategoryValues.RURAL_HOUSEHOLD,
    ];

    this.isPropertyAreaRequired = !!requiredGroup.find(
      (item) => item === categoryValue
    );
  }

  changeColor(e: any): void {
    const element = document.querySelectorAll('.active');
    [].forEach.call(element, (el: any) => {
      el.classList.remove('active');
      el.classList.add('inactive');
    });
    e.target.classList.remove('inactive');
    e.target.className = 'active';
  }

  initForm(): void {
    this.uploadForm = this.formBuilder.group({
      file: [''],
    });
  }

  getAllAreaUnits(): void {
    this.areaUnitService.getAll().subscribe((resp) => {
      this.areaUnitSelectInputConfig.options = resp;
      this.propertyAreaUnitSelectInputConfig.options = resp;
    });
  }

  getAllCities(): void {
    this.cityService.getAll().subscribe((resp) => {
      this.listOfCities = resp;
    });
  }

  getAllAgents(): void {
    this.agentService.getAll().subscribe((agents) => {
      this.listOfAgents = agents.map((agent) => ({
        id: agent.id,
        fullName: agent.person.firstName + ' ' + agent.person.lastName,
      }));
    });
  }

  getAllCategories(): void {
    this.categoryService.getAll().subscribe((resp) => {
      this.categorySelectConfig.options = resp;
    });
  }

  getAllFloors(): void {
    this.floorService.getAll().subscribe((resp) => {
      this.floorSelectConfig.options = resp;
    });
  }

  getMedias(): void {
    if (this.data.id) {
      this.realEstateService.getAllMedia(this.data.id).subscribe((resp) => {
        this.listOfImages = resp;
      });
    }
  }

  getAllHeating(): void {
    this.heatingService.getAll().subscribe((resp) => {
      this.listOfHeatings = resp;
    });
  }

  getAllParkings(): void {
    this.parkingService.getAll().subscribe((resp) => {
      this.listOfParkings = resp;
    });
  }

  getAllFurnitures(): void {
    this.furnitureService.getAll().subscribe((resp) => {
      this.furnitureStatusSelectConfig.options = resp;
    });
  }

  getAllExtraRooms(): void {
    this.extraRoomService.getAll().subscribe((resp) => {
      this.listOfExtraRooms = resp;
    });
  }

  getAllCarpentries(): void {
    this.carpentryService.getAll().subscribe((resp) => {
      this.listOfCarpentries = resp;
    });
  }

  getAllFiledStatuses(): void {
    this.filedStatusService.getAll().subscribe((resp) => {
      this.filedStatusSelectConfig.options = resp;
    });
  }

  getAllPriceTypes(): void {
    this.priceTypeService.getAll().subscribe((resp) => {
      this.priceTypeSelectConfig.options = resp;
    });
  }

  getAllSalesTypes(): void {
    this.realEstateService.getAllSalesTypes().subscribe((resp) => {
      this.saleTypeSelectConfig.options = resp.map((saleType) => ({
        name: saleType,
      }));
    });
  }

  getAllPaymentPeriods(): void {
    this.paymentPeriodService.getAll().subscribe((resp) => {
      this.termsPaymentPeriodSelectConfig.options = resp;
    });
  }

  getAllStructures(): void {
    this.structureService.getAll().subscribe((resp) => {
      this.structureSelectConfig.options = resp;
    });
  }

  getAllExtraInfos(): void {
    this.extraInfoService.getAll().subscribe((resp) => {
      this.listOfExtraInfos = resp;
    });
  }

  addFiles(event): void {
    this.isFloatingBtnDisabled = false;
    // tslint:disable-next-line:prefer-for-of
    if (event.length > 0 && this.fileUploadList.indexOf(event) === -1) {
      for (const file of event) {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          file.url = reader.result;
        };
        this.fileUploadList.push(file);
      }
    }
    this.uploadArraySize = this.fileUploadList.length;
  }

  async uploadImage(realEstateId: number): Promise<void> {
    if (this.fileUploadList.length === 0) {
      this.spinnerService.hide(this.spinner);
    }
    let counter = 0;
    const fileUploadListSize = this.fileUploadList.length;

    while (counter < fileUploadListSize) {
      const formData = new FormData();
      formData.append('file', this.fileUploadList[counter]);
      try {
        const media = await this.mediaService
          .addMediaToEstate(realEstateId, formData)
          .toPromise();
        this.fileUploadIndex++;
        this.data.medias?.push(media);
        SnackbarUtil.openSnackBar(
          this.snackBar,
          SnackBarMessages.SUCCESS_MESSAGE
        );
        this.spinnerService.hide(this.spinner);
        if (this.fileUploadList.length === this.fileUploadIndex) {
          this.fileUploadList = [];
        }
      } catch (e) {
        console.error(e);
      }
      counter++;
    }
  }

  addYoutubeVideo(realEstateID: number): void {
    const ytLink = this.mediaForm.get(
      FormControlNames.YOUTUBE_LINK_FORM_CONTROL
    )?.value;
    if (ytLink) {
      this.mediaService
        .addYoutubeLink(realEstateID, { type: MediaType.YOUTUBE, uri: ytLink })
        .subscribe(
          () => {},
          () => {
            SnackbarUtil.openSnackBar(
              this.snackBar,
              SnackBarMessages.YT_LINK_ERR_MESSAGE
            );
          }
        );
    }
  }

  setYoutubeVideo(idRealEstate: number): void {
    const currentYoutubeVideo: Media | undefined = this.data.medias?.find(
      (item) => item.type === MediaType.YOUTUBE
    );
    const currentYoutubeFormValue = this.mediaForm.get(
      FormControlNames.YOUTUBE_LINK_FORM_CONTROL
    )?.value;
    if (currentYoutubeVideo?.uri !== '' && currentYoutubeVideo === '') {
      // @ts-ignore
      this.mediaService.delete(currentYoutubeVideo?.id).subscribe((resp) => {});
    } else if (
      currentYoutubeVideo &&
      currentYoutubeVideo.uri !== currentYoutubeFormValue
    ) {
      currentYoutubeVideo.uri = currentYoutubeFormValue;
      currentYoutubeVideo.realEstate = { id: this.data.id };
      this.mediaService.update(currentYoutubeVideo).subscribe((resp) => {});
    } else if (!currentYoutubeVideo && currentYoutubeFormValue !== '') {
      this.addYoutubeVideo(idRealEstate);
    } else {
    }
  }

  scrollToElement(id): void {
    // @ts-ignore
    document.getElementById(id).scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'nearest',
    });
  }

  getLongitudeAndLatitude(event: any): void {
    this.characteristicsForm
      .get(FormControlNames.LONGITUDE_FORM_CONTROL)
      ?.setValue(event.coords.lng);
    this.characteristicsForm
      .get(FormControlNames.LATITUDE_FORM_CONTROL)
      ?.setValue(event.coords.lat);
  }

  resetUpload(): void {
    if (this.fileUploadIndex === this.fileUploadList.length) {
      this.fileUploadList = [];
      this.fileUploadIndex = 0;
    }
  }

  compareObjects(o1: any, o2: any): boolean {
    if (o2 !== null && o2 !== undefined) {
      return o1.name === o2.name && o1.id === o2.id;
    } else {
      return false;
    }
  }

  deleteImage(media: Media): void {
    // @ts-ignore
    this.mediaService.delete(media.id).subscribe(() => {
      const index = this.data.medias?.indexOf(media);
      // @ts-ignore
      this.data.medias?.splice(index, 1);
    });
  }

  drag(draggedImage: Media): void {
    this.draggedImage = draggedImage;
  }

  drop(enterImage: Media): void {
    const indexFrom = this.data.medias?.indexOf(this.draggedImage);
    const indexTo = this.data.medias?.indexOf(enterImage);
    // @ts-ignore
    this.insertAndShift(this.data.medias, indexFrom, indexTo);
  }

  dropOnUpload(enterImage: any): void {
    const indexFrom = this.fileUploadList?.indexOf(this.draggedImage);
    const indexTo = this.fileUploadList?.indexOf(enterImage);
    // @ts-ignore
    this.insertAndShift(this.fileUploadList, indexFrom, indexTo);
  }

  allowDrop(event): void {
    event.preventDefault();
  }

  insertAndShift(arr: any[], from, to): void {
    const cutOut = arr.splice(from, 1)[0];
    // @ts-ignore
    arr.splice(to, 0, cutOut);
  }

  drLeave(event): void {
    event.target.style.margin = '0';
  }

  save(): void {
    if (
      this.basicInfoForm.valid &&
      this.characteristicsForm.valid &&
      this.addressForm.valid &&
      this.ownerForm.valid
    ) {
      this.spinnerService.show(this.spinner);
      const realEstate: RealEstate = this.basicInfoForm.getRawValue();
      Object.assign(realEstate, this.characteristicsForm.getRawValue());
      // @ts-ignore
      realEstate.area = realEstate.area / realEstate.areaUnit.conversionFactor;
      if (realEstate.propertyArea) {
        // @ts-ignore
        realEstate.propertyArea =
          realEstate.propertyArea /
          // @ts-ignore
          realEstate.propertyAreaUnit?.conversionFactor;
      }

      realEstate.description = this.editorComponent.editorInstance?.getData();
      for (const [key, value] of Object.entries(realEstate)) {
        if (!value) {
          delete realEstate[key];
        }
      }

      realEstate.owner = {
        person: this.ownerForm.getRawValue(),
        agentNote: this.ownerForm.get(FormControlNames.AGENT_NOTE_FORM_CONTROL)
          ?.value,
      };

      // @ts-ignore
      realEstate.owner.person?.contacts = [
        {
          id: this.ownerTelephone ? this.ownerTelephone.id : null,
          type: ContactType.PHONE,
          value:
            this.ownerForm.get(FormControlNames.TELEPHONE_FORM_CONTROL)
              ?.value || '',
        },
        {
          id: this.ownerEmail ? this.ownerEmail.id : null,
          type: ContactType.EMAIL,
          value:
            this.ownerForm.get(FormControlNames.EMAIL_FORM_CONTROL)?.value ||
            '',
        },
      ];
      realEstate.contactPerson = {
        person:
          this.contactPersonForm.getRawValue().firstName !== null
            ? this.contactPersonForm.getRawValue()
            : {
                firstName: '',
                lastName: '',
                contacts: [
                  { type: ContactType.PHONE.toString(), value: '' },
                  { type: ContactType.EMAIL.toString(), value: '' },
                ],
              },
      };
      // @ts-ignore
      realEstate.contactPerson.person?.contacts = [
        {
          id: this.contactPersonTelephone
            ? this.contactPersonTelephone.id
            : null,
          type: ContactType.PHONE,
          value:
            this.contactPersonForm.get(FormControlNames.TELEPHONE_FORM_CONTROL)
              ?.value || '',
        },
        {
          id: this.contactPersonEmail ? this.contactPersonEmail.id : null,
          type: ContactType.EMAIL,
          value:
            this.contactPersonForm.get(FormControlNames.EMAIL_FORM_CONTROL)
              ?.value || '',
        },
      ];

      realEstate.whatsRenovated =
        this.whatsRenovatedEditor.editorInstance?.getData();
      realEstate.address = this.addressForm.getRawValue();
      if (this.data) {
        this.data.medias?.filter(
          (item) => (item.order = this.data.medias?.indexOf(item))
        );
        realEstate.owner.id = this.data.owner.id;
        // @ts-ignore
        realEstate.owner.person.id = this.data.owner.person.id;
        realEstate.contactPerson.id = this.data.contactPerson.id;
        // @ts-ignore
        realEstate.contactPerson.person.id = this.data.contactPerson.person.id;
        realEstate.id = this.data.id;
        realEstate.agents.filter((agent) => delete agent.realEstates);
        // @ts-ignore
        realEstate.address.id = this.data.address.id;

        // @ts-ignore
        realEstate.address?.municipality = {
          id: realEstate.address?.municipality?.id,
        };
        if (realEstate.renovatedDate) {
          realEstate.renovatedDate = moment(realEstate.renovatedDate).format(
            'YYYY-MM-DD'
          );
        }

        if (realEstate.moveIn) {
          realEstate.moveInDate = moment(realEstate.moveInDate).format(
            'YYYY-MM-DD'
          );
        }

        if (realEstate.address?.municipality?.city?.municipalities) {
          delete realEstate.address?.municipality?.city?.municipalities;
        }

        this.realEstateService.update(realEstate).subscribe(
          async () => {
            // @ts-ignore
            await this.uploadImage(this.data.id);
            // @ts-ignore
            this.mediaService
              // @ts-ignore
              .addMediasToEstate(this.data.id, this.data.medias)
              .subscribe(() => {});
            // @ts-ignore
            this.setYoutubeVideo(this.data.id);
            SnackbarUtil.openSnackBar(
              this.snackBar,
              SnackBarMessages.SUCCESS_MESSAGE
            );
          },
          () => {
            this.spinnerService.hide(this.spinner);
            SnackbarUtil.openSnackBar(
              this.snackBar,
              SnackBarMessages.ERR_MESSAGE
            );
          }
        );
      } else {
        // @ts-ignore
        realEstate.address?.municipality = {
          id: realEstate.address?.municipality?.id,
        };
        this.realEstateService.save(realEstate).subscribe(
          async (savedRealEstate) => {
            SnackbarUtil.openSnackBar(
              this.snackBar,
              SnackBarMessages.SUCCESS_MESSAGE
            );
            SnackbarUtil.openSnackBar(
              this.snackBar,
              SnackBarMessages.SUCCESS_MESSAGE
            );
            this.spinnerService.hide(this.spinner);
            await this.uploadImage(savedRealEstate.id);
            await this.addYoutubeVideo(savedRealEstate.id);
            this.fileUploadList = [];
            this.data = realEstate;
          },
          () => {
            this.spinnerService.hide(this.spinner);
            SnackbarUtil.openSnackBar(
              this.snackBar,
              SnackBarMessages.ERR_MESSAGE
            );
          }
        );
      }
    } else {
      SnackbarUtil.openSnackBar(this.snackBar, 'Popunite obavezna polja');
    }
  }

  removeFromFileUploadList(media: any): void {
    this.fileUploadList.splice(this.fileUploadList.indexOf(media), 1);
  }
}
