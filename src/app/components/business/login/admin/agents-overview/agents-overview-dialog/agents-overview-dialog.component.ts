import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { AgentsService } from '../../../../../../service/agents.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RealEstate } from '../../../../../../models/RealEstate';
import { RealEstateService } from '../../../../../../service/real-estate.service';
import { SpinnerService } from '../../../../../../service/spinner.service';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { FormControl, FormGroup } from '@angular/forms';
import { User } from '../../../../../../models/User';
import { MediaType } from '../../../../../../enums/MediaType';

@Component({
  selector: 'app-agents-overview-dialog',
  templateUrl: './agents-overview-dialog.component.html',
  styleUrls: ['./agents-overview-dialog.component.sass'],
})
export class AgentsOverviewDialogComponent implements OnInit {
  @ViewChild('spinner') spinner: MatProgressSpinner;

  listOfRealEstates: RealEstate[] = [];

  searchForm = new FormGroup({
    search: new FormControl(),
  });

  searchText = '';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: User,
    private agentService: AgentsService,
    private snackBar: MatSnackBar,
    private realEstateService: RealEstateService,
    private spinnerService: SpinnerService
  ) {}

  ngOnInit(): void {
    this.findAllRealEstatesWhereAgentAssigned();
  }

  findAllRealEstatesWhereAgentAssigned(): void {
    // @ts-ignore
    if (this.data.agent.realEstates?.length === 0) {
      setTimeout(() => {
        this.spinnerService.hide(this.spinner);
      }, 500);
    }
    // @ts-ignore
    for (const realEstateID of this.data.agent?.realEstates) {
      this.realEstateService.findById(realEstateID).subscribe((realEstate) => {
        // @ts-ignore
        if (realEstate.medias[0].type === MediaType.YOUTUBE.toString()) {
          // @ts-ignore
          realEstate.medias?.push(realEstate.medias?.splice(0, 1)[0]);
        }
        this.listOfRealEstates.push(realEstate);
        this.spinnerService.hide(this.spinner);
      });
    }
  }
}
