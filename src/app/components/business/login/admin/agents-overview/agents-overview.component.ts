import { Component, OnInit, ViewChild } from '@angular/core';
import { Agent } from '../../../../../models/Agent';
import { DialogOptions } from '../../../../../util/dialog-options';
import { DialogUtil } from '../../../../../util/dialog-util';
import { MatDialog } from '@angular/material/dialog';
import { AddAgentDialogComponent } from './add-agent-dialog/add-agent-dialog.component';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { SpinnerService } from '../../../../../service/spinner.service';
import { AgentColumnNames, SnackBarMessages } from '../../../../../const/const';
import { SnackbarUtil } from '../../../../../util/snackbar-util';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AgentsOverviewDialogComponent } from './agents-overview-dialog/agents-overview-dialog.component';
import { EditAgentDialogComponent } from './edit-agent-dialog/edit-agent-dialog.component';
import { AuthGuard } from '../../../../../guards/auth.guard';
import { Router } from '@angular/router';
import { UserService } from '../../../../../service/user.service';
import { User } from '../../../../../models/User';

@Component({
  selector: 'app-agents-overview',
  templateUrl: './agents-overview.component.html',
  styleUrls: ['./agents-overview.component.sass'],
})
export class AgentsOverviewComponent implements OnInit {
  data = '';

  displayedColumns: string[] = [
    AgentColumnNames.FIRST_NAME_COL,
    AgentColumnNames.LAST_NAME_COL,
    AgentColumnNames.OPT_NAME_COL,
  ];

  @ViewChild('spinner') spinner: MatProgressSpinner;
  listOfAgents: User[] = [];

  constructor(
    private dialog: MatDialog,
    private userService: UserService,
    private snackBar: MatSnackBar,
    private spinnerService: SpinnerService,
    private router: Router,
    private authGuard: AuthGuard
  ) {}

  ngOnInit(): void {
    this.getAllAgents();
  }

  openAgentOverviewDialog(agent: Agent): void {
    DialogUtil.openDialog(
      AgentsOverviewDialogComponent,
      DialogOptions.getOptions(agent),
      this.dialog
    );
  }

  openEditAgentDialog(agent: Agent): void {
    DialogUtil.openDialog(
      EditAgentDialogComponent,
      DialogOptions.getOptions(agent),
      this.dialog
    )
      .afterClosed()
      .subscribe(() => {
        this.getAllAgents();
      });
  }

  getAllAgents(): void {
    this.userService.getAll().subscribe((resp) => {
      this.listOfAgents = resp.filter((user) => user.agent);
      this.spinnerService.hide(this.spinner);
    });
  }

  openAddAgentDialog(): void {
    DialogUtil.openDialog(
      AddAgentDialogComponent,
      DialogOptions.getOptions({}),
      this.dialog
    )
      .afterClosed()
      .subscribe(() => {
        this.getAllAgents();
      });
  }

  deleteAgent(id: number): void {
    this.spinnerService.show(this.spinner);
    this.userService.delete(id).subscribe(
      () => {
        SnackbarUtil.openSnackBar(
          this.snackBar,
          SnackBarMessages.SUCCESS_MESSAGE
        );
        this.spinnerService.hide(this.spinner);
        this.getAllAgents();
      },
      () => {
        SnackbarUtil.openSnackBar(this.snackBar, SnackBarMessages.ERR_MESSAGE);
      }
    );
  }
}
