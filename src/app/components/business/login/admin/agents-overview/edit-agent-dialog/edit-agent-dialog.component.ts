import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FieldConfig } from '../../../../../../models/FIeldConfig';
import {
  FormControlNames,
  InputTypes,
  SnackBarMessages,
} from '../../../../../../const/const';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SpinnerService } from '../../../../../../service/spinner.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ContactType } from '../../../../../../models/ContactType';
import { SnackbarUtil } from '../../../../../../util/snackbar-util';
import { Contact } from '../../../../../../models/Contact';
import { AuthGuard } from '../../../../../../guards/auth.guard';
import { User } from '../../../../../../models/User';
import { RoleService } from '../../../../../../service/role.service';
import { Role } from '../../../../../../models/Role';
import { UserService } from '../../../../../../service/user.service';

@Component({
  selector: 'app-edit-agent-dialog',
  templateUrl: './edit-agent-dialog.component.html',
  styleUrls: ['./edit-agent-dialog.component.sass'],
})
export class EditAgentDialogComponent implements OnInit {
  listOfRoles: Role[] = [];
  listOfUserRoles: Role[] = [];

  @ViewChild('spinner') spinner: MatProgressSpinner;

  telephone: Contact;
  email: Contact;

  agentForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    telephone: new FormControl(''),
    email: new FormControl(''),
    username: new FormControl(this.data.username),
    roles: new FormControl(),
  });

  firstNameInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.FIRST_NAME_FORM_CONTROL,
  };
  lastNameInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.LAST_NAME_FORM_CONTROL,
  };
  telephoneInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.TELEPHONE_FORM_CONTROL,
  };
  emailInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.EMAIL_FORM_CONTROL,
  };
  usernameInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.USERNAME_NAME_FORM_CONTROL,
  };
  roleSelectConfig: FieldConfig = {
    type: InputTypes.SELECT_TYPE_NAME,
    name: FormControlNames.ROLE_FORM_CONTROL,
  };

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: User,
    private userService: UserService,
    private authGuard: AuthGuard,
    private snackBar: MatSnackBar,
    private spinnerService: SpinnerService,
    private roleService: RoleService
  ) {}

  ngOnInit(): void {
    this.getRoles();
    this.getUserRoles();
    setTimeout(() => {
      this.setContactValues();
    }, 100);
  }

  getRoles(): void {
    this.roleService.getAll().subscribe((roles) => {
      this.listOfRoles = roles;
      this.roleSelectConfig.options = roles;
    });
  }

  setContactValues(): void {
    // @ts-ignore
    if (this.data.agent?.person.contacts?.length > 0) {
      // @ts-ignore
      this.email = this.data.agent?.person.contacts?.filter(
        (contact) => contact.type === ContactType.EMAIL.toString()
      )[0];
      // @ts-ignore
      this.telephone = this.data.agent?.person.contacts?.filter(
        (contact) => contact.type === ContactType.PHONE.toString()
      )[0];
      this.agentForm
        .get(FormControlNames.TELEPHONE_FORM_CONTROL)
        ?.setValue(this.telephone.value);
      this.agentForm
        .get(FormControlNames.EMAIL_FORM_CONTROL)
        ?.setValue(this.email.value);
    }
  }

  update(): void {
    this.spinnerService.show(this.spinner);
    const user: User = {
      id: this.data.id,
      agent: {
        id: this.data.agent?.id,
        person: {
          id: this.data.agent?.person.id,
          firstName: this.agentForm.get(
            FormControlNames.FIRST_NAME_FORM_CONTROL
          )?.value,
          lastName: this.agentForm.get(FormControlNames.LAST_NAME_FORM_CONTROL)
            ?.value,
          contacts: [
            {
              type: ContactType.EMAIL,
              value: this.agentForm.get(FormControlNames.EMAIL_FORM_CONTROL)
                ?.value,
            },
            {
              type: ContactType.PHONE,
              value: this.agentForm.get(FormControlNames.TELEPHONE_FORM_CONTROL)
                ?.value,
            },
          ],
        },
      },
      roles: this.listOfUserRoles,
      username: this.agentForm.get(FormControlNames.USERNAME_NAME_FORM_CONTROL)
        ?.value,
    };

    if (this.email) {
      // @ts-ignore
      user.agent.person.contacts[0].id = this.email.id;
    }
    if (this.telephone) {
      // @ts-ignore
      user.agent.person.contacts[1].id = this.telephone.id;
    }
    this.userService.update(user).subscribe(
      () => {
        SnackbarUtil.openSnackBar(
          this.snackBar,
          SnackBarMessages.SUCCESS_MESSAGE
        );
        this.spinnerService.hide(this.spinner);
      },
      () => {
        SnackbarUtil.openSnackBar(this.snackBar, SnackBarMessages.ERR_MESSAGE);
        this.spinnerService.hide(this.spinner);
      }
    );
  }

  getUserRoles(): void {
    this.userService.getUserRoles(this.data.id).subscribe((roles) => {
      this.listOfUserRoles = roles;
      this.agentForm
        .get('roles')
        ?.setValue(this.listOfUserRoles.map((role) => role.id));
    });
  }

  addSelectedRole(role: Role): void {
    const index = this.listOfUserRoles.indexOf(role);
    if (index === -1) {
      this.listOfUserRoles.push(role);
    } else {
      // @ts-ignore
      this.listOfUserRoles.splice(index, 1);
    }
  }
}
