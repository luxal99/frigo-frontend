import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FieldConfig } from '../../../../../../models/FIeldConfig';
import {
  FormControlNames,
  InputTypes,
  SnackBarMessages,
} from '../../../../../../const/const';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ContactType } from '../../../../../../models/ContactType';
import { SnackbarUtil } from '../../../../../../util/snackbar-util';
import { AgentsService } from '../../../../../../service/agents.service';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { SpinnerService } from '../../../../../../service/spinner.service';
import { AuthGuard } from '../../../../../../guards/auth.guard';
import { UserService } from '../../../../../../service/user.service';
import { RoleService } from '../../../../../../service/role.service';
import { Role } from '../../../../../../models/Role';

@Component({
  selector: 'app-add-agent-dialog',
  templateUrl: './add-agent-dialog.component.html',
  styleUrls: ['./add-agent-dialog.component.sass'],
})
export class AddAgentDialogComponent implements OnInit {
  listOfRoles: Role[] = [];
  listOfSelectedRole: Role[] = [];
  @ViewChild('spinner') spinner: MatProgressSpinner;
  agentForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    telephone: new FormControl(''),
    email: new FormControl(''),
    username: new FormControl(),
  });

  firstNameInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.FIRST_NAME_FORM_CONTROL,
  };
  lastNameInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.LAST_NAME_FORM_CONTROL,
  };
  telephoneInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.TELEPHONE_FORM_CONTROL,
  };
  emailInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.EMAIL_FORM_CONTROL,
  };
  usernameInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.USERNAME_NAME_FORM_CONTROL,
  };

  constructor(
    private agentService: AgentsService,
    private authGuard: AuthGuard,
    private userService: UserService,
    private roleService: RoleService,
    private snackBar: MatSnackBar,
    private spinnerService: SpinnerService
  ) {}

  ngOnInit(): void {
    this.getAllRoles();
  }

  addSelectedRole(role: Role): void {
    const index = this.listOfSelectedRole.indexOf(role);
    if (index === -1) {
      this.listOfSelectedRole.push(role);
    } else {
      this.listOfSelectedRole.splice(index, 1);
    }
  }

  getAllRoles(): void {
    this.roleService.getAll().subscribe((roles) => {
      this.listOfRoles = roles;
    });
  }

  save(): void {
    this.spinnerService.show(this.spinner);
    if (this.agentForm.valid) {
      this.userService
        .save({
          roles: this.listOfSelectedRole,
          username: this.agentForm.get(
            FormControlNames.USERNAME_NAME_FORM_CONTROL
          )?.value,
          agent: {
            person: {
              firstName: this.agentForm.get(
                FormControlNames.FIRST_NAME_FORM_CONTROL
              )?.value,
              lastName: this.agentForm.get(
                FormControlNames.LAST_NAME_FORM_CONTROL
              )?.value,
              contacts: [
                {
                  type: ContactType.EMAIL,
                  value: this.agentForm.get(FormControlNames.EMAIL_FORM_CONTROL)
                    ?.value,
                },
                {
                  type: ContactType.PHONE,
                  value: this.agentForm.get(
                    FormControlNames.TELEPHONE_FORM_CONTROL
                  )?.value,
                },
              ],
            },
          },
        })
        .subscribe(
          () => {
            SnackbarUtil.openSnackBar(
              this.snackBar,
              SnackBarMessages.SUCCESS_MESSAGE
            );
            this.spinnerService.hide(this.spinner);
          },
          () => {
            this.spinnerService.hide(this.spinner);
            SnackbarUtil.openSnackBar(
              this.snackBar,
              SnackBarMessages.ERR_MESSAGE
            );
          }
        );
    }
  }
}
