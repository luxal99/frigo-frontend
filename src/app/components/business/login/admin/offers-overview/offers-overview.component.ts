import { Component, OnInit, ViewChild } from '@angular/core';
import { OfferService } from '../../../../../service/offer.service';
import { Offer } from '../../../../../models/Offer';
import { MatDialog } from '@angular/material/dialog';
import { DialogUtil } from '../../../../../util/dialog-util';
import { DialogOptions } from '../../../../../util/dialog-options';
import { OfferOverviewDialogComponent } from './offer-overview-dialog/offer-overview-dialog.component';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { SpinnerService } from '../../../../../service/spinner.service';
import { AuthGuard } from '../../../../../guards/auth.guard';
import { SnackbarUtil } from '../../../../../util/snackbar-util';
import { SnackBarMessages } from '../../../../../const/const';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-offers-overview',
  templateUrl: './offers-overview.component.html',
  styleUrls: ['./offers-overview.component.sass'],
})
export class OffersOverviewComponent implements OnInit {
  @ViewChild('spinner') spinner: MatProgressSpinner;
  listOfOffers: Offer[] = [];

  constructor(
    private offerService: OfferService,
    private authGuard: AuthGuard,
    private spinnerService: SpinnerService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.getOffers();
  }

  getOffers(): void {
    this.offerService.getAll().subscribe((resp) => {
      this.listOfOffers = resp.reverse();
      for (const offer of this.listOfOffers) {
        offer.createdDate = new Date(offer.createdDate);
      }
      this.spinnerService.hide(this.spinner);
    });
  }

  openOfferOverviewDialog(offer: Offer): void {
    DialogUtil.openDialog(
      OfferOverviewDialogComponent,
      DialogOptions.getOptions(offer),
      this.dialog
    );
  }

  deleteOffer(offerId: any): void {
    this.spinnerService.show(this.spinner);
    this.offerService.delete(offerId).subscribe(
      () => {
        this.spinnerService.hide(this.spinner);
        this.getOffers();
      },
      () => {
        SnackbarUtil.openSnackBar(this.snackBar, SnackBarMessages.ERR_MESSAGE);
      }
    );
  }
}
