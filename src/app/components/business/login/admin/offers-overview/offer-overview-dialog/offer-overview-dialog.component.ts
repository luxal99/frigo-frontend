import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Offer } from '../../../../../../models/Offer';

@Component({
  selector: 'app-offer-overview-dialog',
  templateUrl: './offer-overview-dialog.component.html',
  styleUrls: ['./offer-overview-dialog.component.sass'],
})
export class OfferOverviewDialogComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public offer: Offer) {}

  ngOnInit(): void {}
}
