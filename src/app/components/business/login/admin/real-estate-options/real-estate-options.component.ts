import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogUtil } from '../../../../../util/dialog-util';
import { DialogOptions } from '../../../../../util/dialog-options';
import { CityService } from '../../../../../service/city.service';
import {
  FormControlNames,
  InputTypes,
  SnackBarMessages,
} from '../../../../../const/const';
import { Validators } from '@angular/forms';
import { City } from '../../../../../models/City';
import { CategoryService } from '../../../../../service/category.service';
import { Category } from '../../../../../models/Category';
import { CarpentryService } from '../../../../../service/carpentry.service';
import { Floor } from '../../../../../models/Floor';
import { FloorService } from '../../../../../service/floor.service';
import { Furniture } from '../../../../../models/Furniture';
import { FurnitureService } from '../../../../../service/furniture.service';
import { HeatingService } from '../../../../../service/heating.service';
import { Heating } from '../../../../../models/Heating';
import { StructureService } from '../../../../../service/structure.service';
import { Structure } from '../../../../../models/Structure';
import { MatTab, MatTabGroup } from '@angular/material/tabs';
import { ParkingService } from '../../../../../service/parking.service';
import { PriceTypeService } from '../../../../../service/price-type.service';
import { PaymentPeriodService } from '../../../../../service/payment-period.service';
import { FiledStatusService } from '../../../../../service/filed-status.service';
import { Parking } from '../../../../../models/Parking';
import { PriceType } from '../../../../../models/PriceType';
import { PaymentPeriod } from '../../../../../models/PaymentPeriod';
import { FiledStatus } from '../../../../../models/FiledStatus';
import { Municipality } from '../../../../../models/Municipality';
import { MunicipalityService } from '../../../../../service/municipality.service';
import { FieldConfig } from '../../../../../models/FIeldConfig';
import { ExtraInfo } from '../../../../../models/ExtraInfo';
import { ExtraInfoService } from '../../../../../service/extra-info.service';
import { AddExtraInfoDialogComponent } from './add-extra-info-dialog/add-extra-info-dialog.component';
import { SnackbarUtil } from '../../../../../util/snackbar-util';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EditExtraInfoDialogComponent } from './edit-extra-info-dialog/edit-extra-info-dialog.component';
import { AuthGuard } from '../../../../../guards/auth.guard';
import { ExtraRoom } from '../../../../../models/ExtraRoom';
import { ExtraRoomService } from '../../../../../service/extra-room.service';
import { DivideTypeEnum } from '../../../../../enums/DivideTypeEnum';
import { SpinnerService } from '../../../../../service/spinner.service';
import { MatProgressSpinner } from '@angular/material/progress-spinner';

@Component({
  selector: 'app-real-estate-options',
  templateUrl: './real-estate-options.component.html',
  styleUrls: ['./real-estate-options.component.sass'],
})
export class RealEstateOptionsComponent implements OnInit {
  @ViewChild('tabGroup') tabGroup!: MatTabGroup;
  @ViewChild('secondTabGroup') secondTabGroup!: MatTabGroup;

  @ViewChild('category') categoryTab!: MatTab;
  @ViewChild('spinner') spinner!: MatProgressSpinner;

  @ViewChild('city') cityTab!: MatTab;
  @ViewChild('furniture') furnitureTab!: MatTab;
  @ViewChild('structure') structureTab!: MatTab;
  @ViewChild('heating') heatingTab!: MatTab;
  @ViewChild('floor') floorTab!: MatTab;
  @ViewChild('carpentry') carpentryTab!: MatTab;
  @ViewChild('filedStatus') filedStatusTab!: MatTab;
  @ViewChild('parking') parkingTab!: MatTab;
  @ViewChild('priceType') priceTypeTab!: MatTab;
  @ViewChild('paymentPeriod') paymentPeriodTab!: MatTab;
  @ViewChild('municipalityTab') municipalityTab!: MatTab;
  @ViewChild('extraInfo') extraInfoTab!: MatTab;
  @ViewChild('extraRoom') extraRoomTab!: MatTab;

  municipalitySelectConfig: FieldConfig = {
    type: InputTypes.SELECT_TYPE_NAME,
    name: 'city',
  };
  municipalityInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.NAME_FORM_CONTROL,
  };

  listOfCities: City[] = [];
  listOfCategories: Category[] = [];
  listOfCarpentries: City[] = [];
  listOfFloors: Floor[] = [];
  listOfFurnitures: Furniture[] = [];
  listOfHeatings: Heating[] = [];
  listOfStructures: Structure[] = [];
  listOfParkings: Parking[] = [];
  listOfPriceTypes: PriceType[] = [];
  listOfPaymentPeriods: PaymentPeriod[] = [];
  listOfFiledStatuses: FiledStatus[] = [];
  listOfMunicipalities: Municipality[] = [];
  listOfExtraInfos: ExtraInfo[] = [];
  listOfExtraRooms: ExtraRoom[] = [];

  constructor(
    private dialog: MatDialog,
    private spinnerService: SpinnerService,
    public categoryService: CategoryService,
    private authGuard: AuthGuard,
    private snackBar: MatSnackBar,
    public parkingService: ParkingService,
    public priceTypeService: PriceTypeService,
    public paymentPeriodService: PaymentPeriodService,
    public filedStatusService: FiledStatusService,
    public municipalityService: MunicipalityService,
    private extraInfoService: ExtraInfoService,
    public extraRoomService: ExtraRoomService,
    public heatingService: HeatingService,
    public structureService: StructureService,
    public floorService: FloorService,
    public furnitureService: FurnitureService,
    public cityService: CityService,
    public carpentryService: CarpentryService
  ) {}

  ngOnInit(): void {
    this.getAllCities();
    this.getAllParkings();
  }

  getActiveTab(): void {
    if (this.categoryTab.isActive) {
      this.getAllCategories();
    } else if (this.structureTab.isActive) {
      this.getAllStructures();
    } else if (this.furnitureTab.isActive) {
      this.getAllFurnitures();
    } else if (this.floorTab.isActive) {
      this.getAllFloors();
    } else if (this.heatingTab.isActive) {
      this.getAllHeating();
    } else if (this.carpentryTab.isActive) {
      this.getAllCarpentries();
    }

    if (this.priceTypeTab.isActive) {
      this.getAllPriceTypes();
    } else if (this.paymentPeriodTab.isActive) {
      this.getAllPaymentPeriods();
    } else if (this.filedStatusTab.isActive) {
      this.getAllFiledStatuses();
    } else if (this.municipalityTab.isActive) {
      this.getAllMunicipalities();
    } else if (this.extraInfoTab.isActive) {
      this.getAllExtraInfos();
    } else if (this.extraRoomTab.isActive) {
      this.getExtraRooms();
    }
  }

  // region --GET METHODS
  getAllStructures(): void {
    this.structureService.getAll().subscribe((resp) => {
      this.listOfStructures = resp.map((item) => ({
        id: item.id,
        type: item.type,
      }));
    });
  }

  getExtraRooms(): void {
    this.extraRoomService.getAll().subscribe((resp) => {
      this.listOfExtraRooms = resp.map((extraRoom) => ({
        id: extraRoom.id,
        name: extraRoom.name,
      }));
    });
  }

  getAllFloors(): void {
    this.floorService.getAll().subscribe((resp) => {
      this.listOfFloors = resp.map((item) => ({
        id: item.id,
        name: item.name,
      }));
    });
  }

  getAllCities(): void {
    this.cityService.getAll().subscribe((resp) => {
      this.listOfCities = resp.map((item) => ({
        id: item.id,
        name: item.name,
      }));

      this.municipalitySelectConfig.options = this.listOfCities;
    });
  }

  getAllCategories(): any {
    this.categoryService.getAll().subscribe((resp) => {
      this.listOfCategories = resp.map((item) => ({
        id: item.id,
        name: item.name,
      }));
      this.spinnerService.hide(this.spinner);
    });
  }

  getAllMunicipalities(): void {
    this.municipalityService.getAll().subscribe((resp) => {
      this.listOfMunicipalities = resp.map((item) => ({
        id: item.id,
        name: item.name,
        cityName: item.city?.name,
      }));
    });
  }

  getAllFurnitures(): void {
    this.furnitureService.getAll().subscribe((resp) => {
      this.listOfFurnitures = resp.map((item) => ({
        id: item.id,
        type: item.type,
      }));
    });
  }

  getAllCarpentries(): void {
    this.carpentryService.getAll().subscribe((resp) => {
      this.listOfCarpentries = resp.map((item) => ({
        id: item.id,
        type: item.type,
      }));
    });
  }

  getAllHeating(): void {
    this.heatingService.getAll().subscribe((resp) => {
      this.listOfHeatings = resp.map((item) => ({
        id: item.id,
        type: item.type,
      }));
    });
  }

  getAllParkings(): void {
    this.parkingService.getAll().subscribe((resp) => {
      this.listOfParkings = resp.map((item) => ({
        id: item.id,
        type: item.type,
      }));
    });
  }

  getAllPriceTypes(): void {
    this.priceTypeService.getAll().subscribe((resp) => {
      this.listOfPriceTypes = resp.map((item) => ({
        id: item.id,
        type: item.type,
      }));
    });
  }

  getAllPaymentPeriods(): void {
    this.paymentPeriodService.getAll().subscribe((resp) => {
      this.listOfPaymentPeriods = resp.map((item) => ({
        id: item.id,
        value: item.value,
      }));
    });
  }

  getAllFiledStatuses(): void {
    this.filedStatusService.getAll().subscribe((resp) => {
      this.listOfFiledStatuses = resp.map((item) => ({
        id: item.id,
        status: item.status,
      }));
    });
  }

  getAllExtraInfos(): void {
    this.extraInfoService.getAll().subscribe((resp) => {
      this.listOfExtraInfos = resp;
    });
  }

  // endregion

  //region -- OPEN DIALOG METHODS --
  openAddCarpentryDialog(): void {
    DialogUtil.openAddOptionsDialog(
      DialogOptions.getOptionsFormAddOptionsDialog({
        formFields: [
          {
            name: FormControlNames.CARPENTRY_NAME_FORM_CONTROL,
            label: 'Naziv',
            validation: [Validators.required, Validators.minLength(4)],
            type: InputTypes.INPUT_TYPE_NAME,
          },
        ],
        service: this.carpentryService,
        headerText: 'Dodavanje stolarije',
      }),
      this.dialog
    )
      .afterClosed()
      .subscribe(() => {
        this.getAllCarpentries();
      });
  }

  openAddMunicipalityDialog(): void {
    DialogUtil.openAddOptionsDialog(
      DialogOptions.getOptionsFormAddOptionsDialog({
        headerText: 'Dodavanje dela grada/opštine',
        service: this.municipalityService,
        formFields: [
          this.municipalitySelectConfig,
          {
            type: InputTypes.INPUT_TYPE_NAME,
            name: FormControlNames.NAME_FORM_CONTROL,
            label: 'Naziv dela grada/opštine',
          },
          this.municipalityInputConfig,
        ],
      }),
      this.dialog
    );
  }

  openAddCityDialog(): void {
    DialogUtil.openAddOptionsDialog(
      DialogOptions.getOptionsFormAddOptionsDialog({
        formFields: [
          {
            name: FormControlNames.NAME_FORM_CONTROL,
            label: 'Naziv grada',
            validation: [Validators.required, Validators.minLength(4)],
            type: InputTypes.INPUT_TYPE_NAME,
          },
        ],
        service: this.cityService,
        headerText: 'Dodavanje grada',
      }),
      this.dialog
    )
      .afterClosed()
      .subscribe(() => {
        this.getAllCities();
      });
  }

  openAddCategoryDialog(): void {
    DialogUtil.openAddOptionsDialog(
      DialogOptions.getOptionsFormAddOptionsDialog({
        formFields: [
          {
            name: FormControlNames.NAME_FORM_CONTROL,
            label: 'Naziv kategorije',
            validation: [Validators.required, Validators.minLength(4)],
            type: InputTypes.INPUT_TYPE_NAME,
          },
        ],
        service: this.categoryService,
        headerText: 'Dodavanje kategorije',
      }),
      this.dialog
    )
      .afterClosed()
      .subscribe(() => {
        this.getAllCategories();
      });
  }

  openAddFloorDialog(): void {
    DialogUtil.openAddOptionsDialog(
      DialogOptions.getOptionsFormAddOptionsDialog({
        formFields: [
          {
            name: FormControlNames.NAME_FORM_CONTROL,
            validation: [Validators.required, Validators.minLength(4)],
            type: InputTypes.INPUT_TYPE_NAME,
          },
        ],
        service: this.floorService,
        headerText: 'Dodavanje sprata',
      }),
      this.dialog
    )
      .afterClosed()
      .subscribe(() => {
        this.getAllFloors();
      });
  }

  openAddFurnitureDialog(): void {
    DialogUtil.openAddOptionsDialog(
      DialogOptions.getOptionsFormAddOptionsDialog({
        formFields: [
          {
            name: FormControlNames.CARPENTRY_NAME_FORM_CONTROL,
            validation: [Validators.required, Validators.minLength(4)],
            type: InputTypes.INPUT_TYPE_NAME,
          },
        ],
        service: this.furnitureService,
        headerText: 'Dodavanje nameštaja',
      }),
      this.dialog
    )
      .afterClosed()
      .subscribe(() => {
        this.getAllFurnitures();
      });
  }

  openAddHeatingsDialog(): void {
    DialogUtil.openAddOptionsDialog(
      DialogOptions.getOptionsFormAddOptionsDialog({
        formFields: [
          {
            name: FormControlNames.CARPENTRY_NAME_FORM_CONTROL,
            validation: [Validators.required, Validators.minLength(4)],
            type: InputTypes.INPUT_TYPE_NAME,
          },
        ],
        service: this.heatingService,
        headerText: 'Dodavanje grejanja',
      }),
      this.dialog
    )
      .afterClosed()
      .subscribe(() => {
        this.getAllFurnitures();
      });
  }

  openAddStructureDialog(): void {
    DialogUtil.openAddOptionsDialog(
      DialogOptions.getOptionsFormAddOptionsDialog({
        formFields: [
          {
            name: FormControlNames.CARPENTRY_NAME_FORM_CONTROL,
            validation: [Validators.required, Validators.minLength(4)],
            type: InputTypes.INPUT_TYPE_NAME,
          },
        ],
        service: this.structureService,
        headerText: 'Dodavanje strukture',
      }),
      this.dialog
    )
      .afterClosed()
      .subscribe(() => {
        this.getAllStructures();
      });
  }

  openAddParkingDialog(): void {
    DialogUtil.openAddOptionsDialog(
      DialogOptions.getOptionsFormAddOptionsDialog({
        headerText: 'Dodavanje parkinga',
        service: this.parkingService,
        formFields: [
          {
            name: FormControlNames.CARPENTRY_NAME_FORM_CONTROL,
            validation: [Validators.required],
            type: InputTypes.INPUT_TYPE_NAME,
          },
        ],
      }),
      this.dialog
    )
      .afterClosed()
      .subscribe((resp) => {
        this.getAllParkings();
      });
  }

  openAddPriceTypeDialog(): void {
    DialogUtil.openAddOptionsDialog(
      DialogOptions.getOptionsFormAddOptionsDialog({
        headerText: 'Dodavanje tipa plaćanja',
        service: this.priceTypeService,
        formFields: [
          {
            name: FormControlNames.CARPENTRY_NAME_FORM_CONTROL,
            validation: [Validators.required],
            type: InputTypes.INPUT_TYPE_NAME,
          },
          {
            name: FormControlNames.DIVIDE_TYPE,
            type: InputTypes.SELECT_TYPE_NAME,
            options: [DivideTypeEnum.AREA, DivideTypeEnum.FLAT],
          },
        ],
      }),
      this.dialog
    )
      .afterClosed()
      .subscribe((resp) => {
        this.getAllPriceTypes();
      });
  }

  openAddPaymentPeriodDialog(): void {
    DialogUtil.openAddOptionsDialog(
      DialogOptions.getOptionsFormAddOptionsDialog({
        headerText: 'Dodavanje perioda plaćanja',
        service: this.paymentPeriodService,
        formFields: [
          {
            name: FormControlNames.PAYMENT_PERIOD_FORM_CONTROL,
            validation: [Validators.required],
            type: InputTypes.INPUT_TYPE_NAME,
          },
        ],
      }),
      this.dialog
    )
      .afterClosed()
      .subscribe((resp) => {
        this.getAllPaymentPeriods();
      });
  }

  openAddFiledStatusDialog(): void {
    DialogUtil.openAddOptionsDialog(
      DialogOptions.getOptionsFormAddOptionsDialog({
        headerText: 'Dodavanje statusa nekretnine',
        service: this.filedStatusService,
        formFields: [
          {
            name: FormControlNames.FIELD_STATUS_FORM_CONTROL,
            validation: [Validators.required],
            type: InputTypes.INPUT_TYPE_NAME,
          },
        ],
      }),
      this.dialog
    )
      .afterClosed()
      .subscribe((resp) => {
        this.getAllFiledStatuses();
      });
  }

  openAddExtraInfoDialog(): void {
    DialogUtil.openDialog(
      AddExtraInfoDialogComponent,
      DialogOptions.getOptions({}),
      this.dialog
    )
      .afterClosed()
      .subscribe(() => {
        this.getAllExtraInfos();
      });
  }

  openEditExtraInfoDialog(extraInfo: ExtraInfo): void {
    DialogUtil.openDialog(
      EditExtraInfoDialogComponent,
      DialogOptions.getOptions(extraInfo),
      this.dialog
    )
      .afterClosed()
      .subscribe(() => {
        this.getAllExtraInfos();
      });
  }

  openAddExtraRoomDialog(): void {
    DialogUtil.openAddOptionsDialog(
      DialogOptions.getOptionsFormAddOptionsDialog({
        headerText: 'Dodavanje dodatne prostorije',
        service: this.extraRoomService,
        formFields: [
          {
            type: InputTypes.INPUT_TYPE_NAME,
            name: FormControlNames.NAME_FORM_CONTROL,
          },
        ],
      }),
      this.dialog
    );
  }

  //endregion

  deleteExtraInfo(id: number): void {
    this.extraInfoService.delete(id).subscribe(
      () => {
        this.getAllExtraInfos();
      },
      () => {
        SnackbarUtil.openSnackBar(this.snackBar, SnackBarMessages.ERR_MESSAGE);
      }
    );
  }
}
