import {
  AfterContentInit,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { DynamicFormContainerComponent } from '../../../../../../util/dynamic-form/dynamic-form-container/dynamic-form-container.component';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AddOptDialogData } from '../../../../../../models/AddOptDialogData';

@Component({
  selector: 'app-add-option-dialog',
  templateUrl: './add-option-dialog.component.html',
  styleUrls: ['./add-option-dialog.component.sass'],
})
export class AddOptionDialogComponent implements OnInit, AfterContentInit {
  @ViewChild(DynamicFormContainerComponent)
  form!: DynamicFormContainerComponent;

  constructor(@Inject(MAT_DIALOG_DATA) public data: AddOptDialogData) {}

  ngAfterContentInit(): void {}

  ngOnInit(): void {}
}
