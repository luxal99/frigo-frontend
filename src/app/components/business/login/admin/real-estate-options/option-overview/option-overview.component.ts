import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GenericService } from '../../../../../../service/generic.service';
import { CategoryService } from '../../../../../../service/category.service';
import { DialogUtil } from '../../../../../../util/dialog-util';
import { DialogOptions } from '../../../../../../util/dialog-options';
import { FormControlNames, InputTypes } from '../../../../../../const/const';
import { Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { SpinnerService } from '../../../../../../service/spinner.service';
import { MunicipalityService } from '../../../../../../service/municipality.service';
import { FieldConfig } from '../../../../../../models/FIeldConfig';

@Component({
  selector: 'app-option-overview',
  templateUrl: './option-overview.component.html',
  styleUrls: ['./option-overview.component.sass'],
})
export class OptionOverviewComponent implements OnInit {
  constructor(
    private categoryService: CategoryService,
    private dialog: MatDialog,
    private spinnerService: SpinnerService,
    private municipalityService: MunicipalityService
  ) {}

  @Input() list: any[] = [];
  @Input() columnSize = '';
  @Input() service!: GenericService<any>;
  @Output() afterSubmitFunction = new EventEmitter();
  @Input() fControl: any;
  @Input() headerText: any;
  @Output() event = new EventEmitter();
  @Input() isMunicipality = false;

  municipalitySelectConfig: FieldConfig = {
    type: InputTypes.SELECT_TYPE_NAME,
    name: 'city',
  };
  municipalityInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.NAME_FORM_CONTROL,
  };

  ngOnInit(): void {}

  delete(id: number): void {
    this.service.delete(id).subscribe(() => {
      this.afterSubmitFunction.emit(true);
    });
  }

  openMunicipalityEditForm(data?: any): void {
    DialogUtil.openAddOptionsDialog(
      DialogOptions.getOptionsFormAddOptionsDialog({
        headerText: 'Dodavanje dela grada/opštine',
        service: this.municipalityService,
        formFields: [
          this.municipalitySelectConfig,
          {
            type: InputTypes.INPUT_TYPE_NAME,
            name: FormControlNames.NAME_FORM_CONTROL,
            label: 'Naziv dela grada/opštine',
          },
        ],
        data,
      }),
      this.dialog
    )
      .afterClosed()
      .subscribe(() => {
        this.afterSubmitFunction.emit(true);
      });
  }

  update(data: any): void {
    if (this.isMunicipality) {
      this.openMunicipalityEditForm(data);
    } else {
      DialogUtil.openAddOptionsDialog(
        DialogOptions.getOptionsFormAddOptionsDialog({
          formFields: [
            {
              name: this.fControl,
              validation: [Validators.required, Validators.minLength(4)],
              type: InputTypes.INPUT_TYPE_NAME,
            },
          ],
          service: this.service,
          headerText: this.headerText,
          data,
        }),
        this.dialog
      )
        .afterClosed()
        .subscribe(() => {
          this.afterSubmitFunction.emit(true);
        });
    }
  }
}
