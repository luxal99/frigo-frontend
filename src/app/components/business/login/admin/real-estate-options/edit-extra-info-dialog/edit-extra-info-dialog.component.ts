import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { FieldConfig } from '../../../../../../models/FIeldConfig';
import {
  FormControlNames,
  InputTypes,
  SnackBarMessages,
} from '../../../../../../const/const';
import { ExtraInfoService } from '../../../../../../service/extra-info.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ExtraInfo } from '../../../../../../models/ExtraInfo';
import { SnackbarUtil } from '../../../../../../util/snackbar-util';
import { AuthGuard } from '../../../../../../guards/auth.guard';
import { SpinnerService } from '../../../../../../service/spinner.service';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-edit-extra-info-dialog',
  templateUrl: './edit-extra-info-dialog.component.html',
  styleUrls: ['./edit-extra-info-dialog.component.sass'],
})
export class EditExtraInfoDialogComponent implements OnInit {
  @ViewChild('spinner') spinner: MatProgressSpinner;
  fileUpload: any;
  fileUploadIndex = 0;
  extraInfoForm = new FormGroup({
    value: new FormControl(''),
  });

  valueInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.VALUE_FORM_CONTROL,
  };

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ExtraInfo,
    private extraInfoService: ExtraInfoService,
    private snackBar: MatSnackBar,
    private spinnerService: SpinnerService
  ) {}

  ngOnInit(): void {
    this.setValue();
  }

  addFiles(event): void {
    this.fileUpload = event[0];
  }

  setValue(): void {
    this.extraInfoForm
      .get(FormControlNames.VALUE_FORM_CONTROL)
      ?.setValue(this.data.value);
  }

  update(): void {
    const formData = new FormData();

    formData.append('file', this.fileUpload);
    formData.append(
      'value',
      this.extraInfoForm.get(FormControlNames.VALUE_FORM_CONTROL)?.value
    );
    this.extraInfoService.updateCustom(formData, this.data.id).subscribe(
      () => {
        SnackbarUtil.openSnackBar(
          this.snackBar,
          SnackBarMessages.SUCCESS_MESSAGE
        );
      },
      (err: HttpErrorResponse) => {
        SnackbarUtil.openSnackBar(this.snackBar, err.error.error);
        this.spinnerService.hide(this.spinner);
      }
    );
  }
}
