import { Component, OnInit, ViewChild } from '@angular/core';
import { ExtraInfoService } from '../../../../../../service/extra-info.service';
import { FormControl, FormGroup } from '@angular/forms';
import { FieldConfig } from '../../../../../../models/FIeldConfig';
import {
  FormControlNames,
  InputTypes,
  SnackBarMessages,
} from '../../../../../../const/const';
import { SnackbarUtil } from '../../../../../../util/snackbar-util';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthGuard } from '../../../../../../guards/auth.guard';
import { HttpErrorResponse } from '@angular/common/http';
import { SpinnerService } from '../../../../../../service/spinner.service';
import { MatProgressSpinner } from '@angular/material/progress-spinner';

@Component({
  selector: 'app-add-extra-info-dialog',
  templateUrl: './add-extra-info-dialog.component.html',
  styleUrls: ['./add-extra-info-dialog.component.sass'],
})
export class AddExtraInfoDialogComponent implements OnInit {
  @ViewChild('spinner') spinner: MatProgressSpinner;
  fileUpload: any;
  fileUploadIndex = 0;
  extraInfoForm = new FormGroup({
    value: new FormControl(''),
  });

  valueInputConfig: FieldConfig = {
    type: InputTypes.INPUT_TYPE_NAME,
    name: FormControlNames.VALUE_FORM_CONTROL,
  };

  constructor(
    private extraInfoService: ExtraInfoService,
    private snackBar: MatSnackBar,
    private spinnerService: SpinnerService
  ) {}

  ngOnInit(): void {}

  addFiles(event): void {
    this.fileUpload = event[0];
  }

  save(): void {
    this.spinnerService.show(this.spinner);
    const formData = new FormData();
    formData.append('file', this.fileUpload);
    formData.append(
      'value',
      this.extraInfoForm.get(FormControlNames.VALUE_FORM_CONTROL)?.value
    );
    this.extraInfoService.save(formData).subscribe(
      () => {
        SnackbarUtil.openSnackBar(
          this.snackBar,
          SnackBarMessages.SUCCESS_MESSAGE
        );
        this.spinnerService.hide(this.spinner);
      },
      (err: HttpErrorResponse) => {
        SnackbarUtil.openSnackBar(this.snackBar, err.error.error);
        this.spinnerService.hide(this.spinner);
      }
    );
  }
}
