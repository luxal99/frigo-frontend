import {
  AfterViewInit,
  Component,
  ComponentFactoryResolver,
  ElementRef,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { LazyLoadComponentsUtil } from '../../../../util/lazy-load-components';
import { RealEstateOptionsComponent } from './real-estate-options/real-estate-options.component';
import { OffersOverviewComponent } from './offers-overview/offers-overview.component';
import { RealEstateOverviewComponent } from './real-estate-overview/real-estate-overview.component';
import { AgentsOverviewComponent } from './agents-overview/agents-overview.component';
import { AuthGuard } from '../../../../guards/auth.guard';
import { RoleGuard } from '../../../../guards/role.guard';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ContactMessagesOverviewComponent } from './contact-messages-overview/contact-messages-overview.component';
import { TokenConst } from '../../../../const/const';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.sass'],
})
export class AdminComponent implements OnInit, AfterViewInit {
  @ViewChild('agentActivator') agentComponentActivator: ElementRef;
  @ViewChild('realEstateActivator') realEstateComponentActivator: ElementRef;
  @ViewChild('settingActivator') settingsComponentActivator: ElementRef;
  @ViewChild('offerActivator') offerComponentActivator: ElementRef;
  @ViewChild('contactMessagesActivator') contactMessagesActivator: ElementRef;
  // @ts-ignore
  @ViewChild('target', { read: ViewContainerRef, static: false })
  entry: ViewContainerRef;

  constructor(
    private cvRef: ViewContainerRef,
    private resolver: ComponentFactoryResolver,
    private authGuard: AuthGuard,
    private roleGuard: RoleGuard
  ) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.roleGuard.protectComponentByAgentRole([this.agentComponentActivator]);
    this.roleGuard.protectComponentForUnconfirmedUser(
      [
        this.realEstateComponentActivator,
        this.settingsComponentActivator,
        this.agentComponentActivator,
        this.offerComponentActivator,
        this.contactMessagesActivator,
      ],
      () => {
        LazyLoadComponentsUtil.loadComponent(
          WelcomeComponent,
          this.entry,
          this.cvRef,
          this.resolver
        );
      }
    );
    this.initDefaultMenu();
  }

  initDefaultMenu(): void {
    // @ts-ignore
    const element = document.getElementById('overview-btn');
    if (element) {
      element.click();
    }
  }

  changeColor(e: any): void {
    const element = document.querySelectorAll('.active');
    [].forEach.call(element, (el: any) => {
      el.classList.remove('active');
      el.classList.add('inactive');
    });
    e.target.className = 'active';
  }

  loadEstateOptionsComponent(): void {
    LazyLoadComponentsUtil.loadComponent(
      RealEstateOptionsComponent,
      this.entry,
      this.cvRef,
      this.resolver
    );
  }

  loadEstateOverviewComponent(): void {
    LazyLoadComponentsUtil.loadComponent(
      RealEstateOverviewComponent,
      this.entry,
      this.cvRef,
      this.resolver
    ).instance.getAllEstates();
  }

  loadOfferOverviewComponent(): void {
    LazyLoadComponentsUtil.loadComponent(
      OffersOverviewComponent,
      this.entry,
      this.cvRef,
      this.resolver
    );
  }

  loadAgentsOverviewComponent(): void {
    LazyLoadComponentsUtil.loadComponent(
      AgentsOverviewComponent,
      this.entry,
      this.cvRef,
      this.resolver
    );
  }

  loadChangePasswordComponent(): void {
    LazyLoadComponentsUtil.loadComponent(
      ChangePasswordComponent,
      this.entry,
      this.cvRef,
      this.resolver
    );
  }

  loadContactMessagesComponent(): void {
    LazyLoadComponentsUtil.loadComponent(
      ContactMessagesOverviewComponent,
      this.entry,
      this.cvRef,
      this.resolver
    );
  }

  logout(): any {
    sessionStorage.removeItem(TokenConst.TOKEN_NAME);
    location.reload();
  }
}
