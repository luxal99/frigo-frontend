import { Component, OnInit, ViewChild } from '@angular/core';
import { ContactMessage } from '../../../../../models/ContactMessage';
import { ContactMessageService } from '../../../../../service/contact-message.service';
import { AuthGuard } from '../../../../../guards/auth.guard';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { SpinnerService } from '../../../../../service/spinner.service';
import {
  AgentColumnNames,
  ContactMessageColumnNames,
  SnackBarMessages,
} from '../../../../../const/const';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarUtil } from '../../../../../util/snackbar-util';
import { MatDialog } from '@angular/material/dialog';
import { DialogUtil } from '../../../../../util/dialog-util';
import { ContactMessageDialogOverviewComponent } from './contact-message-dialog-overview/contact-message-dialog-overview.component';
import { DialogOptions } from '../../../../../util/dialog-options';

@Component({
  selector: 'app-contact-messages-overview',
  templateUrl: './contact-messages-overview.component.html',
  styleUrls: ['./contact-messages-overview.component.sass'],
})
export class ContactMessagesOverviewComponent implements OnInit {
  constructor(
    private contactMessageService: ContactMessageService,
    private snackBar: MatSnackBar,
    private authGuard: AuthGuard,
    private spinnerService: SpinnerService,
    private dialog: MatDialog
  ) {}

  @ViewChild('spinner') spinner: MatProgressSpinner;
  listOfContactMessages: ContactMessage[] = [];
  token = '';

  displayedColumns: string[] = [
    ContactMessageColumnNames.FIRST_NAME_COL,
    ContactMessageColumnNames.LAST_NAME_COL,
    ContactMessageColumnNames.DATE,
    ContactMessageColumnNames.OPT_NAME_COL,
  ];

  ngOnInit(): void {
    this.getMessages();
  }

  getMessages(): void {
    this.contactMessageService.getAll().subscribe((resp) => {
      this.listOfContactMessages = resp.reverse();
      this.listOfContactMessages.filter(
        (contactMessage) =>
          (contactMessage.createdDate = new Date(contactMessage.createdDate))
      );
      this.spinnerService.hide(this.spinner);
    });
  }

  deleteContactMessage(id: number): void {
    this.contactMessageService.delete(id).subscribe(
      () => {
        this.getMessages();
      },
      () => {
        SnackbarUtil.openSnackBar(this.snackBar, SnackBarMessages.ERR_MESSAGE);
      }
    );
  }

  openContactMessageDialog(contactMessage: ContactMessage): void {
    DialogUtil.openDialog(
      ContactMessageDialogOverviewComponent,
      DialogOptions.getOptions(contactMessage),
      this.dialog
    );
  }
}
