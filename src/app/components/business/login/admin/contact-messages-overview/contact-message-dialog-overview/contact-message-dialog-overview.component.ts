import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ContactMessage } from '../../../../../../models/ContactMessage';

@Component({
  selector: 'app-contact-message-dialog-overview',
  templateUrl: './contact-message-dialog-overview.component.html',
  styleUrls: ['./contact-message-dialog-overview.component.sass'],
})
export class ContactMessageDialogOverviewComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: ContactMessage) {}

  ngOnInit(): void {}
}
