import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { SharedModule } from '../../../common/shared.module';
import { AddAgentDialogComponent } from './agents-overview/add-agent-dialog/add-agent-dialog.component';
import { AgentsOverviewComponent } from './agents-overview/agents-overview.component';
import { EditAgentDialogComponent } from './agents-overview/edit-agent-dialog/edit-agent-dialog.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ContactMessageDialogOverviewComponent } from './contact-messages-overview/contact-message-dialog-overview/contact-message-dialog-overview.component';
import { ContactMessagesOverviewComponent } from './contact-messages-overview/contact-messages-overview.component';
import { RealEstateOptionsComponent } from './real-estate-options/real-estate-options.component';
import { AddExtraInfoDialogComponent } from './real-estate-options/add-extra-info-dialog/add-extra-info-dialog.component';
import { AddOptionDialogComponent } from './real-estate-options/add-option-dialog/add-option-dialog.component';
import { EditExtraInfoDialogComponent } from './real-estate-options/edit-extra-info-dialog/edit-extra-info-dialog.component';
import { OptionOverviewComponent } from './real-estate-options/option-overview/option-overview.component';
import { RealEstateOverviewComponent } from './real-estate-overview/real-estate-overview.component';
import { AddRealEstateDialogComponent } from './real-estate-overview/add-real-estate-dialog/add-real-estate-dialog.component';
import { RealEstateOverviewDialogComponent } from './real-estate-overview/real-estate-overview-dialog/real-estate-overview-dialog.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { AgentsOverviewDialogComponent } from './agents-overview/agents-overview-dialog/agents-overview-dialog.component';
import { OffersOverviewComponent } from './offers-overview/offers-overview.component';
import { OfferOverviewDialogComponent } from './offers-overview/offer-overview-dialog/offer-overview-dialog.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { AgmCoreModule } from '@agm/core';
import { SwiperModule } from 'swiper/angular';

@NgModule({
  declarations: [
    AdminComponent,
    AddAgentDialogComponent,
    AgentsOverviewComponent,
    AgentsOverviewDialogComponent,
    EditAgentDialogComponent,
    ChangePasswordComponent,
    ContactMessageDialogOverviewComponent,
    ContactMessagesOverviewComponent,
    RealEstateOptionsComponent,
    AddExtraInfoDialogComponent,
    AddOptionDialogComponent,
    EditExtraInfoDialogComponent,
    OptionOverviewComponent,
    RealEstateOverviewComponent,
    AddRealEstateDialogComponent,
    RealEstateOverviewDialogComponent,
    ResetPasswordComponent,
    WelcomeComponent,
    OffersOverviewComponent,
    OfferOverviewDialogComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    CKEditorModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD2xkxTiOL085Yi8ZekKHnUB-E9c4UT-Hg',
    }),
    SwiperModule,
  ],
})
export class AdminModule {}
