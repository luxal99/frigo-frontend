import { environment } from '../../environments/environment';
import { CriteriaBuilder } from '../util/query/CriteriaBuilder';
import { Status } from '../enums/Status';

export const AREA_UNIT_FORM_CONTROL = 'areaUnit';
export const PHONE_FORM_CONTROL = 'phone';
export const MESSAGE_FORM_CONTROL = 'message';
export const PROPERTY_AREA_UNIT_FORM_CONTROL = 'propertyAreaUnit';
export const DESIRED_MUNICIPALITY_UNIT_FORM_CONTROL = 'desiredMunicipality';
export const PROPERTY_AREA_TO_FORM_CONTROL = 'propertyAreaTo';
export const PROPERTY_AREA_FROM_FORM_CONTROL = 'propertyAreaFrom';
export const EXTRA_ROOM_FROM_FORM_CONTROL = 'extraRooms';
export const CARPENTRY_FORM_CONTROL = 'carpentries';
export const CADASTRE_NUMBER_FORM_CONTROL = 'cadastreNumber';
export const AGENT_NOTE_FORM_CONTROL = 'agentNote';
export const RENOVATED_DATE_FORM_CONTROL = 'renovatedDate';
export const HOME_CATEGORY = 'KUĆA';
export const CURRENT_QUERY = 'currentQuery';
export const PAGE_INCREMENT = 3;
export const CLIENT_FILTER = 'client-filter';
export const ADMIN_FILTER = 'admin-filter';

export class FormControlNames {
  static ROLE_FORM_CONTROL: 'roles';
  static CONFIRM_PASSWORD_FORM_CONTROL = 'confirm';
  static CITY_FORM_CONTROL = 'city';
  static PREVIOUS_PASSWORD_FORM_CONTROL = 'previous';
  static NAME_FORM_CONTROL = 'name';
  static SEARCH_FORM_CONTROL = 'search';
  static CATEGORY_NAME_FORM_CONTROL = 'categoryName';
  static CODE_FORM_CONTROL = 'code';
  static USERNAME_NAME_FORM_CONTROL = 'username';
  static PASSWORD_NAME_FORM_CONTROL = 'password';
  static CARPENTRY_NAME_FORM_CONTROL = 'type';
  static PAYMENT_PERIOD_FORM_CONTROL = 'value';
  static FIELD_STATUS_FORM_CONTROL = 'status';
  static STREET_FORM_CONTROL = 'street';
  static NUMBER_FORM_CONTROL = 'number';
  static LONGITUDE_FORM_CONTROL = 'longitude';
  static LATITUDE_FORM_CONTROL = 'latitude';
  static BUILDING_FLOORS_FORM_CONTROL = 'buildingFloors';
  static CENTER_DISTANCE_FORM_CONTROL = 'centerDistance';
  static CONSTRUCTION_YEAR_FORM_CONTROL = 'constructionYear';
  static AREA_FORM_CONTROL = 'area';
  static AREA_FROM_FORM_CONTROL = 'areaFrom';
  static AREA_TO_FORM_CONTROL = 'areaTo';
  static CATEGORY_FORM_CONTROL = 'category';
  static FLOOR_FORM_CONTROL = 'floor';
  static BATHROOM_COUNT_FORM_CONTROL = 'bathroomCount';
  static BED_COUNT_FORM_CONTROL = 'bedCount';
  static BUILDING_AREA_FORM_CONTROL = 'buildingArea';
  static CEILING_HEIGHT_FORM_CONTROL = 'ceilingHeight';
  static HEATING_FORM_CONTROL = 'heatings';
  static FIRST_NAME_FORM_CONTROL = 'firstName';
  static LAST_NAME_FORM_CONTROL = 'lastName';
  static STRUCTURE_FORM_CONTROL = 'structure';
  static TELEPHONE_FORM_CONTROL = 'telephone';
  static EMAIL_FORM_CONTROL = 'email';
  static MUNICIPALITY_FORM_CONTROL = 'municipality';
  static SALES_TYPE_FORM_CONTROL = 'saleType';
  static EXPENSES_CABLE_FORM_CONTROL = 'expensesCable';
  static EXPENSES_ELECTRICITY_FORM_CONTROL = 'expensesElectricity';
  static EXPENSES_INFOSTAN_FORM_CONTROL = 'expensesInfostan';
  static EXPENSES_INTERNET_FORM_CONTROL = 'expensesInternet';
  static EXPENSES_MAINTENANCE_FORM_CONTROL = 'expensesMaintenance';
  static EXPENSES_PHONE_FORM_CONTROL = 'expensesPhone';
  static EXPENSES_TAXES_FORM_CONTROL = 'expensesTaxes';
  static FILED_AREA_FORM_CONTROL = 'filedArea';
  static FILED_STATUS_FORM_CONTROL = 'filedStatus';
  static FURNITURE_FORM_CONTROL = 'furniture';
  static PARKING_FORM_CONTROL = 'parkings';
  static PRICE_FORM_CONTROL = 'price';
  static PRICE_DISCOUNTED_FORM_CONTROL = 'priceDiscounted';
  static PRICE_DISPLAY_TYPE_FORM_CONTROL = 'priceDisplayType';
  static PROPERTY_AREA_FORM_CONTROL = 'propertyArea';
  static PRICE_TYPE_FORM_CONTROL = 'priceType';
  static PRICE_TO_FORM_CONTROL = 'priceTo';
  static PRICE_FROM_FORM_CONTROL = 'priceFrom';
  static ROOM_COUNT_FORM_CONTROL = 'roomCount';
  static TERMS_PAYMENT_PERIOD_FORM_CONTROL = 'termsPaymentPeriod';
  static TERMS_DEPOSIT_FORM_CONTROL = 'termsDeposit';
  static TERMS_MINIMUM_LEASE_FORM_CONTROL = 'termsMinimumLease';
  static VALUE_FORM_CONTROL = 'value';
  static WC_COUNT_FORM_CONTROL = 'wcCount';
  static STATUS_FORM_CONTROL = 'status';
  static YOUTUBE_LINK_FORM_CONTROL = 'youtubeLink';
  static AGENT_NOTE_FORM_CONTROL = 'agentNote';
  static MOVE_IN_DATE_FORM_CONTROL = 'moveInDate';
  static YARD_AREA_FORM_CONTROL = 'yardArea';
  static DIVIDE_TYPE = 'divideType';
  static BEDROOM_COUNT_FORM_CONTROL = 'bedroomCount';
}

// Pages
export class Pages {
  static ADMIN_PANEL_PAGE_ROUTE = 'admin';
  static LOGIN_PAGE_ROUTE = 'login';
  static REAL_ESTATE_DETAIL_PAGE_ROUTE = 'oglas';
  static REAL_ESTATE_FILTER_PAGE_ROUTE = 'filtriranje';
}

// Form control type
export class InputTypes {
  static INPUT_TYPE_NAME = 'input';
  static SELECT_TYPE_NAME = 'select';
  static PASSWORD_TYPE_NAME = 'password';
}

// Snackbar messages
export class SnackBarMessages {
  static SUCCESS_MESSAGE = 'Uspešno!';
  static YT_LINK_ERR_MESSAGE = 'Greška prilikom dodavanja linka!';
  static ERR_MESSAGE = 'Dogodila se greška!';
  static BAD_CREDENTIALS_MESSAGE = 'Kredencijali za pristup nisu validni!';
}

// REST routes

export class RestRoutesConst {
  static CITY_REST_ROUTE = '/cities';
  static AREA_UNIT_REST_ROUTE = '/area-units';
  static CONTACT_MESSAGE_REST_ROUTE = '/contact-messages';
  static AGENT_REST_ROUTE = '/agents';
  static CATEGORY_REST_ROUTE = '/categories';
  static CARPENTRY_REST_ROUTE = '/carpentries';
  static FLOOR_REST_ROUTE = '/floors';
  static FURNITURE_REST_ROUTE = '/furnitures';
  static HEATING_REST_ROUTE = '/heatings';
  static STRUCTURE_REST_ROUTE = '/structures';
  static ADVERT_REST_ROUTE = '/real-estates';
  static OFFER_REST_ROUTE = '/offers';
  static FILED_STATUS_REST_ROUTE = '/filed-statuses';
  static CONTACT_REST_ROUTE = '/contacts';
  static PARKING_REST_ROUTE = '/parkings';
  static PAYMENT_PERIOD_REST_ROUTE = '/payment-periods';
  static PRICE_TYPE_REST_ROUTE = '/price-types';
  static AUTH_REST_ROUTE = '/login';
  static SALES_TYPE_ROUTE = '/sale-types';
  static ADDRESS_ROUTE = '/addresses';
  static EXTRA_INFO_ROUTE = '/extra-infos';
  static REST_URL = environment.restUrl;
  static MEDIA_REST_ROUTE = '/media';
  static MEDIAS_REST_ROUTE = '/medias';
  static MUNICIPALITY_REST_ROUTE = '/municipalities';
  static USER_REST_ROUTE = '/users';
  static ROLE_REST_ROUTE = '/roles';
  static EXTRA_ROOM_REST_ROUTE = '/extra-rooms';
}

// Token
export class TokenConst {
  static TOKEN_PREFIX = 'Bearer ';
  static TOKEN_NAME = 'auth-token';
}

export class SpinnerOptions {
  static BLOCK = 'block';
  static NONE = 'none';
}

export class AgentColumnNames {
  static FIRST_NAME_COL = 'firstName';
  static LAST_NAME_COL = 'lastName';
  static NO_OF_REAL_ESTATES_NAME_COL = 'numberOfRealEstates';
  static OPT_NAME_COL = 'option';
}

export class ContactMessageColumnNames {
  static FIRST_NAME_COL = 'firstName';
  static LAST_NAME_COL = 'lastName';
  static DATE = 'date';
  static OPT_NAME_COL = 'option';
}

export class Cordinates {
  static DEF_LONGITUDE = 20.457273;
  static DEF_LATITUDE = 44.787197;
}

export class Regex {
  static PASSWORD_REGEX = '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$';
}

export class Headers {
  static X_PAGE = 'X-Page';
  static X_PAGE_COUNT = 'X-Page-Count';
  static X_DATA_COUNT = 'X-Data-Count';
}

export class CategoryValues {
  static APARTMENT = 'STAN';
  static HOUSE = 'KUĆA';
  static LOCAL = 'LOKAL';
  static GARAGE = 'GARAŽA';
  static COTTAGE = 'VIKENDICA';
  static BUSINESS_SPACE = 'POSLOVNI_PROSTOR';
  static AGRICULTURAL_LAND = 'POLJOPRIVREDNO_ZEMLJIŠTE';
  static BUILDING_LAND = 'GRAĐEVINSKO_ZEMLJIŠTE';
  static RURAL_HOUSEHOLD = 'SEOSKO DOMAĆINSTVO';
}

export const CLIENT_QUERY_DEFAULT = new CriteriaBuilder()
  .eq('status', Status.ACTIVE)
  .and();
