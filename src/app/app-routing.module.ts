import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/business/login/login.component';
import { AdminComponent } from './components/business/login/admin/admin.component';
import { HomeComponent } from './components/client/home/home.component';
import { AboutComponent } from './components/client/presentation/about/about.component';
import { RealEstateByIdComponent } from './components/client/real-estate-by-id/real-estate-by-id.component';
import { RealEstateFilterPageComponent } from './components/client/real-estate-filter-page/real-estate-filter-page.component';
import { AuthGuard } from './guards/auth.guard';
import { ContactFormComponent } from './components/client/contact-form/contact-form.component';
import { AboutFaqComponent } from './components/client/presentation/about-faq/about-faq.component';
import { AboutTermsComponent } from './components/client/presentation/about-terms/about-terms.component';
import { NotFoundComponent } from './components/client/not-found/not-found.component';
import { PricelistComponent } from './components/client/presentation/pricelist/pricelist.component';
import { PrivacyPolicyComponent } from './components/client/presentation/privacy-policy/privacy-policy.component';
import { ActiveRealEstateGuard } from './guards/active-real-estate.guard';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    loadChildren: () =>
      import('./components/client/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'login',
    component: LoginComponent,
    loadChildren: () =>
      import('./components/business/login/login.module').then(
        (m) => m.LoginModule
      ),
  },
  {
    path: 'about',
    component: AboutComponent,
    loadChildren: () =>
      import('./components/client/presentation/about/about.module').then(
        (m) => m.AboutModule
      ),
  },
  {
    path: 'faq',
    component: AboutFaqComponent,
    loadChildren: () =>
      import(
        './components/client/presentation/about-faq/about-faq.module'
      ).then((m) => m.AboutFaqModule),
  },
  {
    path: 'uslovi',
    component: AboutTermsComponent,
    loadChildren: () =>
      import(
        './components/client/presentation/about-terms/about-terms.module'
      ).then((m) => m.AboutTermsModule),
  },
  {
    path: 'oglas/:id',
    component: RealEstateByIdComponent,
    canActivate: [ActiveRealEstateGuard],
    loadChildren: () =>
      import(
        './components/client/real-estate-by-id/real-estate-by-id.module'
      ).then((m) => m.RealEstateByIdModule),
  },
  {
    path: 'filtriranje',
    component: RealEstateFilterPageComponent,
    loadChildren: () =>
      import(
        './components/client/real-estate-filter-page/real-estate-filter-page.module'
      ).then((m) => m.RealEstateFilterPageModule),
  },
  {
    path: 'kontakt',
    component: ContactFormComponent,
    loadChildren: () =>
      import('./components/client/contact-form/contact-form.module').then(
        (m) => m.ContactFormModule
      ),
  },
  {
    path: 'cenovnik',
    component: PricelistComponent,
    loadChildren: () =>
      import(
        './components/client/presentation/pricelist/pricelist.module'
      ).then((m) => m.PricelistModule),
  },
  {
    path: 'pravilnik-privatnosti',
    component: PrivacyPolicyComponent,
    loadChildren: () =>
      import(
        './components/client/presentation/privacy-policy/privacy-policy.module'
      ).then((m) => m.PrivacyPolicyModule),
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./components/business/login/admin/admin.module').then(
        (m) => m.AdminModule
      ),
  },
  {
    path: '**',
    component: NotFoundComponent,
    loadChildren: () =>
      import('./components/client/not-found/not-found.module').then(
        (m) => m.NotFoundModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
