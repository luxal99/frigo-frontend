export enum PriceDisplayType {
  'TOTAL' = 'TOTAL',
  'PER_UNIT' = 'PER_UNIT',
}
