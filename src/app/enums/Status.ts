export enum Status {
  'ACTIVE' = 'ACTIVE',
  'INACTIVE' = 'INACTIVE',
  'SOLD_RENTED' = 'SOLD_RENTED',
}
