export enum RoleType {
  'ROLE_ADMIN' = 'ROLE_ADMIN',
  'ROLE_AGENT' = 'ROLE_AGENT',
}
