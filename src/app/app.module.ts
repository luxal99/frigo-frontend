import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { SharedModule } from './components/common/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';
import { RealEstateFilterPageComponent } from './components/client/real-estate-filter-page/real-estate-filter-page.component';

@NgModule({
    declarations: [AppComponent, RealEstateFilterPageComponent],
    imports: [
        BrowserModule,
        CommonModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD2xkxTiOL085Yi8ZekKHnUB-E9c4UT-Hg',
        }),
        HammerModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        CKEditorModule,
        SharedModule,
        HttpClientModule,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
