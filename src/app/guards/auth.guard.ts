import { Observable } from 'rxjs';
import { User } from '../models/User';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { Pages, TokenConst } from '../const/const';
import { JwtUtil } from '../util/jwt-util';
import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  protected authUser!: User;

  constructor(private router: Router) {
    this.addAuthUser();
  }

  addAuthUser(): void {
    if (sessionStorage.getItem(TokenConst.TOKEN_NAME)) {
      // @ts-ignore
      JwtUtil.decode(sessionStorage.getItem(TokenConst.TOKEN_NAME)).then(
        (resp) => {
          this.authUser = resp as User;
        }
      );
    }
  }

  getAuthUser(): User {
    return this.authUser;
  }

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    const user = await JwtUtil.decode(
      sessionStorage.getItem(TokenConst.TOKEN_NAME) as string
    );
    if (!user) {
      await this.router.navigate([Pages.LOGIN_PAGE_ROUTE], {
        queryParams: { returnUrl: state.url },
      });
      return false;
    }
    // @ts-ignore
    if (
      moment(user?.exp * 1000).format('YYYY-MM-DD HH:mm') >
      moment(new Date()).format('YYYY-MM-DD HH:mm')
    ) {
      return true;
    } else {
      await this.router.navigate([Pages.LOGIN_PAGE_ROUTE], {
        queryParams: { returnUrl: state.url },
      });
      return false;
    }
  }
}
