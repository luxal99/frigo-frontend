import { ElementRef, EventEmitter, Injectable } from '@angular/core';
import { AuthGuard } from './auth.guard';
import { RoleType } from '../enums/RoleType';

@Injectable({
  providedIn: 'root',
})
export class RoleGuard {
  constructor(private authGuard: AuthGuard) {}

  protectComponentByAgentRole(activators: ElementRef[]): void {
    const authUser = this.authGuard.getAuthUser();

    // @ts-ignore
    if (
      authUser.roles?.filter((role) => role === RoleType.ROLE_ADMIN).length ===
      0
    ) {
      for (const protectedActivator of activators) {
        protectedActivator.nativeElement.remove();
      }
    }
  }

  protectComponentForUnconfirmedUser(
    activators: ElementRef[],
    callback: () => any
  ): void {
    const authUser = this.authGuard.getAuthUser();
    if (authUser.roles?.length === 0) {
      for (const protectedActivator of activators) {
        protectedActivator.nativeElement.remove();
      }
      callback();
    }
  }
}
