import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { RealEstateService } from '../service/real-estate.service';
import { RealEstate } from '../models/RealEstate';
import { Status } from '../enums/Status';

@Injectable({
  providedIn: 'root',
})
export class ActiveRealEstateGuard implements CanActivate {
  constructor(
    private realEstateService: RealEstateService,
    private router: Router
  ) {}

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    const realEstateById: RealEstate = await this.realEstateService
      .findById(
        // tslint:disable-next-line:radix
        Number.parseInt(route.params.id)
      )
      .toPromise();
    if (!(realEstateById.status === Status.ACTIVE)) {
      await this.router.navigate(['/']);
      return false;
    } else {
      return true;
    }
  }
}
