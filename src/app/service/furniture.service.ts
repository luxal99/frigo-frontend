import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Furniture } from '../models/Furniture';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class FurnitureService extends GenericService<Furniture> {
  route = RestRoutesConst.FURNITURE_REST_ROUTE;
}
