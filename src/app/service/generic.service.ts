import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RestRoutesConst, TokenConst } from '../const/const';
import { RealEstate } from '../models/RealEstate';

@Injectable({
  providedIn: 'root',
})
export class GenericService<T> {
  protected route = '';

  constructor(protected http: HttpClient) {}

  save(entity: T): Observable<T> {
    return this.http.post<T>(
      `${RestRoutesConst.REST_URL}${this.route}`,
      entity,
      {
        responseType: 'json',
        headers: {
          Authorization:
            TokenConst.TOKEN_PREFIX +
            sessionStorage.getItem(TokenConst.TOKEN_NAME),
        },
      }
    );
  }

  findById(id: number): Observable<T> {
    return this.http.get<T>(`${RestRoutesConst.REST_URL}${this.route}/` + id, {
      responseType: 'json',
    });
  }

  getAll(): Observable<T[]> {
    return this.http.get<T[]>(`${RestRoutesConst.REST_URL}${this.route}`, {
      responseType: 'json',
      headers: {
        Authorization:
          TokenConst.TOKEN_PREFIX +
          sessionStorage.getItem(TokenConst.TOKEN_NAME),
      },
    });
  }

  update(entity: T): Observable<any> {
    return this.http.put(`${RestRoutesConst.REST_URL}${this.route}`, entity, {
      responseType: 'text',
      headers: {
        Authorization:
          TokenConst.TOKEN_PREFIX +
          sessionStorage.getItem(TokenConst.TOKEN_NAME),
      },
    });
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${RestRoutesConst.REST_URL}${this.route}/${id}`, {
      responseType: 'text',
      headers: {
        Authorization:
          TokenConst.TOKEN_PREFIX +
          sessionStorage.getItem(TokenConst.TOKEN_NAME),
      },
    });
  }
}
