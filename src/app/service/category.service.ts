import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Category } from '../models/Category';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class CategoryService extends GenericService<Category> {
  route = RestRoutesConst.CATEGORY_REST_ROUTE;
}
