import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Carpentry } from '../models/Carpentry';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class CarpentryService extends GenericService<Carpentry> {
  route = RestRoutesConst.CARPENTRY_REST_ROUTE;
}
