import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Agent } from '../models/Agent';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class AgentsService extends GenericService<Agent> {
  route = RestRoutesConst.AGENT_REST_ROUTE;
}
