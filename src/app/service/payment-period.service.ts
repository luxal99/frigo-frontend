import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { PaymentPeriod } from '../models/PaymentPeriod';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class PaymentPeriodService extends GenericService<PaymentPeriod> {
  route = RestRoutesConst.PAYMENT_PERIOD_REST_ROUTE;
}
