import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Contact } from '../models/Contact';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class ContactService extends GenericService<Contact> {
  route = RestRoutesConst.CONTACT_REST_ROUTE;
}
