import { Injectable } from '@angular/core';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { SpinnerOptions } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class SpinnerService {
  show(spinner: MatProgressSpinner): void {
    spinner._elementRef.nativeElement.style.display = SpinnerOptions.BLOCK;
  }

  hide(spinner: MatProgressSpinner): void {
    spinner._elementRef.nativeElement.style.display = SpinnerOptions.NONE;
  }
}
