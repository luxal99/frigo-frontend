import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Heating } from '../models/Heating';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class HeatingService extends GenericService<Heating> {
  route = RestRoutesConst.HEATING_REST_ROUTE;
}
