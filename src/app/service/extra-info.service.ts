import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { ExtraInfo } from '../models/ExtraInfo';
import { RestRoutesConst, TokenConst } from '../const/const';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ExtraInfoService extends GenericService<ExtraInfo> {
  route = RestRoutesConst.EXTRA_INFO_ROUTE;

  save(entity: any): Observable<any> {
    return this.http.post<any>(
      `${RestRoutesConst.REST_URL}${this.route}`,
      entity,
      {
        responseType: 'json',
        headers: {
          Authorization:
            TokenConst.TOKEN_PREFIX +
            sessionStorage.getItem(TokenConst.TOKEN_NAME),
        },
      }
    );
  }

  updateCustom(entity: any, id: any, token?: string): Observable<any> {
    return this.http.put(
      `${RestRoutesConst.REST_URL}${this.route}/${id}`,
      entity,
      {
        responseType: 'text',
        headers: {
          Authorization:
            TokenConst.TOKEN_PREFIX +
            sessionStorage.getItem(TokenConst.TOKEN_NAME),
        },
      }
    );
  }
}
