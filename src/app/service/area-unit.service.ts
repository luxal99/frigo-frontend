import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { AreaUnit } from '../models/AreaUnit';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class AreaUnitService extends GenericService<AreaUnit> {
  route = RestRoutesConst.AREA_UNIT_REST_ROUTE;
}
