import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { User } from '../models/User';
import { RestRoutesConst, TokenConst } from '../const/const';
import { Observable } from 'rxjs';
import { ChangePasswordDTO } from '../models/ChangePasswordDTO';
import { Role } from '../models/Role';

@Injectable({
  providedIn: 'root',
})
export class UserService extends GenericService<User> {
  route = RestRoutesConst.USER_REST_ROUTE;

  changePassword(changePasswordDto: ChangePasswordDTO): Observable<any> {
    return this.http.put(
      RestRoutesConst.REST_URL + this.route + '/' + 'change-password',
      changePasswordDto,
      {
        responseType: 'json',
        headers: {
          Authorization:
            TokenConst.TOKEN_PREFIX +
            sessionStorage.getItem(TokenConst.TOKEN_NAME),
        },
      }
    );
  }

  getUserRoles(id: any): Observable<Role[]> {
    return this.http.get<Role[]>(
      RestRoutesConst.REST_URL +
        this.route +
        '/' +
        id +
        RestRoutesConst.ROLE_REST_ROUTE,
      {
        responseType: 'json',
        headers: {
          Authorization:
            TokenConst.TOKEN_PREFIX +
            sessionStorage.getItem(TokenConst.TOKEN_NAME),
        },
      }
    );
  }
}
