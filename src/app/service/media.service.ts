import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RestRoutesConst, TokenConst } from '../const/const';
import { Media } from '../models/Media';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root',
})
export class MediaService extends GenericService<Media> {
  route = RestRoutesConst.MEDIAS_REST_ROUTE;

  addMediaToEstate(realEstateId: any, formDate: FormData): Observable<Media> {
    return this.http.put<Media>(
      RestRoutesConst.REST_URL +
        RestRoutesConst.ADVERT_REST_ROUTE +
        '/' +
        realEstateId +
        RestRoutesConst.MEDIA_REST_ROUTE,
      formDate,
      {
        responseType: 'json',
        headers: {
          Authorization:
            TokenConst.TOKEN_PREFIX +
            sessionStorage.getItem(TokenConst.TOKEN_NAME),
        },
      }
    );
  }

  addMediasToEstate(realEstateId: any, medias: Media[]): Observable<Media> {
    return this.http.put<Media>(
      RestRoutesConst.REST_URL +
        RestRoutesConst.ADVERT_REST_ROUTE +
        '/' +
        realEstateId +
        RestRoutesConst.MEDIAS_REST_ROUTE,
      medias,
      {
        responseType: 'json',
        headers: {
          Authorization:
            TokenConst.TOKEN_PREFIX +
            sessionStorage.getItem(TokenConst.TOKEN_NAME),
        },
      }
    );
  }

  addYoutubeLink(realEstateId: any, media: Media): Observable<any> {
    return this.http.put<any>(
      RestRoutesConst.REST_URL +
        RestRoutesConst.ADVERT_REST_ROUTE +
        '/' +
        realEstateId +
        RestRoutesConst.MEDIA_REST_ROUTE +
        '/' +
        'youtube',
      media,
      {
        responseType: 'json',
        headers: {
          Authorization:
            TokenConst.TOKEN_PREFIX +
            sessionStorage.getItem(TokenConst.TOKEN_NAME),
        },
      }
    );
  }
}
