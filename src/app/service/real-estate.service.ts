import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { RealEstate } from '../models/RealEstate';
import { RestRoutesConst } from '../const/const';
import { Observable } from 'rxjs';
import { Media } from '../models/Media';
import { HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class RealEstateService extends GenericService<RealEstate> {
  route = RestRoutesConst.ADVERT_REST_ROUTE;

  getAllSalesTypes(): Observable<string[]> {
    return this.http.get<string[]>(
      RestRoutesConst.REST_URL + this.route + RestRoutesConst.SALES_TYPE_ROUTE,
      { responseType: 'json' }
    );
  }

  getAllEstatesWithBasicInfo(q?: string): Observable<RealEstate[]> {
    return this.http.get<RealEstate[]>(
      RestRoutesConst.REST_URL +
        this.route +
        `/previews` +
        (q ? `?q=${q}` : ''),
      { responseType: 'json' }
    );
  }

  getRealEstateByProperty(
    property: any,
    page?: any,
    sortByDate?: boolean,
    sortByHighestPrice?: boolean
  ): Observable<HttpResponse<any>> {
    return this.http.get<HttpResponse<any>>(
      RestRoutesConst.REST_URL +
        this.route +
        '/previews?q=' +
        property +
        (page !== undefined ? `&page=${page as string}` : '') +
        encodeURIComponent(
          sortByDate
            ? ',^createdDate'
            : sortByDate === undefined
            ? ''
            : ',createdDate'
        ) +
        encodeURIComponent(
          sortByHighestPrice
            ? ',^price'
            : sortByHighestPrice === undefined
            ? ''
            : ',price'
        ),
      {
        observe: 'response',
        responseType: 'json',
      }
    );
  }

  getRealEstateByPropertyAdmin(
    property: any,
    sortByDate?: boolean,
    sortByHighestPrice?: boolean
  ): Observable<HttpResponse<any>> {
    return this.http.get<HttpResponse<any>>(
      RestRoutesConst.REST_URL +
        this.route +
        '?q=' +
        property +
        +encodeURIComponent(
          sortByDate
            ? ',^createdDate'
            : sortByDate === undefined
            ? ''
            : ',createdDate'
        ) +
        encodeURIComponent(
          sortByHighestPrice
            ? ',^price'
            : sortByHighestPrice === undefined
            ? ''
            : ',price'
        ),
      {
        observe: 'response',
        responseType: 'json',
      }
    );
  }

  getRealEstateMedias(id: any): Observable<Media[]> {
    return this.http.get<Media[]>(
      RestRoutesConst.REST_URL +
        this.route +
        '/' +
        id +
        '/' +
        RestRoutesConst.MEDIAS_REST_ROUTE,
      { responseType: 'json' }
    );
  }

  getPinnedEstates(): Observable<RealEstate[]> {
    return this.http.get<RealEstate[]>(
      RestRoutesConst.REST_URL + this.route + '/previews/latest',
      { responseType: 'json', params: { count: '20' } }
    );
  }

  getAllMedia(realEstateID): Observable<Media[]> {
    return this.http.get<Media[]>(
      RestRoutesConst.REST_URL + this.route + '/' + realEstateID + '/medias',
      { responseType: 'json' }
    );
  }
}
