import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Floor } from '../models/Floor';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class FloorService extends GenericService<Floor> {
  route = RestRoutesConst.FLOOR_REST_ROUTE;
}
