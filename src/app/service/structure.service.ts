import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Structure } from '../models/Structure';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class StructureService extends GenericService<Structure> {
  route = RestRoutesConst.STRUCTURE_REST_ROUTE;
}
