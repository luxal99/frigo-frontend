import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Offer } from '../models/Offer';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class OfferService extends GenericService<Offer> {
  route = RestRoutesConst.OFFER_REST_ROUTE;
}
