import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Address } from '../models/Address';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class AddressService extends GenericService<Address> {
  route = RestRoutesConst.ADDRESS_ROUTE;
}
