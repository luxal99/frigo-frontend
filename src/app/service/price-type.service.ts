import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { PriceType } from '../models/PriceType';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class PriceTypeService extends GenericService<PriceType> {
  route = RestRoutesConst.PRICE_TYPE_REST_ROUTE;
}
