import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { FiledStatus } from '../models/FiledStatus';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class FiledStatusService extends GenericService<FiledStatus> {
  route = RestRoutesConst.FILED_STATUS_REST_ROUTE;
}
