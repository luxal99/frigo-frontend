import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { ContactMessage } from '../models/ContactMessage';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class ContactMessageService extends GenericService<ContactMessage> {
  route = RestRoutesConst.CONTACT_MESSAGE_REST_ROUTE;
}
