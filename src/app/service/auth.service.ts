import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/User';
import { BehaviorSubject, Observable } from 'rxjs';
import { RestRoutesConst } from '../const/const';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient, private router: Router) {}

  // tslint:disable-next-line:variable-name
  private _isAdminRoute = new BehaviorSubject(location.pathname === '/admin');

  get isAnyLoggedUser(): boolean {
    return this._isAdminRoute.value;
  }

  setIsAdminRoute(value: boolean): void {
    this._isAdminRoute.next(value);
  }

  auth(user: User): Observable<string> {
    return this.http.post(
      RestRoutesConst.REST_URL + RestRoutesConst.AUTH_REST_ROUTE,
      user,
      { responseType: 'text' }
    );
  }
}
