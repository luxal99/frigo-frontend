import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { ExtraRoom } from '../models/ExtraRoom';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class ExtraRoomService extends GenericService<ExtraRoom> {
  route = RestRoutesConst.EXTRA_ROOM_REST_ROUTE;
}
