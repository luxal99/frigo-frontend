import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Parking } from '../models/Parking';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class ParkingService extends GenericService<Parking> {
  route = RestRoutesConst.PARKING_REST_ROUTE;
}
