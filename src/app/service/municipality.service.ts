import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Municipality } from '../models/Municipality';
import { RestRoutesConst } from '../const/const';

@Injectable({
  providedIn: 'root',
})
export class MunicipalityService extends GenericService<Municipality> {
  route = RestRoutesConst.MUNICIPALITY_REST_ROUTE;
}
