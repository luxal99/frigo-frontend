export interface ConstructionYearDto {
  id?: number;
  name?: string;
  lt?: string;
  gt?: string;
}
