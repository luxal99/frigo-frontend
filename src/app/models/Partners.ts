export interface Partners {
  id: number;
  url: string;
  path: string;
}
