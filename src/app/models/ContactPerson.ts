import { Person } from './Person';

export interface ContactPerson {
  id?: number;
  person?: Person;
}
