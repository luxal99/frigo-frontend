import { Role } from './Role';

export interface TokenBody {
  exp?: number;
  iat?: number;
  roles?: Role[];
  sub?: string;
}
