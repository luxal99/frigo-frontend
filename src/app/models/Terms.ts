import { PaymentPeriod } from './PaymentPeriod';

export interface Terms {
  id?: number;
  deposit?: number;
  minimumLease?: number;
  paymentPeriod?: PaymentPeriod;
  pets?: boolean;
}
