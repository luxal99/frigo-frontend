export interface MunicipalityDto {
  id?: number;
  city?: string;
  name?: string;
}
