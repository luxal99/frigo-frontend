export interface ContactMessage {
  id?: number;
  message?: string;
  firstName?: string;
  lastName?: string;
  phone?: string;
  createdDate?: any;
  email?: string;
}
