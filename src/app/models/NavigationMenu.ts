export interface NavigationMenu {
  title: string;
  navigateTo: string;
}
