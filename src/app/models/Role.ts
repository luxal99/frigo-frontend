export interface Role {
  id?: number;
  authority?: string;
  name?: string;
}
