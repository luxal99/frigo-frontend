import { Person } from './Person';

export interface Owner {
  id?: number;
  agentNote?: string;
  person?: Person;
}
