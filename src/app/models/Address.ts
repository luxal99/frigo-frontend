import { City } from './City';
import { Municipality } from './Municipality';

export interface Address {
  id?: number;
  street?: string;
  number?: string;
  municipality?: Municipality;
}
