import { FieldConfig } from './FIeldConfig';
import { GenericService } from '../service/generic.service';

export interface AddOptDialogData {
  service: GenericService<any>;
  formFields: FieldConfig[];
  data?: any;
  headerText?: any;
}
