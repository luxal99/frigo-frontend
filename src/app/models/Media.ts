import { MediaType } from '../enums/MediaType';

export interface Media {
  id?: number;
  uri?: string;
  type?: MediaType;
  order?: number;
  realEstate?: any;
}
