import { Contact } from './Contact';

export interface Person {
  id?: number;
  firstName?: string;
  lastName?: string;
  contacts?: Contact[];
}
