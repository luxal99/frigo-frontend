import { ContactType } from './ContactType';
import { Person } from './Person';

export interface Contact {
  id?: number;
  type: ContactType;
  value: string;
  person?: number;
}
