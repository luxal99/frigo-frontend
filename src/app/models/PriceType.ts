import { DivideTypeEnum } from '../enums/DivideTypeEnum';

export interface PriceType {
  id?: number;
  type?: string;
  divideType?: DivideTypeEnum;
}
