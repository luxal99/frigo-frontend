import { MunicipalityDto } from './MunicipalityDto';

export interface City {
  id?: number;
  name?: string;
  municipalities?: MunicipalityDto[];
}
