export interface ChangePasswordDTO {
  confirm: string;
  password: string;
  previous: string;
  valid: boolean;
}
