export interface AreaUnit {
  id?: number;
  conversionFactor?: number;
  value?: string;
}
