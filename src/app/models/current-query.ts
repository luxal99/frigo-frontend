export interface CurrentQuery {
  page: number;
  urlParams: string;
}
