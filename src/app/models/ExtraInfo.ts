export interface ExtraInfo {
  id?: number;
  image?: string;
  value?: string;
}
