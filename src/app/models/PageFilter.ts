export class PageFilter {
  set currentPage(value: number) {
    this._currentPage = value;
  }
  // tslint:disable-next-line:variable-name
  private _currentPage: number;

  // tslint:disable-next-line:variable-name
  private _dataCount: number;

  // tslint:disable-next-line:variable-name
  private _numberOfPages = 0;
  // tslint:disable-next-line:variable-name
  private _numberOfPagesArr: number[] = [];

  constructor(currentPage: number, numberOfPages: number, dataCount: number) {
    this._numberOfPages = numberOfPages;
    this._currentPage = currentPage;
    this._dataCount = dataCount;

    this.makePageArray();
  }

  get dataCount(): number {
    return this._dataCount;
  }

  set dataCount(value: number) {
    this._dataCount = value;
  }

  // tslint:disable-next-line:adjacent-overload-signatures
  get currentPage(): number {
    return this._currentPage;
  }

  get numberOfPages(): any {
    return this._numberOfPages;
  }

  get numberOfPagesArr(): any[] {
    return this._numberOfPagesArr;
  }

  private makePageArray(): void {
    for (let i = 0; i < this._numberOfPages; i++) {
      this._numberOfPagesArr.push(i);
    }
  }
}
