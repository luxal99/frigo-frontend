import { Person } from './Person';

export interface Agent {
  id?: number;
  person: Person;
  realEstates?: number[];
}
