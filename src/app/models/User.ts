import { Agent } from './Agent';
import { Role } from './Role';

export interface User {
  id?: number;
  username?: string;
  password?: string;
  agent?: Agent;
  roles?: Role[];
}
