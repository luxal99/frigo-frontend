import { Floor } from './Floor';
import { SaleType } from '../enums/SaleType';
import { Owner } from './Owner';
import { Structure } from './Structure';
import { Category } from './Category';
import { City } from './City';

export interface Offer {
  id?: number;
  area?: string;
  propertyArea?: string;
  address?: string;
  city?: City;
  municipality?: string;
  floor?: Floor;
  saleType?: SaleType;
  owner?: Owner;
  structure?: Structure;
  category?: Category;
  createdDate?: any;
  dateFormatted?: any;
  price?: number;
}
