export interface PaymentPeriod {
  id?: number;
  value?: string;
}
