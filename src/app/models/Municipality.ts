import { City } from './City';

export interface Municipality {
  id?: number;
  name?: string;
  city?: City;
}
