import { Pipe, PipeTransform } from '@angular/core';
import { RealEstate } from '../models/RealEstate';
import { PriceDisplayType } from '../enums/PriceDisplayType';
import { formatPrice } from '../util/price';
import { DivideTypeEnum } from '../enums/DivideTypeEnum';

@Pipe({
  name: 'priceDiscounted',
})
export class PriceDiscountedPipe implements PipeTransform {
  transform(realEstate: RealEstate): string {
    if (!realEstate.priceDiscounted) {
      return '';
    }
    if (realEstate.priceDisplayType === PriceDisplayType.TOTAL) {
      return formatPrice(realEstate.price) + ' €';
    } else if (realEstate.priceDisplayType === PriceDisplayType.PER_UNIT) {
      // @ts-ignore
      return realEstate.priceType?.divideType === DivideTypeEnum.FLAT
        ? formatPrice(realEstate.price) +
            ' €/ ' +
            realEstate.priceType.type?.toLowerCase()
        : // @ts-ignore
          formatPrice(realEstate.price / realEstate.area) +
            ' €' +
            realEstate.priceType.type?.toLowerCase();
    } else {
      return '';
    }
  }
}
