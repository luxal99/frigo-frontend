import { Pipe, PipeTransform } from '@angular/core';
import { Contact } from '../models/Contact';
import { ContactType } from '../models/ContactType';

@Pipe({
  name: 'findContactEmail',
})
export class FindContactEmailPipe implements PipeTransform {
  transform(contacts: Contact[]): string {
    if (!contacts) {
      return '';
    } else {
      // @ts-ignore
      return (
        contacts.find((contact) => contact.type === ContactType.EMAIL).value ||
        ''
      );
    }
  }
}
