import { Pipe, PipeTransform } from '@angular/core';
import { RealEstate } from '../models/RealEstate';
import { PriceDisplayType } from '../enums/PriceDisplayType';
import { DivideTypeEnum } from '../enums/DivideTypeEnum';
import { formatPrice } from '../util/price';

@Pipe({
  name: 'price',
})
export class PricePipe implements PipeTransform {
  transform(realEstate: RealEstate): string {
    if (realEstate.priceDisplayType === PriceDisplayType.TOTAL.toString()) {
      return realEstate.priceDiscounted
        ? formatPrice(realEstate.priceDiscounted) + ' € '
        : formatPrice(realEstate.price) + ' € ';
    } else if (realEstate.priceType?.type?.toLowerCase() === 'neodređeno') {
      return realEstate.priceDiscounted
        ? formatPrice(realEstate.priceDiscounted) + ' € '
        : formatPrice(realEstate.price) + ' € ';
    } else if (realEstate.priceType?.divideType === DivideTypeEnum.FLAT) {
      // @ts-ignore
      return realEstate.priceDiscounted
        ? formatPrice(realEstate.priceDiscounted) +
            ' € ' +
            realEstate.priceType.type?.toLowerCase()
        : // @ts-ignore
          formatPrice(realEstate.price) +
            ' € ' +
            realEstate.priceType.type?.toLowerCase();
    } else if (realEstate.priceType?.divideType === DivideTypeEnum.AREA) {
      // @ts-ignore
      return realEstate.priceDiscounted
        ? formatPrice(
            realEstate.priceDiscounted /
              realEstate.area /
              realEstate.areaUnit.conversionFactor
          ) + ` €/${realEstate.areaUnit.value}`
        : // @ts-ignore
          formatPrice(
            realEstate.price /
              realEstate.area /
              realEstate.areaUnit.conversionFactor
          ) + ` €/${realEstate.areaUnit.value}`;
    } else {
      return '';
    }
  }
}
