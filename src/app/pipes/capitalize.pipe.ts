import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalize',
})
export class CapitalizePipe implements PipeTransform {
  transform(word: any): string {
    if (!word) {
      return word;
    }

    if (typeof word === 'string') {
      return word[0].toUpperCase() + word.substr(1).toLowerCase();
    } else {
      return word;
    }
  }
}
