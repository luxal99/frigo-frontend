import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'realEstatePropertyToUppercase',
})
export class RealEstatePropertyToUppercasePipe implements PipeTransform {
  transform(realEstatePropertyValue: string): string {
    if (realEstatePropertyValue === 'PVC' || realEstatePropertyValue === 'CG') {
      return realEstatePropertyValue.toUpperCase();
    } else {
      return realEstatePropertyValue;
    }
  }
}
