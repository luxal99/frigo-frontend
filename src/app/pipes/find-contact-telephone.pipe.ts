import { Pipe, PipeTransform } from '@angular/core';
import { Contact } from '../models/Contact';
import { ContactType } from '../models/ContactType';

@Pipe({
  name: 'findContactTelephone',
})
export class FindContactTelephonePipe implements PipeTransform {
  transform(contacts: Contact[]): string {
    if (!contacts) {
      return '';
    } else {
      // @ts-ignore
      return (
        contacts.find((contact) => contact.type === ContactType.PHONE).value ||
        ''
      );
    }
  }
}
