import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'role',
})
export class RolePipe implements PipeTransform {
  transform(word: string): unknown {
    if (!word) {
      return word;
    }
    const newWord = word.toLowerCase().split('role_').join('');
    return newWord[0].toUpperCase() + newWord.substr(1).toLowerCase();
  }
}
