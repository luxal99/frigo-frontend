import { Pipe, PipeTransform } from '@angular/core';
import { SaleType } from '../enums/SaleType';
import { Status } from '../enums/Status';
import { PriceDisplayType } from '../enums/PriceDisplayType';
import { DivideTypeEnum } from '../enums/DivideTypeEnum';

@Pipe({
  name: 'translate',
})
export class TranslatePipe implements PipeTransform {
  transform(word: any): string {
    if (!word) {
      return word;
    }
    if (word.toLowerCase() === SaleType.SALE.toString().toLowerCase()) {
      word = 'Prodaja';
    } else if (word.toLowerCase() === SaleType.RENT.toString().toLowerCase()) {
      word = 'Rentiranje';
    } else if (word.toLowerCase() === Status.ACTIVE.toString().toLowerCase()) {
      word = 'Aktivan';
    } else if (
      word.toLowerCase() === Status.INACTIVE.toString().toLowerCase()
    ) {
      word = 'Neaktivan';
    } else if (
      word.toLowerCase() === Status.SOLD_RENTED.toString().toLowerCase()
    ) {
      word = 'Zakupljeno';
    } else if (
      word.toLowerCase() === PriceDisplayType.PER_UNIT.toString().toLowerCase()
    ) {
      word = 'Po jedinici';
    } else if (
      word.toLowerCase() === PriceDisplayType.TOTAL.toString().toLowerCase()
    ) {
      word = 'Ukupno';
    } else if (word === 'true') {
      word = 'Da';
    } else if (word.toUpperCase() === DivideTypeEnum.FLAT.toUpperCase()) {
      word = 'Fiksno';
    } else if (word.toUpperCase() === DivideTypeEnum.AREA.toUpperCase()) {
      word = 'Povrsšina';
    }
    return word;
  }
}
