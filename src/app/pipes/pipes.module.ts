import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CapitalizePipe } from './capitalize.pipe';
import { DecimalPipe } from './decimal.pipe';
import { FindContactEmailPipe } from './find-contact-email.pipe';
import { FindContactTelephonePipe } from './find-contact-telephone.pipe';
import { FormatDatePipe } from './format-date.pipe';
import { PricePipe } from './price.pipe';
import { PriceDiscountedPipe } from './price-discounted.pipe';
import { RealEstatePropertyToUppercasePipe } from './real-estate-property-to-uppercase.pipe';
import { RolePipe } from './role.pipe';
import { SafePipe } from './safe.pipe';
import { TranslatePipe } from './translate.pipe';
import { MomentPipe } from './moment.pipe';

@NgModule({
  declarations: [
    CapitalizePipe,
    DecimalPipe,
    FindContactEmailPipe,
    FindContactTelephonePipe,
    FormatDatePipe,
    PricePipe,
    PriceDiscountedPipe,
    RealEstatePropertyToUppercasePipe,
    RolePipe,
    SafePipe,
    TranslatePipe,
    MomentPipe,
  ],
  imports: [CommonModule],
  exports: [
    CapitalizePipe,
    DecimalPipe,
    FindContactEmailPipe,
    FindContactTelephonePipe,
    FormatDatePipe,
    PricePipe,
    PriceDiscountedPipe,
    RealEstatePropertyToUppercasePipe,
    RolePipe,
    SafePipe,
    TranslatePipe,
    MomentPipe,
  ],
})
export class PipesModule {}
