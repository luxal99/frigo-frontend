import { Pipe, PipeTransform } from '@angular/core';
import { formatPrice } from '../util/price';

@Pipe({
  name: 'decimal',
})
export class DecimalPipe implements PipeTransform {
  transform(value: number): string {
    return formatPrice(value);
  }
}
