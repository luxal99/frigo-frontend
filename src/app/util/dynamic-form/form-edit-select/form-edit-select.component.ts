import { Component, Input, OnInit } from '@angular/core';
import { Field } from '../../../models/Field';
import { FieldConfig } from '../../../models/FIeldConfig';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-edit-select',
  templateUrl: './form-edit-select.component.html',
  styleUrls: ['./form-edit-select.component.sass'],
})
export class FormEditSelectComponent implements Field, OnInit {
  @Input() config!: FieldConfig;
  @Input() group!: FormGroup;
  @Input() label = 'Izaberi opciju';

  ngOnInit(): void {}
}
