import { Component, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Field } from '../../../models/Field';
import { FieldConfig } from '../../../models/FIeldConfig';
import { FormGroup } from '@angular/forms';
import { MatFormField } from '@angular/material/form-field';

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.sass'],
})
export class FormInputComponent implements Field, OnInit {
  @Input() config!: FieldConfig;
  @Input() group!: FormGroup;
  @Input() label = 'Title';
  @Input() type = 'text';
  @Input() icon = 'format_align_right';
  @Input() isRequired: boolean;
  @Input() appearance = 'fill';
  @Input() model: any;
  @Input() hint!: string;

  ngOnInit(): void {}
}
