import { Component, Input, OnInit } from '@angular/core';
import { Field } from '../../../models/Field';
import { FieldConfig } from '../../../models/FIeldConfig';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-multiple-select',
  templateUrl: './form-multiple-select.component.html',
  styleUrls: ['./form-multiple-select.component.sass'],
})
export class FormMultipleSelectComponent implements Field, OnInit {
  @Input() config!: FieldConfig;
  @Input() group!: FormGroup;
  @Input() label = 'Izaberi opciju';
  @Input() displayList: any[] = [];
  @Input() values: any[] = [];
  @Input() bindValue = 'name';
  @Input() model: any;
  @Input() fName: any;

  constructor() {}

  ngOnInit(): void {
    if (this.values.length > 0) {
      this.group.get(this.config.name)?.setValue(this.values);
    }
  }

  compareObjects(o1: any, o2: any): boolean {
    if (o2 !== null && o2 !== undefined) {
      return o1.name === o2.name && o1.id === o2.id;
    } else {
      return false;
    }
  }
}
