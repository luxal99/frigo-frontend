import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Field } from '../../../models/Field';
import { FieldConfig } from '../../../models/FIeldConfig';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-select-id',
  templateUrl: './form-select-id.component.html',
  styleUrls: ['./form-select-id.component.sass'],
})
export class FormSelectIdComponent implements Field, OnInit {
  @Output() selectionChange = new EventEmitter();
  @Input() config!: FieldConfig;
  @Input() group!: FormGroup;
  @Input() label = 'Izaberi opciju';
  @Input() isRequired = false;

  ngOnInit(): void {}
}
