import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../material.module';
import { FormInputComponent } from './form-input/form-input.component';
import { DynamicFieldDirective } from './dynamic-field/dynamic-field.directive';
import { FormSelectNameComponent } from './form-select-name/form-select-name.component';
import { FormButtonComponent } from './form-button/form-button.component';
import { FormEditSelectComponent } from './form-edit-select/form-edit-select.component';
import { FormSelectIdComponent } from './form-select-id/form-select-id.component';
import { FormSelectComponent } from './form-select/form-select.component';
import { FormMultipleSelectComponent } from './form-multiple-select/form-multiple-select.component';
import { DynamicFormContainerComponent } from './dynamic-form-container/dynamic-form-container.component';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    DynamicFieldDirective,
    FormButtonComponent,
    FormInputComponent,
    FormSelectComponent,
    FormEditSelectComponent,
    FormSelectNameComponent,
    FormSelectIdComponent,
    FormMultipleSelectComponent,
    DynamicFormContainerComponent,
  ],
  imports: [CommonModule, MaterialModule, PipesModule],
  exports: [
    MaterialModule,
    DynamicFormContainerComponent,
    DynamicFieldDirective,
    FormButtonComponent,
    FormInputComponent,
    FormSelectComponent,
    FormEditSelectComponent,
    FormSelectNameComponent,
    FormSelectIdComponent,
    FormMultipleSelectComponent,
  ],
})
export class DynamicFormModule {}
