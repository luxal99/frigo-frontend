import { Component, Input, OnInit } from '@angular/core';
import { Field } from '../../../models/Field';
import { FieldConfig } from '../../../models/FIeldConfig';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-select-name',
  templateUrl: './form-select-name.component.html',
  styleUrls: ['./form-select-name.component.sass'],
})
export class FormSelectNameComponent implements Field, OnInit {
  @Input() config!: FieldConfig;
  @Input() group!: FormGroup;
  @Input() label = 'Izaberi opciju';
  @Input() isRequired: boolean;

  ngOnInit(): void {}
}
