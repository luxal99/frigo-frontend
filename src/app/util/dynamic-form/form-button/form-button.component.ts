import { Component, OnInit } from '@angular/core';
import { Field } from '../../../models/Field';
import { FieldConfig } from '../../../models/FIeldConfig';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-button',
  templateUrl: './form-button.component.html',
  styleUrls: ['./form-button.component.sass'],
})
export class FormButtonComponent implements Field {
  config!: FieldConfig;
  group!: FormGroup;
}
