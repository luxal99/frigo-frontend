import { ComponentType } from '@angular/cdk/portal';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AddOptionDialogComponent } from '../components/business/login/admin/real-estate-options/add-option-dialog/add-option-dialog.component';

export class DialogUtil {
  // tslint:disable-next-line:typedef
  static openAddOptionsDialog(
    options: {},
    dialog: MatDialog
  ): MatDialogRef<any> {
    return dialog.open<any>(AddOptionDialogComponent, options);
  }

  static openDialog(
    component: ComponentType<any>,
    options: {},
    dialog: MatDialog
  ): MatDialogRef<any> {
    return dialog.open<any>(component, options);
  }
}
