import jwt from 'jwt-decode';
import { User } from '../models/User';
import { TokenBody } from '../models/TokenBody';

export class JwtUtil {
  static async decode(token: string): Promise<TokenBody | null> {
    try {
      const decoded: TokenBody = await jwt(token);
      return {
        exp: decoded.exp,
        // @ts-ignore
        roles: decoded.roles,
        // @ts-ignore
        username: decoded.sub,
      };
    } catch (e) {
      return null;
    }
  }
}
