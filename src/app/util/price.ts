import { formatNumber, registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import localeDeExtra from '@angular/common/locales/extra/de';

registerLocaleData(localeDe, 'de-DE', localeDeExtra);

export function formatPrice(price: any): string {
  if (price % 1 !== 0) {
    price = price.toFixed(2);
  }
  return formatNumber(price, 'de-DE');
}
