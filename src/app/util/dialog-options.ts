import { Options } from '../models/Options';
import { AddOptDialogData } from '../models/AddOptDialogData';
import { MatDialogConfig } from '@angular/material/dialog';

export class DialogOptions {
  static dialogOptions: MatDialogConfig;

  static getOptionsFormAddOptionsDialog(data: AddOptDialogData): any {
    if (window.screen.width <= 570) {
      this.dialogOptions = {
        minWidth: '50%',
        data,
        height: '100vh',
        position: { right: '0' },
      };
    } else {
      this.dialogOptions = {
        minWidth: '30%',
        data,
        height: '91vh',
        position: { right: '2%' },
      };
    }
    return this.dialogOptions;
  }

  static getOptions(data: any): any {
    if (window.screen.width <= 570) {
      this.dialogOptions = {
        minWidth: '50%',
        data,
        height: '100vh',
        position: { right: '0' },
      };
    } else {
      this.dialogOptions = {
        maxWidth: '45%',
        minWidth: '30%',
        data,
        height: '91vh',
        position: { right: '2%' },
      };
    }
    return this.dialogOptions;
  }
}
