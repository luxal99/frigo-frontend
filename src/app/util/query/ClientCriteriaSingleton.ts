import { CriteriaBuilder } from './CriteriaBuilder';
import { Status } from '../../enums/Status';

export class ClientCriteriaSingleton {
  // tslint:disable-next-line:variable-name
  private readonly _queryBuilder: CriteriaBuilder;

  constructor() {
    this._queryBuilder = new CriteriaBuilder()
      .eq('status', Status.ACTIVE)
      .and();
  }

  get queryBuilder(): CriteriaBuilder {
    return this._queryBuilder;
  }
}
