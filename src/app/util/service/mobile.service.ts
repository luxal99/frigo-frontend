import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MobileService {
  private userAgent = window.navigator.userAgent;
  // tslint:disable-next-line:variable-name
  private _isMobile = new BehaviorSubject(window.screen.width <= 660);

  // tslint:disable-next-line:variable-name
  private _isTablet = new BehaviorSubject(
    window.screen.width >= 660 && window.screen.width <= 1025
  );

  constructor() {}

  // @ts-ignore
  get isMobile(): boolean {
    return this._isMobile.value;
  }

  // @ts-ignore
  get isTablet(): boolean {
    return this._isTablet.value;
  }

  setIsMobileValue(value: boolean): void {
    this._isMobile.next(value);
  }

  setIsTabletValue(value: boolean): void {
    this._isMobile.next(value);
  }
}
