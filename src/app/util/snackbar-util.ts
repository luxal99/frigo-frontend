import { MatSnackBar } from '@angular/material/snack-bar';

export class SnackbarUtil {
  static openSnackBar(snackbar: MatSnackBar, message: string): void {
    snackbar.open(message, 'DONE', { duration: 3000 });
  }
}
