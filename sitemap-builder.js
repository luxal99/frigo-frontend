const {SitemapStream, streamToPromise} = require("sitemap");
const fs = require("fs");

//Prodaja vikendica Kosmaj
//Prodaja placeva Kosmaj
//Prodaja stanova Mladenovac
//Prodaja stanova
//Prodaja kuća
//Kontakt

const hostname = "https://frigonekretnine.com";
const now = new Date();
const links = [
	{changefreq: "monthly", lastmod: now, priority: 0.7, url: "/prodaja-vikendica-kosmaj"},
  {changefreq: "monthly", lastmod: now, priority: 0.7, url: "/prodaja-placeva-kosmaj"},
  {changefreq: "monthly", lastmod: now, priority: 0.7, url: "/prodaja-stanova-mladenovac"},
  {changefreq: "monthly", lastmod: now, priority: 0.7, url: "/prodaja-stanova"},
  {changefreq: "monthly", lastmod: now, priority: 0.7, url: "/prodaja-kuca"},
  {changefreq: "monthly", lastmod: now, priority: 0.5, url: "/kontakt"},
];

const stream = new SitemapStream({hostname, lastmodDateOnly: true});
links.forEach(link => stream.write(link));
stream.end();
return streamToPromise(stream).then(data => fs.writeFileSync("./dist/frigo-frontend/sitemap.xml", data));
